﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiTechOrderManagementSystem.Business
{
    public class Publisher
    {
        private int publisherId;
        private string publisherName;     
        private string publisherEmail;

        public int PublisherId { get => publisherId; set => publisherId = value; }
        public string PublisherName { get => publisherName; set => publisherName = value; }
        public string PublisherEmail { get => publisherEmail; set => publisherEmail = value; }
    }
}
