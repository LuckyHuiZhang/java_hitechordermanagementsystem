﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HiTechOrderManagementSystem.Business;
using MetroFramework;
using System.Data.Entity.Migrations;
using HiTechOrderManagementSystem.DataAccess;


namespace HiTechOrderManagementSystem
{
    public partial class LoginForm : MetroFramework.Forms.MetroForm
    {
        private int count = 0;
        public LoginForm()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
          
          //   MessageBox.Show(UtilityDB.ConnectDB().State.ToString());

            User aUser = new User();
            aUser.UserId = Convert.ToInt32(textBoxUserId.Text.Trim());
            aUser.Password = textBoxPassword.Text.Trim();

            if (aUser.VerifyUser(aUser))
            {
                MessageBox.Show("WELCOME TO \n HiTech Order Management System!", "Confirmation");
                HiTechSystem mainForm = new HiTechSystem();
                mainForm.Show();
            }
            else
            {
                count++;
                if (count > 3)
                {
                    textBoxUserId.Clear();
                    textBoxPassword.Clear();
                    MessageBox.Show("Please, contact your Administrator.", "Login failed!");               
                    return;
                }
                MessageBox.Show("Username or password is not valid.Please login again.", "Confirmation");
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you want to exit the application?", "Exit?", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
