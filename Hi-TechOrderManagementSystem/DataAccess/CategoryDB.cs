﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiTechOrderManagementSystem.Business;
using System.Data.SqlClient;

namespace HiTechOrderManagementSystem.DataAccess
{
    public static class CategoryDB
    {
        public static void SaveCategory(Category category)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;

            string sqlInsert = "INSERT INTO Categorys(CategoryId,CategoryName)"
                + "VALUES (@CategoryId,@CategoryName)";
            cmd.CommandText = sqlInsert;
            cmd.Parameters.AddWithValue("@CategoryId", category.CategoryId);
            cmd.Parameters.AddWithValue("@CategoryName", category.CategoryName);
            cmd.ExecuteNonQuery();
            connDB.Close();

        }

        public static void UpdateCategory(Category category)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;

            cmd.CommandText = "UPDATE Categorys " +
                              " SET CategoryId = " + category.CategoryId +
                              ",CategoryName = '" + category.CategoryName + "'" +
                              " WHERE CategoryId = " + category.CategoryId;

            cmd.ExecuteNonQuery();
            connDB.Close();
        }

        public static void DeleteCategory(Category category)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "DELETE FROM Categorys WHERE CategoryId = " + category.CategoryId;
            cmd.ExecuteNonQuery();
            connDB.Close();
        }

        public static SqlDataReader SearchCategory(Category category, int searchBy)
        {
            string searchColumn = String.Empty;
            switch (searchBy)
            {
                case 0:
                    searchColumn = "CategoryId = " + category.CategoryId + ";";
                    break;
                case 1:
                    searchColumn = "CategoryName = '" + category.CategoryName + "';";
                    break;

                default:
                    break;
            }
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "SELECT * FROM Categorys " +
                             "WHERE " + searchColumn;
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }

        public static SqlDataReader ListCategory()
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "SELECT * FROM Categorys ORDER BY CategoryId ";
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }


    }
}
