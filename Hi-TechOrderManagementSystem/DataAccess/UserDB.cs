﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiTechOrderManagementSystem.Business;
using System.Data.SqlClient;

namespace HiTechOrderManagementSystem.DataAccess
{
    public static class UserDB
    {
        public static bool IsValidUser(User aUser)
        {
            
            SqlConnection conn = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "Select UserName,Password " +
                            "FROM Users " +
                            "WHERE UserId=@UserId " +
                            "AND Password=@Password ";
            cmd.Parameters.AddWithValue("@UserId", aUser.UserId);
            cmd.Parameters.AddWithValue("@Password", aUser.Password);
            SqlDataReader sqlReader = cmd.ExecuteReader();
            if (sqlReader.Read())
            {
                conn.Close();
                return true;
            }
            else
            {
                conn.Close();
                return false;
            }

        }
        public static void SaveUser(User user)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;

            string sqlInsert = "INSERT INTO Users(UserId,EmployeeNumber,UserName,Password,Email)"
                + "VALUES (@UserId,@EmployeeNumber,@UserName,@Password,@Email)";
            cmd.CommandText = sqlInsert;
            cmd.Parameters.AddWithValue("@UserId", user.UserId);
            cmd.Parameters.AddWithValue("@EmployeeNumber", user.EmployeeNumber);
            cmd.Parameters.AddWithValue("@UserName", user.UserName);
            cmd.Parameters.AddWithValue("@Password", user.Password);
            cmd.Parameters.AddWithValue("@Email", user.Email);
            cmd.ExecuteNonQuery();
            connDB.Close();

        }

        public static void UpdateUser(User user)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            
            cmd.CommandText = "UPDATE Users " +
                              " SET UserId = " + user.UserId +
                              ",EmployeeNumber= " + user.EmployeeNumber + 
                              ",UserName= '" + user.UserName + "'" +
                              ",Password ='" + user.Password + "'" +
                              ",Email = '" + user.Email + "'" +
                              " WHERE UserId = " + user.UserId;

            cmd.ExecuteNonQuery();
            connDB.Close();
        }

        public static void DeleteUser(User user)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "DELETE FROM Users WHERE UserId = " + user.UserId;
            cmd.ExecuteNonQuery();
            connDB.Close();
        }

       public static SqlDataReader SearchUser(User user,int searchBy)
       {
            string searchColumn = String.Empty;
            switch (searchBy)
            {
                case 0:
                    searchColumn = "UserId = " + user.UserId + ";";
                    break;
                case 1:
                    searchColumn = "UserName = '" + user.UserName + "';";
                    break;

                default:
                    break;
            }
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "SELECT * FROM Users " +
                             "WHERE " + searchColumn;
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
       }

        public static SqlDataReader ListUser()
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "SELECT * FROM Users ORDER BY UserId ";
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }




    }
}
