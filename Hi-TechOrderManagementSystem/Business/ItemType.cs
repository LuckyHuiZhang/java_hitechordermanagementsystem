﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiTechOrderManagementSystem.Business
{
   public class ItemType
    {
        private int itemId;
        private string itemTypeName;

        public int ItemId { get => itemId; set => itemId = value; }
        public string ItemTypeName { get => itemTypeName; set => itemTypeName = value; }
    }
}
