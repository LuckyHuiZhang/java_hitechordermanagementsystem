﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiTechOrderManagementSystem.DataAccess;
using System.Data.SqlClient;

namespace HiTechOrderManagementSystem.Business
{
    public class Book
    {
        private int bookISBN;
        private int publisherId;
        private int categoryId;
        private string bookTitle;
        private int bookPrice;
        private int bookQOH;
        private int bookOrderQuantity;
        private string bookPublishedDate;

        public int BookISBN { get => bookISBN; set => bookISBN = value; }
        public int PublisherId { get => publisherId; set => publisherId = value; }
        public int CategoryId { get => categoryId; set => categoryId = value; }
        public string BookTitle { get => bookTitle; set => bookTitle = value; }
        public int BookPrice { get => bookPrice; set => bookPrice = value; }
        public int BookQOH { get => bookQOH; set => bookQOH = value; }
        public int BookOrderQuantity { get => bookOrderQuantity; set => bookOrderQuantity = value; }
        public string BookPublishedDate { get => bookPublishedDate; set => bookPublishedDate = value; }
      

        public void SaveRecord(Book book)
        {
            BookDB.SaveBook(book);
        }

        public void UpdateRecord(Book book)
        {
            BookDB.UpdateBook(book);
        }

        public void DeleteRecord(Book book)
        {
            BookDB.DeleteBook(book);
        }

        public SqlDataReader SearchRecord(Book book, int searchBy)
        {
            return BookDB.SearchBook(book, searchBy);
        }

        public SqlDataReader ListRecord()
        {
            return BookDB.ListBook();
        }

    }
}
