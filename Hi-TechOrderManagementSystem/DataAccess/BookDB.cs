﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiTechOrderManagementSystem.Business;
using System.Data.SqlClient;

namespace HiTechOrderManagementSystem.DataAccess
{
    public static class BookDB
    {
        public static void SaveBook(Book book)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;

            string sqlInsert = "INSERT INTO Books(BookISBN,PublisherId,CategoryId,BookTitle,BookPrice,BookQOH,BookOrderQuantity,BookPublishedDate)"
                + "VALUES (@BookISBN,@PublisherId,@CategoryId,@BookTitle,@BookPrice,@BookQOH,@BookOrderQuantity,@BookPublishedDate)";
            cmd.CommandText = sqlInsert;
            cmd.Parameters.AddWithValue("@BookISBN", book.BookISBN);
            cmd.Parameters.AddWithValue("@PublisherId", book.PublisherId);
            cmd.Parameters.AddWithValue("@CategoryId", book.CategoryId);
            cmd.Parameters.AddWithValue("@BookTitle", book.BookTitle);
            cmd.Parameters.AddWithValue("@BookPrice", book.BookPrice);
            cmd.Parameters.AddWithValue("@BookQOH", book.BookQOH);
            cmd.Parameters.AddWithValue("@BookOrderQuantity", book.BookOrderQuantity);
            cmd.Parameters.AddWithValue("@BookPublishedDate", book.BookPublishedDate);
            cmd.ExecuteNonQuery();
            connDB.Close();

        }

        public static void UpdateBook(Book book)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;

            cmd.CommandText = "UPDATE Books " +
                              " SET BookISBN = " + book.BookISBN + 
                              ",PublisherId = " + book.PublisherId + 
                              ",CategoryId = " + book.CategoryId + 
                              ",BookTitle ='" + book.BookTitle + "'" +
                              ",BookPrice = " + book.BookPrice + 
                              ",BookQOH = " + book.BookQOH + 
                              ",BookOrderQuantity = " + book.BookOrderQuantity + 
                              ",BookPublishedDate = '" + book.BookPublishedDate + "'" +
                              " WHERE BookISBN = " + book.BookISBN;

            cmd.ExecuteNonQuery();
            connDB.Close();
        }

        public static void DeleteBook(Book book)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "DELETE FROM Books WHERE BookISBN = " + book.BookISBN;
            cmd.ExecuteNonQuery();
            connDB.Close();
        }

        public static SqlDataReader SearchBook(Book book, int searchBy)
        {
            string searchColumn = String.Empty;
            switch (searchBy)
            {
                case 0:
                    searchColumn = "BookISBN = " + book.BookISBN + ";";
                    break;
                case 1:
                    searchColumn = "BookTitle = '" + book.BookTitle + "';";
                    break;

                default:
                    break;
            }
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "SELECT * FROM Books " +
                             "WHERE " + searchColumn;
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }

        public static SqlDataReader ListBook()
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "SELECT * FROM Books ORDER BY BookISBN ";
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }
    }
}
