﻿namespace HiTechOrderManagementSystem
{
    partial class HiTechSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HiTechSystem));
            this.MainTab = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPageUserManagement = new MetroFramework.Controls.MetroTabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lvUserInfo = new MetroFramework.Controls.MetroListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxUserEmpNumber = new MetroFramework.Controls.MetroTextBox();
            this.metroTextUserID = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxUserName = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxUserEmail = new MetroFramework.Controls.MetroTextBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.btnUserList = new MetroFramework.Controls.MetroButton();
            this.btnUserAdd = new MetroFramework.Controls.MetroButton();
            this.btnUserUpdate = new MetroFramework.Controls.MetroButton();
            this.btnUserDelete = new MetroFramework.Controls.MetroButton();
            this.metroTextBoxUserPassword = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.metroComboBoxUserSearch = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelInput = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxUserSearch = new MetroFramework.Controls.MetroTextBox();
            this.btnUserSearch = new MetroFramework.Controls.MetroButton();
            this.metroTabPageMISManager = new MetroFramework.Controls.MetroTabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.metroListViewEmployee = new MetroFramework.Controls.MetroListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.metroComboBoxMISSearch = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelMISInput = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxSearchID = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonEmpSearch = new MetroFramework.Controls.MetroButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxEmpPhoneNumber = new MetroFramework.Controls.MetroTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.metroButtonEmpList = new MetroFramework.Controls.MetroButton();
            this.metroButtonEmpAdd = new MetroFramework.Controls.MetroButton();
            this.metroButtonEmpUpdate = new MetroFramework.Controls.MetroButton();
            this.metroButtonEmpDelete = new MetroFramework.Controls.MetroButton();
            this.metroTextBoxEmpEmail = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxEmployeeNumber = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxFirstName = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxJobTitle = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxLastName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabelFN = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPageSalesManager = new MetroFramework.Controls.MetroTabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.metroComboBoxSaleSearch = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelSaleInput = new MetroFramework.Controls.MetroLabel();
            this.metroButtonClientSearch = new MetroFramework.Controls.MetroButton();
            this.metroTextBoxClientSearchID = new MetroFramework.Controls.MetroTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.metroButtonClientList = new MetroFramework.Controls.MetroButton();
            this.metroButtonClientAdd = new MetroFramework.Controls.MetroButton();
            this.metroButtonClientUpdate = new MetroFramework.Controls.MetroButton();
            this.metroButtonClientDelete = new MetroFramework.Controls.MetroButton();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxClientFax = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxClientCredit = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxClientPostCode = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxClientStreet = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxClientCity = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxClientID = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxClientName = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxClientEmail = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxClientPhoneNumber = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridViewClients = new System.Windows.Forms.DataGridView();
            this.metroTabPageOrderClerks = new MetroFramework.Controls.MetroTabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.metroComboBoxOrderSearch = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelOrderInput = new MetroFramework.Controls.MetroLabel();
            this.metroButtonOrderSearch = new MetroFramework.Controls.MetroButton();
            this.metroTextBoxOrderSearchId = new MetroFramework.Controls.MetroTextBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.metroListViewOrder = new MetroFramework.Controls.MetroListView();
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.metroTextBoxOrderShippingDate = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxOrderDate = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxOrderId = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel33 = new MetroFramework.Controls.MetroLabel();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.metroButtonOrderList = new MetroFramework.Controls.MetroButton();
            this.metroButtonOrderAdd = new MetroFramework.Controls.MetroButton();
            this.metroButtonOderUpdate = new MetroFramework.Controls.MetroButton();
            this.metroButtonOrderDelete = new MetroFramework.Controls.MetroButton();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxOderQuantity = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxOrderTotal = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel24 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxOdrtPayment = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxOderClientID = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxOderItemId = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxOderUnitPrice = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxOderItemName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel26 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel27 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel28 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPageAccountant = new MetroFramework.Controls.MetroTabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.metroListViewInvoice = new MetroFramework.Controls.MetroListView();
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.metroButtonInvoiceList = new MetroFramework.Controls.MetroButton();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.metroComboBoxInvoiceSearch = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelInvoiceInput = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxInvoiceSearchId = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonInvoiceSearch = new MetroFramework.Controls.MetroButton();
            this.metroTabPageAuthor = new MetroFramework.Controls.MetroTabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.metroListViewAuthor = new MetroFramework.Controls.MetroListView();
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.metroTextBoxAuthorEmail = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel39 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxAuthorLN = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel34 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxAuthorPublisherId = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxAuthorId = new MetroFramework.Controls.MetroTextBox();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.metroButtonAuthorUpdate = new MetroFramework.Controls.MetroButton();
            this.metroButtonAuthorList = new MetroFramework.Controls.MetroButton();
            this.metroButtonAuthorAdd = new MetroFramework.Controls.MetroButton();
            this.metroButtonAuthorDelete = new MetroFramework.Controls.MetroButton();
            this.metroTextBoxAuthorBookISBN = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxAuthorFN = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel35 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel36 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel37 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel38 = new MetroFramework.Controls.MetroLabel();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.metroComboBoxAuthorSearch = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelAuthorInput = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxAuthorSearchId = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonAuthorSearch = new MetroFramework.Controls.MetroButton();
            this.metroTabPageItem = new MetroFramework.Controls.MetroTabPage();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.metroListViewBook = new MetroFramework.Controls.MetroListView();
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader37 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader38 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader39 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader41 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.metroTextBoxBookOrderQuantity = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel40 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxBookQOH = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxBookPublishedDate = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel47 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel49 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxBookPrice = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel41 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxBookPublisherId = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxBookISBN = new MetroFramework.Controls.MetroTextBox();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.metroButtonBookUpdate = new MetroFramework.Controls.MetroButton();
            this.metroButtonBookList = new MetroFramework.Controls.MetroButton();
            this.metroButtonBookAdd = new MetroFramework.Controls.MetroButton();
            this.metroButtonBookDelete = new MetroFramework.Controls.MetroButton();
            this.metroTextBoxBookCategoryId = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxBookBookTitle = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel42 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel43 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel44 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel45 = new MetroFramework.Controls.MetroLabel();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.metroComboBoxBookSearch = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelBookInput = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxBookSearchId = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonBookSearch = new MetroFramework.Controls.MetroButton();
            this.metroTabPageCategory = new MetroFramework.Controls.MetroTabPage();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.metroListViewCategory = new MetroFramework.Controls.MetroListView();
            this.columnHeader40 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader42 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.metroLabel46 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxCategoryName = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxCategoryId = new MetroFramework.Controls.MetroTextBox();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.metroButtonCategoryList = new MetroFramework.Controls.MetroButton();
            this.metroButtonCategoryAdd = new MetroFramework.Controls.MetroButton();
            this.metroButtonCategoryUpdate = new MetroFramework.Controls.MetroButton();
            this.metroButtonCategoryDelete = new MetroFramework.Controls.MetroButton();
            this.metroLabel48 = new MetroFramework.Controls.MetroLabel();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.metroComboBoxCategorySearch = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelCategoryInput = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxCategorySearchId = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonCategory = new MetroFramework.Controls.MetroButton();
            this.metroContextMenu1 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.metroButtonAppExit = new MetroFramework.Controls.MetroButton();
            this.MainTab.SuspendLayout();
            this.metroTabPageUserManagement.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.metroTabPageMISManager.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.metroTabPageSalesManager.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClients)).BeginInit();
            this.metroTabPageOrderClerks.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.metroTabPageAccountant.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.metroTabPageAuthor.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.metroTabPageItem.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.metroTabPageCategory.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTab
            // 
            this.MainTab.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.MainTab.Controls.Add(this.metroTabPageUserManagement);
            this.MainTab.Controls.Add(this.metroTabPageMISManager);
            this.MainTab.Controls.Add(this.metroTabPageSalesManager);
            this.MainTab.Controls.Add(this.metroTabPageOrderClerks);
            this.MainTab.Controls.Add(this.metroTabPageAccountant);
            this.MainTab.Controls.Add(this.metroTabPageAuthor);
            this.MainTab.Controls.Add(this.metroTabPageItem);
            this.MainTab.Controls.Add(this.metroTabPageCategory);
            this.MainTab.FontSize = MetroFramework.MetroTabControlSize.Small;
            this.MainTab.Location = new System.Drawing.Point(56, 120);
            this.MainTab.Margin = new System.Windows.Forms.Padding(6);
            this.MainTab.Multiline = true;
            this.MainTab.Name = "MainTab";
            this.MainTab.SelectedIndex = 6;
            this.MainTab.Size = new System.Drawing.Size(1926, 986);
            this.MainTab.Style = MetroFramework.MetroColorStyle.Green;
            this.MainTab.TabIndex = 9;
            this.MainTab.Theme = MetroFramework.MetroThemeStyle.Light;
            this.MainTab.UseSelectable = true;
            // 
            // metroTabPageUserManagement
            // 
            this.metroTabPageUserManagement.BackColor = System.Drawing.Color.White;
            this.metroTabPageUserManagement.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroTabPageUserManagement.BackgroundImage")));
            this.metroTabPageUserManagement.Controls.Add(this.groupBox8);
            this.metroTabPageUserManagement.Controls.Add(this.groupBox22);
            this.metroTabPageUserManagement.Controls.Add(this.groupBox23);
            this.metroTabPageUserManagement.Font = new System.Drawing.Font("SimSun", 4.875F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.metroTabPageUserManagement.HorizontalScrollbarBarColor = true;
            this.metroTabPageUserManagement.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageUserManagement.HorizontalScrollbarSize = 10;
            this.metroTabPageUserManagement.Location = new System.Drawing.Point(4, 37);
            this.metroTabPageUserManagement.Margin = new System.Windows.Forms.Padding(4);
            this.metroTabPageUserManagement.Name = "metroTabPageUserManagement";
            this.metroTabPageUserManagement.Size = new System.Drawing.Size(1918, 945);
            this.metroTabPageUserManagement.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTabPageUserManagement.TabIndex = 0;
            this.metroTabPageUserManagement.Text = "User Managerment";
            this.metroTabPageUserManagement.VerticalScrollbarBarColor = true;
            this.metroTabPageUserManagement.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageUserManagement.VerticalScrollbarSize = 10;
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox8.Controls.Add(this.lvUserInfo);
            this.groupBox8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox8.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(36, 440);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox8.Size = new System.Drawing.Size(1848, 422);
            this.groupBox8.TabIndex = 82;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "List Clients";
            // 
            // lvUserInfo
            // 
            this.lvUserInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader11,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lvUserInfo.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lvUserInfo.FullRowSelect = true;
            this.lvUserInfo.GridLines = true;
            this.lvUserInfo.Location = new System.Drawing.Point(0, 66);
            this.lvUserInfo.Margin = new System.Windows.Forms.Padding(4);
            this.lvUserInfo.Name = "lvUserInfo";
            this.lvUserInfo.OwnerDraw = true;
            this.lvUserInfo.Size = new System.Drawing.Size(1836, 316);
            this.lvUserInfo.TabIndex = 81;
            this.lvUserInfo.UseCompatibleStateImageBehavior = false;
            this.lvUserInfo.UseSelectable = true;
            this.lvUserInfo.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "User ID";
            this.columnHeader1.Width = 312;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "EmployeeNumber";
            this.columnHeader11.Width = 323;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "User Name";
            this.columnHeader2.Width = 304;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Password";
            this.columnHeader3.Width = 426;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Email";
            this.columnHeader4.Width = 479;
            // 
            // groupBox22
            // 
            this.groupBox22.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox22.Controls.Add(this.metroLabel11);
            this.groupBox22.Controls.Add(this.metroTextBoxUserEmpNumber);
            this.groupBox22.Controls.Add(this.metroTextUserID);
            this.groupBox22.Controls.Add(this.metroTextBoxUserName);
            this.groupBox22.Controls.Add(this.metroTextBoxUserEmail);
            this.groupBox22.Controls.Add(this.groupBox24);
            this.groupBox22.Controls.Add(this.metroTextBoxUserPassword);
            this.groupBox22.Controls.Add(this.metroLabel4);
            this.groupBox22.Controls.Add(this.metroLabel3);
            this.groupBox22.Controls.Add(this.metroLabel2);
            this.groupBox22.Controls.Add(this.metroLabel1);
            this.groupBox22.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox22.Location = new System.Drawing.Point(36, 24);
            this.groupBox22.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox22.Size = new System.Drawing.Size(1268, 380);
            this.groupBox22.TabIndex = 78;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "User Info";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(92, 108);
            this.metroLabel11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(120, 19);
            this.metroLabel11.TabIndex = 82;
            this.metroLabel11.Text = "EmployeeNumber:";
            // 
            // metroTextBoxUserEmpNumber
            // 
            // 
            // 
            // 
            this.metroTextBoxUserEmpNumber.CustomButton.Image = null;
            this.metroTextBoxUserEmpNumber.CustomButton.Location = new System.Drawing.Point(510, 2);
            this.metroTextBoxUserEmpNumber.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxUserEmpNumber.CustomButton.Name = "";
            this.metroTextBoxUserEmpNumber.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxUserEmpNumber.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxUserEmpNumber.CustomButton.TabIndex = 1;
            this.metroTextBoxUserEmpNumber.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxUserEmpNumber.CustomButton.UseSelectable = true;
            this.metroTextBoxUserEmpNumber.CustomButton.Visible = false;
            this.metroTextBoxUserEmpNumber.Lines = new string[0];
            this.metroTextBoxUserEmpNumber.Location = new System.Drawing.Point(92, 146);
            this.metroTextBoxUserEmpNumber.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxUserEmpNumber.MaxLength = 32767;
            this.metroTextBoxUserEmpNumber.Name = "metroTextBoxUserEmpNumber";
            this.metroTextBoxUserEmpNumber.PasswordChar = '\0';
            this.metroTextBoxUserEmpNumber.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxUserEmpNumber.SelectedText = "";
            this.metroTextBoxUserEmpNumber.SelectionLength = 0;
            this.metroTextBoxUserEmpNumber.SelectionStart = 0;
            this.metroTextBoxUserEmpNumber.ShortcutsEnabled = true;
            this.metroTextBoxUserEmpNumber.Size = new System.Drawing.Size(532, 24);
            this.metroTextBoxUserEmpNumber.TabIndex = 81;
            this.metroTextBoxUserEmpNumber.UseSelectable = true;
            this.metroTextBoxUserEmpNumber.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxUserEmpNumber.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextUserID
            // 
            // 
            // 
            // 
            this.metroTextUserID.CustomButton.Image = null;
            this.metroTextUserID.CustomButton.Location = new System.Drawing.Point(510, 2);
            this.metroTextUserID.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextUserID.CustomButton.Name = "";
            this.metroTextUserID.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextUserID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextUserID.CustomButton.TabIndex = 1;
            this.metroTextUserID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextUserID.CustomButton.UseSelectable = true;
            this.metroTextUserID.CustomButton.Visible = false;
            this.metroTextUserID.Lines = new string[0];
            this.metroTextUserID.Location = new System.Drawing.Point(92, 68);
            this.metroTextUserID.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextUserID.MaxLength = 32767;
            this.metroTextUserID.Name = "metroTextUserID";
            this.metroTextUserID.PasswordChar = '\0';
            this.metroTextUserID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextUserID.SelectedText = "";
            this.metroTextUserID.SelectionLength = 0;
            this.metroTextUserID.SelectionStart = 0;
            this.metroTextUserID.ShortcutsEnabled = true;
            this.metroTextUserID.Size = new System.Drawing.Size(532, 24);
            this.metroTextUserID.TabIndex = 80;
            this.metroTextUserID.Tag = "Search";
            this.metroTextUserID.UseSelectable = true;
            this.metroTextUserID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextUserID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxUserName
            // 
            // 
            // 
            // 
            this.metroTextBoxUserName.CustomButton.Image = null;
            this.metroTextBoxUserName.CustomButton.Location = new System.Drawing.Point(510, 2);
            this.metroTextBoxUserName.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxUserName.CustomButton.Name = "";
            this.metroTextBoxUserName.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxUserName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxUserName.CustomButton.TabIndex = 1;
            this.metroTextBoxUserName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxUserName.CustomButton.UseSelectable = true;
            this.metroTextBoxUserName.CustomButton.Visible = false;
            this.metroTextBoxUserName.Lines = new string[0];
            this.metroTextBoxUserName.Location = new System.Drawing.Point(92, 212);
            this.metroTextBoxUserName.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxUserName.MaxLength = 32767;
            this.metroTextBoxUserName.Name = "metroTextBoxUserName";
            this.metroTextBoxUserName.PasswordChar = '\0';
            this.metroTextBoxUserName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxUserName.SelectedText = "";
            this.metroTextBoxUserName.SelectionLength = 0;
            this.metroTextBoxUserName.SelectionStart = 0;
            this.metroTextBoxUserName.ShortcutsEnabled = true;
            this.metroTextBoxUserName.Size = new System.Drawing.Size(532, 24);
            this.metroTextBoxUserName.TabIndex = 79;
            this.metroTextBoxUserName.Tag = "Search";
            this.metroTextBoxUserName.UseSelectable = true;
            this.metroTextBoxUserName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxUserName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxUserEmail
            // 
            // 
            // 
            // 
            this.metroTextBoxUserEmail.CustomButton.Image = null;
            this.metroTextBoxUserEmail.CustomButton.Location = new System.Drawing.Point(510, 2);
            this.metroTextBoxUserEmail.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxUserEmail.CustomButton.Name = "";
            this.metroTextBoxUserEmail.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxUserEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxUserEmail.CustomButton.TabIndex = 1;
            this.metroTextBoxUserEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxUserEmail.CustomButton.UseSelectable = true;
            this.metroTextBoxUserEmail.CustomButton.Visible = false;
            this.metroTextBoxUserEmail.Lines = new string[0];
            this.metroTextBoxUserEmail.Location = new System.Drawing.Point(92, 344);
            this.metroTextBoxUserEmail.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxUserEmail.MaxLength = 32767;
            this.metroTextBoxUserEmail.Name = "metroTextBoxUserEmail";
            this.metroTextBoxUserEmail.PasswordChar = '\0';
            this.metroTextBoxUserEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxUserEmail.SelectedText = "";
            this.metroTextBoxUserEmail.SelectionLength = 0;
            this.metroTextBoxUserEmail.SelectionStart = 0;
            this.metroTextBoxUserEmail.ShortcutsEnabled = true;
            this.metroTextBoxUserEmail.Size = new System.Drawing.Size(532, 24);
            this.metroTextBoxUserEmail.TabIndex = 78;
            this.metroTextBoxUserEmail.Tag = "Search";
            this.metroTextBoxUserEmail.UseSelectable = true;
            this.metroTextBoxUserEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxUserEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // groupBox24
            // 
            this.groupBox24.BackColor = System.Drawing.Color.White;
            this.groupBox24.Controls.Add(this.btnUserList);
            this.groupBox24.Controls.Add(this.btnUserAdd);
            this.groupBox24.Controls.Add(this.btnUserUpdate);
            this.groupBox24.Controls.Add(this.btnUserDelete);
            this.groupBox24.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox24.Location = new System.Drawing.Point(754, 24);
            this.groupBox24.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox24.Size = new System.Drawing.Size(476, 344);
            this.groupBox24.TabIndex = 77;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Buttom Bar";
            // 
            // btnUserList
            // 
            this.btnUserList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUserList.BackgroundImage")));
            this.btnUserList.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnUserList.Location = new System.Drawing.Point(60, 284);
            this.btnUserList.Margin = new System.Windows.Forms.Padding(6);
            this.btnUserList.Name = "btnUserList";
            this.btnUserList.Size = new System.Drawing.Size(392, 50);
            this.btnUserList.TabIndex = 79;
            this.btnUserList.Text = "&List";
            this.btnUserList.UseSelectable = true;
            this.btnUserList.Click += new System.EventHandler(this.btnUserList_Click_1);
            // 
            // btnUserAdd
            // 
            this.btnUserAdd.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnUserAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUserAdd.BackgroundImage")));
            this.btnUserAdd.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnUserAdd.Location = new System.Drawing.Point(60, 64);
            this.btnUserAdd.Margin = new System.Windows.Forms.Padding(6);
            this.btnUserAdd.Name = "btnUserAdd";
            this.btnUserAdd.Size = new System.Drawing.Size(392, 48);
            this.btnUserAdd.TabIndex = 75;
            this.btnUserAdd.Text = "&Add";
            this.btnUserAdd.UseSelectable = true;
            this.btnUserAdd.Click += new System.EventHandler(this.btnUserAdd_Click_1);
            // 
            // btnUserUpdate
            // 
            this.btnUserUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUserUpdate.BackgroundImage")));
            this.btnUserUpdate.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnUserUpdate.Location = new System.Drawing.Point(60, 140);
            this.btnUserUpdate.Margin = new System.Windows.Forms.Padding(6);
            this.btnUserUpdate.Name = "btnUserUpdate";
            this.btnUserUpdate.Size = new System.Drawing.Size(392, 48);
            this.btnUserUpdate.TabIndex = 77;
            this.btnUserUpdate.Text = "&Update";
            this.btnUserUpdate.UseSelectable = true;
            this.btnUserUpdate.Click += new System.EventHandler(this.btnUserUpdate_Click_1);
            // 
            // btnUserDelete
            // 
            this.btnUserDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUserDelete.BackgroundImage")));
            this.btnUserDelete.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnUserDelete.Location = new System.Drawing.Point(60, 204);
            this.btnUserDelete.Margin = new System.Windows.Forms.Padding(6);
            this.btnUserDelete.Name = "btnUserDelete";
            this.btnUserDelete.Size = new System.Drawing.Size(392, 52);
            this.btnUserDelete.TabIndex = 76;
            this.btnUserDelete.Text = "&Delete";
            this.btnUserDelete.UseSelectable = true;
            this.btnUserDelete.Click += new System.EventHandler(this.btnUserDelete_Click_1);
            // 
            // metroTextBoxUserPassword
            // 
            // 
            // 
            // 
            this.metroTextBoxUserPassword.CustomButton.Image = null;
            this.metroTextBoxUserPassword.CustomButton.Location = new System.Drawing.Point(510, 2);
            this.metroTextBoxUserPassword.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxUserPassword.CustomButton.Name = "";
            this.metroTextBoxUserPassword.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxUserPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxUserPassword.CustomButton.TabIndex = 1;
            this.metroTextBoxUserPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxUserPassword.CustomButton.UseSelectable = true;
            this.metroTextBoxUserPassword.CustomButton.Visible = false;
            this.metroTextBoxUserPassword.Lines = new string[0];
            this.metroTextBoxUserPassword.Location = new System.Drawing.Point(92, 280);
            this.metroTextBoxUserPassword.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxUserPassword.MaxLength = 32767;
            this.metroTextBoxUserPassword.Name = "metroTextBoxUserPassword";
            this.metroTextBoxUserPassword.PasswordChar = '\0';
            this.metroTextBoxUserPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxUserPassword.SelectedText = "";
            this.metroTextBoxUserPassword.SelectionLength = 0;
            this.metroTextBoxUserPassword.SelectionStart = 0;
            this.metroTextBoxUserPassword.ShortcutsEnabled = true;
            this.metroTextBoxUserPassword.Size = new System.Drawing.Size(532, 24);
            this.metroTextBoxUserPassword.TabIndex = 77;
            this.metroTextBoxUserPassword.Tag = "Search";
            this.metroTextBoxUserPassword.UseSelectable = true;
            this.metroTextBoxUserPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxUserPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel4.Location = new System.Drawing.Point(92, 44);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(47, 15);
            this.metroLabel4.TabIndex = 76;
            this.metroLabel4.Text = "User ID:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel3.Location = new System.Drawing.Point(92, 180);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(67, 15);
            this.metroLabel3.TabIndex = 75;
            this.metroLabel3.Text = "User Name:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel2.Location = new System.Drawing.Point(92, 242);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(61, 15);
            this.metroLabel2.TabIndex = 74;
            this.metroLabel2.Text = "Password: ";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel1.Location = new System.Drawing.Point(92, 308);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(39, 15);
            this.metroLabel1.TabIndex = 73;
            this.metroLabel1.Text = "Email: ";
            // 
            // groupBox23
            // 
            this.groupBox23.BackColor = System.Drawing.Color.White;
            this.groupBox23.Controls.Add(this.metroComboBoxUserSearch);
            this.groupBox23.Controls.Add(this.metroLabelInput);
            this.groupBox23.Controls.Add(this.metroTextBoxUserSearch);
            this.groupBox23.Controls.Add(this.btnUserSearch);
            this.groupBox23.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox23.Location = new System.Drawing.Point(1348, 24);
            this.groupBox23.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox23.Size = new System.Drawing.Size(528, 380);
            this.groupBox23.TabIndex = 76;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Search By";
            // 
            // metroComboBoxUserSearch
            // 
            this.metroComboBoxUserSearch.FormattingEnabled = true;
            this.metroComboBoxUserSearch.ItemHeight = 23;
            this.metroComboBoxUserSearch.Items.AddRange(new object[] {
            "User ID",
            "User Name"});
            this.metroComboBoxUserSearch.Location = new System.Drawing.Point(26, 96);
            this.metroComboBoxUserSearch.Margin = new System.Windows.Forms.Padding(4);
            this.metroComboBoxUserSearch.Name = "metroComboBoxUserSearch";
            this.metroComboBoxUserSearch.Size = new System.Drawing.Size(478, 29);
            this.metroComboBoxUserSearch.TabIndex = 11;
            this.metroComboBoxUserSearch.UseSelectable = true;
            this.metroComboBoxUserSearch.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxUserSearch_SelectedIndexChanged);
            // 
            // metroLabelInput
            // 
            this.metroLabelInput.AutoSize = true;
            this.metroLabelInput.Location = new System.Drawing.Point(26, 64);
            this.metroLabelInput.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabelInput.Name = "metroLabelInput";
            this.metroLabelInput.Size = new System.Drawing.Size(103, 19);
            this.metroLabelInput.TabIndex = 85;
            this.metroLabelInput.Text = "Messages Show:";
            // 
            // metroTextBoxUserSearch
            // 
            // 
            // 
            // 
            this.metroTextBoxUserSearch.CustomButton.Image = null;
            this.metroTextBoxUserSearch.CustomButton.Location = new System.Drawing.Point(438, 2);
            this.metroTextBoxUserSearch.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxUserSearch.CustomButton.Name = "";
            this.metroTextBoxUserSearch.CustomButton.Size = new System.Drawing.Size(37, 37);
            this.metroTextBoxUserSearch.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxUserSearch.CustomButton.TabIndex = 1;
            this.metroTextBoxUserSearch.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxUserSearch.CustomButton.UseSelectable = true;
            this.metroTextBoxUserSearch.CustomButton.Visible = false;
            this.metroTextBoxUserSearch.Lines = new string[0];
            this.metroTextBoxUserSearch.Location = new System.Drawing.Point(26, 168);
            this.metroTextBoxUserSearch.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxUserSearch.MaxLength = 32767;
            this.metroTextBoxUserSearch.Name = "metroTextBoxUserSearch";
            this.metroTextBoxUserSearch.PasswordChar = '\0';
            this.metroTextBoxUserSearch.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxUserSearch.SelectedText = "";
            this.metroTextBoxUserSearch.SelectionLength = 0;
            this.metroTextBoxUserSearch.SelectionStart = 0;
            this.metroTextBoxUserSearch.ShortcutsEnabled = true;
            this.metroTextBoxUserSearch.Size = new System.Drawing.Size(478, 42);
            this.metroTextBoxUserSearch.TabIndex = 83;
            this.metroTextBoxUserSearch.Tag = "Search";
            this.metroTextBoxUserSearch.UseSelectable = true;
            this.metroTextBoxUserSearch.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxUserSearch.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnUserSearch
            // 
            this.btnUserSearch.BackColor = System.Drawing.Color.White;
            this.btnUserSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUserSearch.BackgroundImage")));
            this.btnUserSearch.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnUserSearch.Location = new System.Drawing.Point(26, 256);
            this.btnUserSearch.Margin = new System.Windows.Forms.Padding(6);
            this.btnUserSearch.Name = "btnUserSearch";
            this.btnUserSearch.Size = new System.Drawing.Size(482, 50);
            this.btnUserSearch.TabIndex = 11;
            this.btnUserSearch.Text = "&Search";
            this.btnUserSearch.UseSelectable = true;
            this.btnUserSearch.Click += new System.EventHandler(this.btnUserSearch_Click_1);
            // 
            // metroTabPageMISManager
            // 
            this.metroTabPageMISManager.BackColor = System.Drawing.Color.White;
            this.metroTabPageMISManager.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroTabPageMISManager.BackgroundImage")));
            this.metroTabPageMISManager.Controls.Add(this.groupBox17);
            this.metroTabPageMISManager.Controls.Add(this.groupBox2);
            this.metroTabPageMISManager.Controls.Add(this.groupBox1);
            this.metroTabPageMISManager.HorizontalScrollbarBarColor = true;
            this.metroTabPageMISManager.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageMISManager.HorizontalScrollbarSize = 10;
            this.metroTabPageMISManager.Location = new System.Drawing.Point(4, 37);
            this.metroTabPageMISManager.Margin = new System.Windows.Forms.Padding(4);
            this.metroTabPageMISManager.Name = "metroTabPageMISManager";
            this.metroTabPageMISManager.Size = new System.Drawing.Size(1918, 945);
            this.metroTabPageMISManager.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTabPageMISManager.TabIndex = 1;
            this.metroTabPageMISManager.Text = "MIS Manager";
            this.metroTabPageMISManager.VerticalScrollbarBarColor = true;
            this.metroTabPageMISManager.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageMISManager.VerticalScrollbarSize = 10;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.metroListViewEmployee);
            this.groupBox17.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox17.Location = new System.Drawing.Point(52, 464);
            this.groupBox17.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox17.Size = new System.Drawing.Size(1812, 408);
            this.groupBox17.TabIndex = 83;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "List Employee";
            // 
            // metroListViewEmployee
            // 
            this.metroListViewEmployee.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.metroListViewEmployee.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewEmployee.FullRowSelect = true;
            this.metroListViewEmployee.GridLines = true;
            this.metroListViewEmployee.Location = new System.Drawing.Point(6, 54);
            this.metroListViewEmployee.Margin = new System.Windows.Forms.Padding(4);
            this.metroListViewEmployee.Name = "metroListViewEmployee";
            this.metroListViewEmployee.OwnerDraw = true;
            this.metroListViewEmployee.Size = new System.Drawing.Size(1788, 314);
            this.metroListViewEmployee.TabIndex = 82;
            this.metroListViewEmployee.UseCompatibleStateImageBehavior = false;
            this.metroListViewEmployee.UseSelectable = true;
            this.metroListViewEmployee.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Employee Number";
            this.columnHeader5.Width = 310;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "First Name";
            this.columnHeader6.Width = 257;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Last Name";
            this.columnHeader7.Width = 296;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Job Title";
            this.columnHeader8.Width = 288;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Phone Number";
            this.columnHeader9.Width = 289;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Email";
            this.columnHeader10.Width = 397;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.metroComboBoxMISSearch);
            this.groupBox2.Controls.Add(this.metroLabelMISInput);
            this.groupBox2.Controls.Add(this.metroTextBoxSearchID);
            this.groupBox2.Controls.Add(this.metroButtonEmpSearch);
            this.groupBox2.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(1336, 32);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox2.Size = new System.Drawing.Size(528, 402);
            this.groupBox2.TabIndex = 80;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search By";
            // 
            // metroComboBoxMISSearch
            // 
            this.metroComboBoxMISSearch.FormattingEnabled = true;
            this.metroComboBoxMISSearch.ItemHeight = 23;
            this.metroComboBoxMISSearch.Items.AddRange(new object[] {
            "Employee Number",
            "First Name"});
            this.metroComboBoxMISSearch.Location = new System.Drawing.Point(30, 104);
            this.metroComboBoxMISSearch.Margin = new System.Windows.Forms.Padding(4);
            this.metroComboBoxMISSearch.Name = "metroComboBoxMISSearch";
            this.metroComboBoxMISSearch.Size = new System.Drawing.Size(478, 29);
            this.metroComboBoxMISSearch.TabIndex = 86;
            this.metroComboBoxMISSearch.UseSelectable = true;
            this.metroComboBoxMISSearch.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxMISSearch_SelectedIndexChanged);
            // 
            // metroLabelMISInput
            // 
            this.metroLabelMISInput.AutoSize = true;
            this.metroLabelMISInput.Location = new System.Drawing.Point(30, 64);
            this.metroLabelMISInput.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabelMISInput.Name = "metroLabelMISInput";
            this.metroLabelMISInput.Size = new System.Drawing.Size(103, 19);
            this.metroLabelMISInput.TabIndex = 87;
            this.metroLabelMISInput.Text = "Messages Show:";
            // 
            // metroTextBoxSearchID
            // 
            // 
            // 
            // 
            this.metroTextBoxSearchID.CustomButton.Image = null;
            this.metroTextBoxSearchID.CustomButton.Location = new System.Drawing.Point(438, 2);
            this.metroTextBoxSearchID.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxSearchID.CustomButton.Name = "";
            this.metroTextBoxSearchID.CustomButton.Size = new System.Drawing.Size(37, 37);
            this.metroTextBoxSearchID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxSearchID.CustomButton.TabIndex = 1;
            this.metroTextBoxSearchID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxSearchID.CustomButton.UseSelectable = true;
            this.metroTextBoxSearchID.CustomButton.Visible = false;
            this.metroTextBoxSearchID.Lines = new string[0];
            this.metroTextBoxSearchID.Location = new System.Drawing.Point(30, 184);
            this.metroTextBoxSearchID.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxSearchID.MaxLength = 32767;
            this.metroTextBoxSearchID.Name = "metroTextBoxSearchID";
            this.metroTextBoxSearchID.PasswordChar = '\0';
            this.metroTextBoxSearchID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxSearchID.SelectedText = "";
            this.metroTextBoxSearchID.SelectionLength = 0;
            this.metroTextBoxSearchID.SelectionStart = 0;
            this.metroTextBoxSearchID.ShortcutsEnabled = true;
            this.metroTextBoxSearchID.Size = new System.Drawing.Size(478, 42);
            this.metroTextBoxSearchID.TabIndex = 83;
            this.metroTextBoxSearchID.Tag = "Search";
            this.metroTextBoxSearchID.UseSelectable = true;
            this.metroTextBoxSearchID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxSearchID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonEmpSearch
            // 
            this.metroButtonEmpSearch.BackColor = System.Drawing.Color.White;
            this.metroButtonEmpSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonEmpSearch.BackgroundImage")));
            this.metroButtonEmpSearch.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonEmpSearch.Location = new System.Drawing.Point(26, 262);
            this.metroButtonEmpSearch.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonEmpSearch.Name = "metroButtonEmpSearch";
            this.metroButtonEmpSearch.Size = new System.Drawing.Size(482, 50);
            this.metroButtonEmpSearch.TabIndex = 11;
            this.metroButtonEmpSearch.Text = "&Search";
            this.metroButtonEmpSearch.UseSelectable = true;
            this.metroButtonEmpSearch.Click += new System.EventHandler(this.metroButtonEmpSearch_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.metroLabel9);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroTextBoxEmpPhoneNumber);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.metroTextBoxEmpEmail);
            this.groupBox1.Controls.Add(this.metroTextBoxEmployeeNumber);
            this.groupBox1.Controls.Add(this.metroTextBoxFirstName);
            this.groupBox1.Controls.Add(this.metroTextBoxJobTitle);
            this.groupBox1.Controls.Add(this.metroTextBoxLastName);
            this.groupBox1.Controls.Add(this.metroLabelFN);
            this.groupBox1.Controls.Add(this.metroLabel6);
            this.groupBox1.Controls.Add(this.metroLabel7);
            this.groupBox1.Controls.Add(this.metroLabel8);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(52, 32);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox1.Size = new System.Drawing.Size(1220, 402);
            this.groupBox1.TabIndex = 79;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Employee Info";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(74, 274);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(103, 19);
            this.metroLabel9.TabIndex = 84;
            this.metroLabel9.Text = "Phone Number:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(74, 336);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(44, 19);
            this.metroLabel5.TabIndex = 83;
            this.metroLabel5.Text = "Email:";
            // 
            // metroTextBoxEmpPhoneNumber
            // 
            // 
            // 
            // 
            this.metroTextBoxEmpPhoneNumber.CustomButton.Image = null;
            this.metroTextBoxEmpPhoneNumber.CustomButton.Location = new System.Drawing.Point(438, 2);
            this.metroTextBoxEmpPhoneNumber.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxEmpPhoneNumber.CustomButton.Name = "";
            this.metroTextBoxEmpPhoneNumber.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxEmpPhoneNumber.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxEmpPhoneNumber.CustomButton.TabIndex = 1;
            this.metroTextBoxEmpPhoneNumber.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEmpPhoneNumber.CustomButton.UseSelectable = true;
            this.metroTextBoxEmpPhoneNumber.CustomButton.Visible = false;
            this.metroTextBoxEmpPhoneNumber.Lines = new string[0];
            this.metroTextBoxEmpPhoneNumber.Location = new System.Drawing.Point(74, 306);
            this.metroTextBoxEmpPhoneNumber.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxEmpPhoneNumber.MaxLength = 32767;
            this.metroTextBoxEmpPhoneNumber.Name = "metroTextBoxEmpPhoneNumber";
            this.metroTextBoxEmpPhoneNumber.PasswordChar = '\0';
            this.metroTextBoxEmpPhoneNumber.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxEmpPhoneNumber.SelectedText = "";
            this.metroTextBoxEmpPhoneNumber.SelectionLength = 0;
            this.metroTextBoxEmpPhoneNumber.SelectionStart = 0;
            this.metroTextBoxEmpPhoneNumber.ShortcutsEnabled = true;
            this.metroTextBoxEmpPhoneNumber.Size = new System.Drawing.Size(460, 24);
            this.metroTextBoxEmpPhoneNumber.TabIndex = 82;
            this.metroTextBoxEmpPhoneNumber.UseSelectable = true;
            this.metroTextBoxEmpPhoneNumber.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxEmpPhoneNumber.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.metroButtonEmpList);
            this.groupBox3.Controls.Add(this.metroButtonEmpAdd);
            this.groupBox3.Controls.Add(this.metroButtonEmpUpdate);
            this.groupBox3.Controls.Add(this.metroButtonEmpDelete);
            this.groupBox3.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(686, 22);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox3.Size = new System.Drawing.Size(520, 344);
            this.groupBox3.TabIndex = 81;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Buttom Bar";
            // 
            // metroButtonEmpList
            // 
            this.metroButtonEmpList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonEmpList.BackgroundImage")));
            this.metroButtonEmpList.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonEmpList.Location = new System.Drawing.Point(60, 284);
            this.metroButtonEmpList.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonEmpList.Name = "metroButtonEmpList";
            this.metroButtonEmpList.Size = new System.Drawing.Size(392, 50);
            this.metroButtonEmpList.TabIndex = 79;
            this.metroButtonEmpList.Text = "&List";
            this.metroButtonEmpList.UseSelectable = true;
            this.metroButtonEmpList.Click += new System.EventHandler(this.metroButtonEmpList_Click);
            // 
            // metroButtonEmpAdd
            // 
            this.metroButtonEmpAdd.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.metroButtonEmpAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonEmpAdd.BackgroundImage")));
            this.metroButtonEmpAdd.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonEmpAdd.Location = new System.Drawing.Point(60, 64);
            this.metroButtonEmpAdd.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonEmpAdd.Name = "metroButtonEmpAdd";
            this.metroButtonEmpAdd.Size = new System.Drawing.Size(392, 48);
            this.metroButtonEmpAdd.TabIndex = 75;
            this.metroButtonEmpAdd.Text = "&Add";
            this.metroButtonEmpAdd.UseSelectable = true;
            this.metroButtonEmpAdd.Click += new System.EventHandler(this.metroButtonEmpAdd_Click);
            // 
            // metroButtonEmpUpdate
            // 
            this.metroButtonEmpUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonEmpUpdate.BackgroundImage")));
            this.metroButtonEmpUpdate.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonEmpUpdate.Location = new System.Drawing.Point(60, 132);
            this.metroButtonEmpUpdate.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonEmpUpdate.Name = "metroButtonEmpUpdate";
            this.metroButtonEmpUpdate.Size = new System.Drawing.Size(392, 48);
            this.metroButtonEmpUpdate.TabIndex = 77;
            this.metroButtonEmpUpdate.Text = "&Update";
            this.metroButtonEmpUpdate.UseSelectable = true;
            this.metroButtonEmpUpdate.Click += new System.EventHandler(this.metroButtonEmpUpdate_Click);
            // 
            // metroButtonEmpDelete
            // 
            this.metroButtonEmpDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonEmpDelete.BackgroundImage")));
            this.metroButtonEmpDelete.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonEmpDelete.Location = new System.Drawing.Point(60, 208);
            this.metroButtonEmpDelete.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonEmpDelete.Name = "metroButtonEmpDelete";
            this.metroButtonEmpDelete.Size = new System.Drawing.Size(392, 52);
            this.metroButtonEmpDelete.TabIndex = 76;
            this.metroButtonEmpDelete.Text = "&Delete";
            this.metroButtonEmpDelete.UseSelectable = true;
            this.metroButtonEmpDelete.Click += new System.EventHandler(this.metroButtonEmpDelete_Click);
            // 
            // metroTextBoxEmpEmail
            // 
            // 
            // 
            // 
            this.metroTextBoxEmpEmail.CustomButton.Image = null;
            this.metroTextBoxEmpEmail.CustomButton.Location = new System.Drawing.Point(438, 2);
            this.metroTextBoxEmpEmail.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxEmpEmail.CustomButton.Name = "";
            this.metroTextBoxEmpEmail.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxEmpEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxEmpEmail.CustomButton.TabIndex = 1;
            this.metroTextBoxEmpEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEmpEmail.CustomButton.UseSelectable = true;
            this.metroTextBoxEmpEmail.CustomButton.Visible = false;
            this.metroTextBoxEmpEmail.Lines = new string[0];
            this.metroTextBoxEmpEmail.Location = new System.Drawing.Point(74, 370);
            this.metroTextBoxEmpEmail.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxEmpEmail.MaxLength = 32767;
            this.metroTextBoxEmpEmail.Name = "metroTextBoxEmpEmail";
            this.metroTextBoxEmpEmail.PasswordChar = '\0';
            this.metroTextBoxEmpEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxEmpEmail.SelectedText = "";
            this.metroTextBoxEmpEmail.SelectionLength = 0;
            this.metroTextBoxEmpEmail.SelectionStart = 0;
            this.metroTextBoxEmpEmail.ShortcutsEnabled = true;
            this.metroTextBoxEmpEmail.Size = new System.Drawing.Size(460, 24);
            this.metroTextBoxEmpEmail.TabIndex = 81;
            this.metroTextBoxEmpEmail.UseSelectable = true;
            this.metroTextBoxEmpEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxEmpEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxEmployeeNumber
            // 
            // 
            // 
            // 
            this.metroTextBoxEmployeeNumber.CustomButton.Image = null;
            this.metroTextBoxEmployeeNumber.CustomButton.Location = new System.Drawing.Point(438, 2);
            this.metroTextBoxEmployeeNumber.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxEmployeeNumber.CustomButton.Name = "";
            this.metroTextBoxEmployeeNumber.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxEmployeeNumber.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxEmployeeNumber.CustomButton.TabIndex = 1;
            this.metroTextBoxEmployeeNumber.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEmployeeNumber.CustomButton.UseSelectable = true;
            this.metroTextBoxEmployeeNumber.CustomButton.Visible = false;
            this.metroTextBoxEmployeeNumber.Lines = new string[0];
            this.metroTextBoxEmployeeNumber.Location = new System.Drawing.Point(74, 64);
            this.metroTextBoxEmployeeNumber.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxEmployeeNumber.MaxLength = 32767;
            this.metroTextBoxEmployeeNumber.Name = "metroTextBoxEmployeeNumber";
            this.metroTextBoxEmployeeNumber.PasswordChar = '\0';
            this.metroTextBoxEmployeeNumber.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxEmployeeNumber.SelectedText = "";
            this.metroTextBoxEmployeeNumber.SelectionLength = 0;
            this.metroTextBoxEmployeeNumber.SelectionStart = 0;
            this.metroTextBoxEmployeeNumber.ShortcutsEnabled = true;
            this.metroTextBoxEmployeeNumber.Size = new System.Drawing.Size(460, 24);
            this.metroTextBoxEmployeeNumber.TabIndex = 80;
            this.metroTextBoxEmployeeNumber.Tag = "Search";
            this.metroTextBoxEmployeeNumber.UseSelectable = true;
            this.metroTextBoxEmployeeNumber.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxEmployeeNumber.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxFirstName
            // 
            // 
            // 
            // 
            this.metroTextBoxFirstName.CustomButton.Image = null;
            this.metroTextBoxFirstName.CustomButton.Location = new System.Drawing.Point(438, 2);
            this.metroTextBoxFirstName.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxFirstName.CustomButton.Name = "";
            this.metroTextBoxFirstName.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxFirstName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxFirstName.CustomButton.TabIndex = 1;
            this.metroTextBoxFirstName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxFirstName.CustomButton.UseSelectable = true;
            this.metroTextBoxFirstName.CustomButton.Visible = false;
            this.metroTextBoxFirstName.Lines = new string[0];
            this.metroTextBoxFirstName.Location = new System.Drawing.Point(74, 124);
            this.metroTextBoxFirstName.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxFirstName.MaxLength = 32767;
            this.metroTextBoxFirstName.Name = "metroTextBoxFirstName";
            this.metroTextBoxFirstName.PasswordChar = '\0';
            this.metroTextBoxFirstName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxFirstName.SelectedText = "";
            this.metroTextBoxFirstName.SelectionLength = 0;
            this.metroTextBoxFirstName.SelectionStart = 0;
            this.metroTextBoxFirstName.ShortcutsEnabled = true;
            this.metroTextBoxFirstName.Size = new System.Drawing.Size(460, 24);
            this.metroTextBoxFirstName.TabIndex = 79;
            this.metroTextBoxFirstName.Tag = "Search";
            this.metroTextBoxFirstName.UseSelectable = true;
            this.metroTextBoxFirstName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxFirstName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxJobTitle
            // 
            // 
            // 
            // 
            this.metroTextBoxJobTitle.CustomButton.Image = null;
            this.metroTextBoxJobTitle.CustomButton.Location = new System.Drawing.Point(438, 2);
            this.metroTextBoxJobTitle.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxJobTitle.CustomButton.Name = "";
            this.metroTextBoxJobTitle.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxJobTitle.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxJobTitle.CustomButton.TabIndex = 1;
            this.metroTextBoxJobTitle.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxJobTitle.CustomButton.UseSelectable = true;
            this.metroTextBoxJobTitle.CustomButton.Visible = false;
            this.metroTextBoxJobTitle.Lines = new string[0];
            this.metroTextBoxJobTitle.Location = new System.Drawing.Point(74, 244);
            this.metroTextBoxJobTitle.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxJobTitle.MaxLength = 32767;
            this.metroTextBoxJobTitle.Name = "metroTextBoxJobTitle";
            this.metroTextBoxJobTitle.PasswordChar = '\0';
            this.metroTextBoxJobTitle.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxJobTitle.SelectedText = "";
            this.metroTextBoxJobTitle.SelectionLength = 0;
            this.metroTextBoxJobTitle.SelectionStart = 0;
            this.metroTextBoxJobTitle.ShortcutsEnabled = true;
            this.metroTextBoxJobTitle.Size = new System.Drawing.Size(460, 24);
            this.metroTextBoxJobTitle.TabIndex = 78;
            this.metroTextBoxJobTitle.Tag = "Search";
            this.metroTextBoxJobTitle.UseSelectable = true;
            this.metroTextBoxJobTitle.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxJobTitle.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxLastName
            // 
            // 
            // 
            // 
            this.metroTextBoxLastName.CustomButton.Image = null;
            this.metroTextBoxLastName.CustomButton.Location = new System.Drawing.Point(438, 2);
            this.metroTextBoxLastName.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxLastName.CustomButton.Name = "";
            this.metroTextBoxLastName.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxLastName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxLastName.CustomButton.TabIndex = 1;
            this.metroTextBoxLastName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxLastName.CustomButton.UseSelectable = true;
            this.metroTextBoxLastName.CustomButton.Visible = false;
            this.metroTextBoxLastName.Lines = new string[0];
            this.metroTextBoxLastName.Location = new System.Drawing.Point(74, 184);
            this.metroTextBoxLastName.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxLastName.MaxLength = 32767;
            this.metroTextBoxLastName.Name = "metroTextBoxLastName";
            this.metroTextBoxLastName.PasswordChar = '\0';
            this.metroTextBoxLastName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxLastName.SelectedText = "";
            this.metroTextBoxLastName.SelectionLength = 0;
            this.metroTextBoxLastName.SelectionStart = 0;
            this.metroTextBoxLastName.ShortcutsEnabled = true;
            this.metroTextBoxLastName.Size = new System.Drawing.Size(460, 24);
            this.metroTextBoxLastName.TabIndex = 77;
            this.metroTextBoxLastName.Tag = "Search";
            this.metroTextBoxLastName.UseSelectable = true;
            this.metroTextBoxLastName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxLastName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabelFN
            // 
            this.metroLabelFN.AutoSize = true;
            this.metroLabelFN.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabelFN.Location = new System.Drawing.Point(74, 44);
            this.metroLabelFN.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabelFN.Name = "metroLabelFN";
            this.metroLabelFN.Size = new System.Drawing.Size(105, 15);
            this.metroLabelFN.TabIndex = 76;
            this.metroLabelFN.Text = "Employee Number:";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel6.Location = new System.Drawing.Point(76, 92);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(65, 15);
            this.metroLabel6.TabIndex = 75;
            this.metroLabel6.Text = "First Name:";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel7.Location = new System.Drawing.Point(74, 158);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(67, 15);
            this.metroLabel7.TabIndex = 74;
            this.metroLabel7.Text = "Last Name: ";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel8.Location = new System.Drawing.Point(74, 224);
            this.metroLabel8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(54, 15);
            this.metroLabel8.TabIndex = 73;
            this.metroLabel8.Text = "Job Title: ";
            // 
            // metroTabPageSalesManager
            // 
            this.metroTabPageSalesManager.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroTabPageSalesManager.BackgroundImage")));
            this.metroTabPageSalesManager.Controls.Add(this.groupBox6);
            this.metroTabPageSalesManager.Controls.Add(this.groupBox5);
            this.metroTabPageSalesManager.Controls.Add(this.groupBox4);
            this.metroTabPageSalesManager.HorizontalScrollbarBarColor = true;
            this.metroTabPageSalesManager.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageSalesManager.HorizontalScrollbarSize = 10;
            this.metroTabPageSalesManager.Location = new System.Drawing.Point(4, 37);
            this.metroTabPageSalesManager.Margin = new System.Windows.Forms.Padding(4);
            this.metroTabPageSalesManager.Name = "metroTabPageSalesManager";
            this.metroTabPageSalesManager.Size = new System.Drawing.Size(1918, 945);
            this.metroTabPageSalesManager.TabIndex = 2;
            this.metroTabPageSalesManager.Text = "Sales Manager";
            this.metroTabPageSalesManager.VerticalScrollbarBarColor = true;
            this.metroTabPageSalesManager.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageSalesManager.VerticalScrollbarSize = 10;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.White;
            this.groupBox6.Controls.Add(this.metroComboBoxSaleSearch);
            this.groupBox6.Controls.Add(this.metroLabelSaleInput);
            this.groupBox6.Controls.Add(this.metroButtonClientSearch);
            this.groupBox6.Controls.Add(this.metroTextBoxClientSearchID);
            this.groupBox6.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(1440, 26);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox6.Size = new System.Drawing.Size(400, 404);
            this.groupBox6.TabIndex = 81;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Search By";
            // 
            // metroComboBoxSaleSearch
            // 
            this.metroComboBoxSaleSearch.FormattingEnabled = true;
            this.metroComboBoxSaleSearch.ItemHeight = 23;
            this.metroComboBoxSaleSearch.Items.AddRange(new object[] {
            "Client Id",
            "Client Name"});
            this.metroComboBoxSaleSearch.Location = new System.Drawing.Point(32, 132);
            this.metroComboBoxSaleSearch.Margin = new System.Windows.Forms.Padding(4);
            this.metroComboBoxSaleSearch.Name = "metroComboBoxSaleSearch";
            this.metroComboBoxSaleSearch.Size = new System.Drawing.Size(328, 29);
            this.metroComboBoxSaleSearch.TabIndex = 88;
            this.metroComboBoxSaleSearch.UseSelectable = true;
            this.metroComboBoxSaleSearch.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxSaleSearch_SelectedIndexChanged);
            // 
            // metroLabelSaleInput
            // 
            this.metroLabelSaleInput.AutoSize = true;
            this.metroLabelSaleInput.Location = new System.Drawing.Point(32, 92);
            this.metroLabelSaleInput.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabelSaleInput.Name = "metroLabelSaleInput";
            this.metroLabelSaleInput.Size = new System.Drawing.Size(103, 19);
            this.metroLabelSaleInput.TabIndex = 89;
            this.metroLabelSaleInput.Text = "Messages Show:";
            // 
            // metroButtonClientSearch
            // 
            this.metroButtonClientSearch.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.metroButtonClientSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonClientSearch.BackgroundImage")));
            this.metroButtonClientSearch.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonClientSearch.Location = new System.Drawing.Point(32, 276);
            this.metroButtonClientSearch.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonClientSearch.Name = "metroButtonClientSearch";
            this.metroButtonClientSearch.Size = new System.Drawing.Size(328, 48);
            this.metroButtonClientSearch.TabIndex = 84;
            this.metroButtonClientSearch.Text = "&Search";
            this.metroButtonClientSearch.UseSelectable = true;
            this.metroButtonClientSearch.Click += new System.EventHandler(this.metroButtonClientSearch_Click);
            // 
            // metroTextBoxClientSearchID
            // 
            // 
            // 
            // 
            this.metroTextBoxClientSearchID.CustomButton.Image = null;
            this.metroTextBoxClientSearchID.CustomButton.Location = new System.Drawing.Point(288, 2);
            this.metroTextBoxClientSearchID.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientSearchID.CustomButton.Name = "";
            this.metroTextBoxClientSearchID.CustomButton.Size = new System.Drawing.Size(37, 37);
            this.metroTextBoxClientSearchID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClientSearchID.CustomButton.TabIndex = 1;
            this.metroTextBoxClientSearchID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClientSearchID.CustomButton.UseSelectable = true;
            this.metroTextBoxClientSearchID.CustomButton.Visible = false;
            this.metroTextBoxClientSearchID.Lines = new string[0];
            this.metroTextBoxClientSearchID.Location = new System.Drawing.Point(32, 192);
            this.metroTextBoxClientSearchID.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxClientSearchID.MaxLength = 32767;
            this.metroTextBoxClientSearchID.Name = "metroTextBoxClientSearchID";
            this.metroTextBoxClientSearchID.PasswordChar = '\0';
            this.metroTextBoxClientSearchID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClientSearchID.SelectedText = "";
            this.metroTextBoxClientSearchID.SelectionLength = 0;
            this.metroTextBoxClientSearchID.SelectionStart = 0;
            this.metroTextBoxClientSearchID.ShortcutsEnabled = true;
            this.metroTextBoxClientSearchID.Size = new System.Drawing.Size(328, 42);
            this.metroTextBoxClientSearchID.TabIndex = 83;
            this.metroTextBoxClientSearchID.Tag = "Search";
            this.metroTextBoxClientSearchID.UseSelectable = true;
            this.metroTextBoxClientSearchID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClientSearchID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox5.Controls.Add(this.metroLabel19);
            this.groupBox5.Controls.Add(this.metroLabel18);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.metroLabel17);
            this.groupBox5.Controls.Add(this.metroLabel16);
            this.groupBox5.Controls.Add(this.metroTextBoxClientFax);
            this.groupBox5.Controls.Add(this.metroTextBoxClientCredit);
            this.groupBox5.Controls.Add(this.metroTextBoxClientPostCode);
            this.groupBox5.Controls.Add(this.metroTextBoxClientStreet);
            this.groupBox5.Controls.Add(this.metroLabel10);
            this.groupBox5.Controls.Add(this.metroTextBoxClientCity);
            this.groupBox5.Controls.Add(this.metroTextBoxClientID);
            this.groupBox5.Controls.Add(this.metroTextBoxClientName);
            this.groupBox5.Controls.Add(this.metroTextBoxClientEmail);
            this.groupBox5.Controls.Add(this.metroTextBoxClientPhoneNumber);
            this.groupBox5.Controls.Add(this.metroLabel12);
            this.groupBox5.Controls.Add(this.metroLabel13);
            this.groupBox5.Controls.Add(this.metroLabel14);
            this.groupBox5.Controls.Add(this.metroLabel15);
            this.groupBox5.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(60, 26);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox5.Size = new System.Drawing.Size(1328, 404);
            this.groupBox5.TabIndex = 80;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Client Info";
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(502, 192);
            this.metroLabel19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(72, 19);
            this.metroLabel19.TabIndex = 92;
            this.metroLabel19.Text = "Post Code:";
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(38, 320);
            this.metroLabel18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(31, 19);
            this.metroLabel18.TabIndex = 91;
            this.metroLabel18.Text = "Fax:";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.White;
            this.groupBox7.Controls.Add(this.metroButtonClientList);
            this.groupBox7.Controls.Add(this.metroButtonClientAdd);
            this.groupBox7.Controls.Add(this.metroButtonClientUpdate);
            this.groupBox7.Controls.Add(this.metroButtonClientDelete);
            this.groupBox7.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(960, 28);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox7.Size = new System.Drawing.Size(356, 354);
            this.groupBox7.TabIndex = 82;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Buttom Bar";
            // 
            // metroButtonClientList
            // 
            this.metroButtonClientList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonClientList.BackgroundImage")));
            this.metroButtonClientList.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonClientList.Location = new System.Drawing.Point(56, 284);
            this.metroButtonClientList.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonClientList.Name = "metroButtonClientList";
            this.metroButtonClientList.Size = new System.Drawing.Size(252, 50);
            this.metroButtonClientList.TabIndex = 79;
            this.metroButtonClientList.Text = "&List";
            this.metroButtonClientList.UseSelectable = true;
            this.metroButtonClientList.Click += new System.EventHandler(this.metroButtonClientList_Click);
            // 
            // metroButtonClientAdd
            // 
            this.metroButtonClientAdd.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.metroButtonClientAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonClientAdd.BackgroundImage")));
            this.metroButtonClientAdd.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonClientAdd.Location = new System.Drawing.Point(56, 64);
            this.metroButtonClientAdd.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonClientAdd.Name = "metroButtonClientAdd";
            this.metroButtonClientAdd.Size = new System.Drawing.Size(252, 48);
            this.metroButtonClientAdd.TabIndex = 75;
            this.metroButtonClientAdd.Text = "&Add";
            this.metroButtonClientAdd.UseSelectable = true;
            this.metroButtonClientAdd.Click += new System.EventHandler(this.metroButtonClientAdd_Click);
            // 
            // metroButtonClientUpdate
            // 
            this.metroButtonClientUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonClientUpdate.BackgroundImage")));
            this.metroButtonClientUpdate.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonClientUpdate.Location = new System.Drawing.Point(56, 132);
            this.metroButtonClientUpdate.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonClientUpdate.Name = "metroButtonClientUpdate";
            this.metroButtonClientUpdate.Size = new System.Drawing.Size(252, 48);
            this.metroButtonClientUpdate.TabIndex = 77;
            this.metroButtonClientUpdate.Text = "&Update";
            this.metroButtonClientUpdate.UseSelectable = true;
            this.metroButtonClientUpdate.Click += new System.EventHandler(this.metroButtonClientUpdate_Click);
            // 
            // metroButtonClientDelete
            // 
            this.metroButtonClientDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonClientDelete.BackgroundImage")));
            this.metroButtonClientDelete.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonClientDelete.Location = new System.Drawing.Point(56, 208);
            this.metroButtonClientDelete.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonClientDelete.Name = "metroButtonClientDelete";
            this.metroButtonClientDelete.Size = new System.Drawing.Size(252, 52);
            this.metroButtonClientDelete.TabIndex = 76;
            this.metroButtonClientDelete.Text = "&Delete";
            this.metroButtonClientDelete.UseSelectable = true;
            this.metroButtonClientDelete.Click += new System.EventHandler(this.metroButtonClientDelete_Click);
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(502, 252);
            this.metroLabel17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(48, 19);
            this.metroLabel17.TabIndex = 90;
            this.metroLabel17.Text = "Credit:";
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(504, 120);
            this.metroLabel16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(83, 19);
            this.metroLabel16.TabIndex = 89;
            this.metroLabel16.Text = "Client Street:";
            // 
            // metroTextBoxClientFax
            // 
            // 
            // 
            // 
            this.metroTextBoxClientFax.CustomButton.Image = null;
            this.metroTextBoxClientFax.CustomButton.Location = new System.Drawing.Point(354, 2);
            this.metroTextBoxClientFax.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientFax.CustomButton.Name = "";
            this.metroTextBoxClientFax.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxClientFax.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClientFax.CustomButton.TabIndex = 1;
            this.metroTextBoxClientFax.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClientFax.CustomButton.UseSelectable = true;
            this.metroTextBoxClientFax.CustomButton.Visible = false;
            this.metroTextBoxClientFax.Lines = new string[0];
            this.metroTextBoxClientFax.Location = new System.Drawing.Point(38, 358);
            this.metroTextBoxClientFax.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientFax.MaxLength = 32767;
            this.metroTextBoxClientFax.Name = "metroTextBoxClientFax";
            this.metroTextBoxClientFax.PasswordChar = '\0';
            this.metroTextBoxClientFax.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClientFax.SelectedText = "";
            this.metroTextBoxClientFax.SelectionLength = 0;
            this.metroTextBoxClientFax.SelectionStart = 0;
            this.metroTextBoxClientFax.ShortcutsEnabled = true;
            this.metroTextBoxClientFax.Size = new System.Drawing.Size(376, 24);
            this.metroTextBoxClientFax.TabIndex = 88;
            this.metroTextBoxClientFax.UseSelectable = true;
            this.metroTextBoxClientFax.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClientFax.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxClientCredit
            // 
            // 
            // 
            // 
            this.metroTextBoxClientCredit.CustomButton.Image = null;
            this.metroTextBoxClientCredit.CustomButton.Location = new System.Drawing.Point(360, 2);
            this.metroTextBoxClientCredit.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientCredit.CustomButton.Name = "";
            this.metroTextBoxClientCredit.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxClientCredit.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClientCredit.CustomButton.TabIndex = 1;
            this.metroTextBoxClientCredit.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClientCredit.CustomButton.UseSelectable = true;
            this.metroTextBoxClientCredit.CustomButton.Visible = false;
            this.metroTextBoxClientCredit.Lines = new string[0];
            this.metroTextBoxClientCredit.Location = new System.Drawing.Point(504, 292);
            this.metroTextBoxClientCredit.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientCredit.MaxLength = 32767;
            this.metroTextBoxClientCredit.Name = "metroTextBoxClientCredit";
            this.metroTextBoxClientCredit.PasswordChar = '\0';
            this.metroTextBoxClientCredit.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClientCredit.SelectedText = "";
            this.metroTextBoxClientCredit.SelectionLength = 0;
            this.metroTextBoxClientCredit.SelectionStart = 0;
            this.metroTextBoxClientCredit.ShortcutsEnabled = true;
            this.metroTextBoxClientCredit.Size = new System.Drawing.Size(382, 24);
            this.metroTextBoxClientCredit.TabIndex = 87;
            this.metroTextBoxClientCredit.UseSelectable = true;
            this.metroTextBoxClientCredit.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClientCredit.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxClientPostCode
            // 
            // 
            // 
            // 
            this.metroTextBoxClientPostCode.CustomButton.Image = null;
            this.metroTextBoxClientPostCode.CustomButton.Location = new System.Drawing.Point(360, 2);
            this.metroTextBoxClientPostCode.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientPostCode.CustomButton.Name = "";
            this.metroTextBoxClientPostCode.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxClientPostCode.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClientPostCode.CustomButton.TabIndex = 1;
            this.metroTextBoxClientPostCode.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClientPostCode.CustomButton.UseSelectable = true;
            this.metroTextBoxClientPostCode.CustomButton.Visible = false;
            this.metroTextBoxClientPostCode.Lines = new string[0];
            this.metroTextBoxClientPostCode.Location = new System.Drawing.Point(502, 226);
            this.metroTextBoxClientPostCode.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientPostCode.MaxLength = 32767;
            this.metroTextBoxClientPostCode.Name = "metroTextBoxClientPostCode";
            this.metroTextBoxClientPostCode.PasswordChar = '\0';
            this.metroTextBoxClientPostCode.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClientPostCode.SelectedText = "";
            this.metroTextBoxClientPostCode.SelectionLength = 0;
            this.metroTextBoxClientPostCode.SelectionStart = 0;
            this.metroTextBoxClientPostCode.ShortcutsEnabled = true;
            this.metroTextBoxClientPostCode.Size = new System.Drawing.Size(382, 24);
            this.metroTextBoxClientPostCode.TabIndex = 86;
            this.metroTextBoxClientPostCode.UseSelectable = true;
            this.metroTextBoxClientPostCode.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClientPostCode.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxClientStreet
            // 
            // 
            // 
            // 
            this.metroTextBoxClientStreet.CustomButton.Image = null;
            this.metroTextBoxClientStreet.CustomButton.Location = new System.Drawing.Point(360, 2);
            this.metroTextBoxClientStreet.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientStreet.CustomButton.Name = "";
            this.metroTextBoxClientStreet.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxClientStreet.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClientStreet.CustomButton.TabIndex = 1;
            this.metroTextBoxClientStreet.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClientStreet.CustomButton.UseSelectable = true;
            this.metroTextBoxClientStreet.CustomButton.Visible = false;
            this.metroTextBoxClientStreet.Lines = new string[0];
            this.metroTextBoxClientStreet.Location = new System.Drawing.Point(504, 148);
            this.metroTextBoxClientStreet.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientStreet.MaxLength = 32767;
            this.metroTextBoxClientStreet.Name = "metroTextBoxClientStreet";
            this.metroTextBoxClientStreet.PasswordChar = '\0';
            this.metroTextBoxClientStreet.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClientStreet.SelectedText = "";
            this.metroTextBoxClientStreet.SelectionLength = 0;
            this.metroTextBoxClientStreet.SelectionStart = 0;
            this.metroTextBoxClientStreet.ShortcutsEnabled = true;
            this.metroTextBoxClientStreet.Size = new System.Drawing.Size(382, 24);
            this.metroTextBoxClientStreet.TabIndex = 85;
            this.metroTextBoxClientStreet.UseSelectable = true;
            this.metroTextBoxClientStreet.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClientStreet.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(512, 60);
            this.metroLabel10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(37, 19);
            this.metroLabel10.TabIndex = 84;
            this.metroLabel10.Text = "City::";
            // 
            // metroTextBoxClientCity
            // 
            // 
            // 
            // 
            this.metroTextBoxClientCity.CustomButton.Image = null;
            this.metroTextBoxClientCity.CustomButton.Location = new System.Drawing.Point(360, 2);
            this.metroTextBoxClientCity.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientCity.CustomButton.Name = "";
            this.metroTextBoxClientCity.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxClientCity.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClientCity.CustomButton.TabIndex = 1;
            this.metroTextBoxClientCity.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClientCity.CustomButton.UseSelectable = true;
            this.metroTextBoxClientCity.CustomButton.Visible = false;
            this.metroTextBoxClientCity.Lines = new string[0];
            this.metroTextBoxClientCity.Location = new System.Drawing.Point(504, 80);
            this.metroTextBoxClientCity.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientCity.MaxLength = 32767;
            this.metroTextBoxClientCity.Name = "metroTextBoxClientCity";
            this.metroTextBoxClientCity.PasswordChar = '\0';
            this.metroTextBoxClientCity.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClientCity.SelectedText = "";
            this.metroTextBoxClientCity.SelectionLength = 0;
            this.metroTextBoxClientCity.SelectionStart = 0;
            this.metroTextBoxClientCity.ShortcutsEnabled = true;
            this.metroTextBoxClientCity.Size = new System.Drawing.Size(382, 24);
            this.metroTextBoxClientCity.TabIndex = 82;
            this.metroTextBoxClientCity.UseSelectable = true;
            this.metroTextBoxClientCity.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClientCity.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxClientID
            // 
            // 
            // 
            // 
            this.metroTextBoxClientID.CustomButton.Image = null;
            this.metroTextBoxClientID.CustomButton.Location = new System.Drawing.Point(354, 2);
            this.metroTextBoxClientID.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientID.CustomButton.Name = "";
            this.metroTextBoxClientID.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxClientID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClientID.CustomButton.TabIndex = 1;
            this.metroTextBoxClientID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClientID.CustomButton.UseSelectable = true;
            this.metroTextBoxClientID.CustomButton.Visible = false;
            this.metroTextBoxClientID.Lines = new string[0];
            this.metroTextBoxClientID.Location = new System.Drawing.Point(38, 80);
            this.metroTextBoxClientID.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxClientID.MaxLength = 32767;
            this.metroTextBoxClientID.Name = "metroTextBoxClientID";
            this.metroTextBoxClientID.PasswordChar = '\0';
            this.metroTextBoxClientID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClientID.SelectedText = "";
            this.metroTextBoxClientID.SelectionLength = 0;
            this.metroTextBoxClientID.SelectionStart = 0;
            this.metroTextBoxClientID.ShortcutsEnabled = true;
            this.metroTextBoxClientID.Size = new System.Drawing.Size(376, 24);
            this.metroTextBoxClientID.TabIndex = 80;
            this.metroTextBoxClientID.Tag = "Search";
            this.metroTextBoxClientID.UseSelectable = true;
            this.metroTextBoxClientID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClientID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxClientName
            // 
            // 
            // 
            // 
            this.metroTextBoxClientName.CustomButton.Image = null;
            this.metroTextBoxClientName.CustomButton.Location = new System.Drawing.Point(354, 2);
            this.metroTextBoxClientName.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientName.CustomButton.Name = "";
            this.metroTextBoxClientName.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxClientName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClientName.CustomButton.TabIndex = 1;
            this.metroTextBoxClientName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClientName.CustomButton.UseSelectable = true;
            this.metroTextBoxClientName.CustomButton.Visible = false;
            this.metroTextBoxClientName.Lines = new string[0];
            this.metroTextBoxClientName.Location = new System.Drawing.Point(38, 148);
            this.metroTextBoxClientName.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxClientName.MaxLength = 32767;
            this.metroTextBoxClientName.Name = "metroTextBoxClientName";
            this.metroTextBoxClientName.PasswordChar = '\0';
            this.metroTextBoxClientName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClientName.SelectedText = "";
            this.metroTextBoxClientName.SelectionLength = 0;
            this.metroTextBoxClientName.SelectionStart = 0;
            this.metroTextBoxClientName.ShortcutsEnabled = true;
            this.metroTextBoxClientName.Size = new System.Drawing.Size(376, 24);
            this.metroTextBoxClientName.TabIndex = 79;
            this.metroTextBoxClientName.Tag = "Search";
            this.metroTextBoxClientName.UseSelectable = true;
            this.metroTextBoxClientName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClientName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxClientEmail
            // 
            // 
            // 
            // 
            this.metroTextBoxClientEmail.CustomButton.Image = null;
            this.metroTextBoxClientEmail.CustomButton.Location = new System.Drawing.Point(354, 2);
            this.metroTextBoxClientEmail.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientEmail.CustomButton.Name = "";
            this.metroTextBoxClientEmail.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxClientEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClientEmail.CustomButton.TabIndex = 1;
            this.metroTextBoxClientEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClientEmail.CustomButton.UseSelectable = true;
            this.metroTextBoxClientEmail.CustomButton.Visible = false;
            this.metroTextBoxClientEmail.Lines = new string[0];
            this.metroTextBoxClientEmail.Location = new System.Drawing.Point(38, 292);
            this.metroTextBoxClientEmail.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxClientEmail.MaxLength = 32767;
            this.metroTextBoxClientEmail.Name = "metroTextBoxClientEmail";
            this.metroTextBoxClientEmail.PasswordChar = '\0';
            this.metroTextBoxClientEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClientEmail.SelectedText = "";
            this.metroTextBoxClientEmail.SelectionLength = 0;
            this.metroTextBoxClientEmail.SelectionStart = 0;
            this.metroTextBoxClientEmail.ShortcutsEnabled = true;
            this.metroTextBoxClientEmail.Size = new System.Drawing.Size(376, 24);
            this.metroTextBoxClientEmail.TabIndex = 78;
            this.metroTextBoxClientEmail.Tag = "Search";
            this.metroTextBoxClientEmail.UseSelectable = true;
            this.metroTextBoxClientEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClientEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxClientPhoneNumber
            // 
            // 
            // 
            // 
            this.metroTextBoxClientPhoneNumber.CustomButton.Image = null;
            this.metroTextBoxClientPhoneNumber.CustomButton.Location = new System.Drawing.Point(354, 2);
            this.metroTextBoxClientPhoneNumber.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxClientPhoneNumber.CustomButton.Name = "";
            this.metroTextBoxClientPhoneNumber.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxClientPhoneNumber.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClientPhoneNumber.CustomButton.TabIndex = 1;
            this.metroTextBoxClientPhoneNumber.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClientPhoneNumber.CustomButton.UseSelectable = true;
            this.metroTextBoxClientPhoneNumber.CustomButton.Visible = false;
            this.metroTextBoxClientPhoneNumber.Lines = new string[0];
            this.metroTextBoxClientPhoneNumber.Location = new System.Drawing.Point(38, 228);
            this.metroTextBoxClientPhoneNumber.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxClientPhoneNumber.MaxLength = 32767;
            this.metroTextBoxClientPhoneNumber.Name = "metroTextBoxClientPhoneNumber";
            this.metroTextBoxClientPhoneNumber.PasswordChar = '\0';
            this.metroTextBoxClientPhoneNumber.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClientPhoneNumber.SelectedText = "";
            this.metroTextBoxClientPhoneNumber.SelectionLength = 0;
            this.metroTextBoxClientPhoneNumber.SelectionStart = 0;
            this.metroTextBoxClientPhoneNumber.ShortcutsEnabled = true;
            this.metroTextBoxClientPhoneNumber.Size = new System.Drawing.Size(376, 24);
            this.metroTextBoxClientPhoneNumber.TabIndex = 77;
            this.metroTextBoxClientPhoneNumber.Tag = "Search";
            this.metroTextBoxClientPhoneNumber.UseSelectable = true;
            this.metroTextBoxClientPhoneNumber.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClientPhoneNumber.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel12.Location = new System.Drawing.Point(38, 44);
            this.metroLabel12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(51, 15);
            this.metroLabel12.TabIndex = 76;
            this.metroLabel12.Text = "Client ID:";
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel13.Location = new System.Drawing.Point(38, 116);
            this.metroLabel13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(71, 15);
            this.metroLabel13.TabIndex = 75;
            this.metroLabel13.Text = "Client Name:";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel14.Location = new System.Drawing.Point(38, 192);
            this.metroLabel14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(91, 15);
            this.metroLabel14.TabIndex = 74;
            this.metroLabel14.Text = "Phone Number: ";
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel15.Location = new System.Drawing.Point(38, 256);
            this.metroLabel15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(39, 15);
            this.metroLabel15.TabIndex = 73;
            this.metroLabel15.Text = "Email: ";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.White;
            this.groupBox4.Controls.Add(this.dataGridViewClients);
            this.groupBox4.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(50, 456);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(1808, 426);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "List Clients";
            // 
            // dataGridViewClients
            // 
            this.dataGridViewClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewClients.Location = new System.Drawing.Point(20, 60);
            this.dataGridViewClients.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewClients.Name = "dataGridViewClients";
            this.dataGridViewClients.RowTemplate.Height = 37;
            this.dataGridViewClients.Size = new System.Drawing.Size(1768, 324);
            this.dataGridViewClients.TabIndex = 0;
            // 
            // metroTabPageOrderClerks
            // 
            this.metroTabPageOrderClerks.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroTabPageOrderClerks.BackgroundImage")));
            this.metroTabPageOrderClerks.Controls.Add(this.groupBox10);
            this.metroTabPageOrderClerks.Controls.Add(this.groupBox12);
            this.metroTabPageOrderClerks.Controls.Add(this.groupBox9);
            this.metroTabPageOrderClerks.HorizontalScrollbarBarColor = true;
            this.metroTabPageOrderClerks.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageOrderClerks.HorizontalScrollbarSize = 10;
            this.metroTabPageOrderClerks.Location = new System.Drawing.Point(4, 37);
            this.metroTabPageOrderClerks.Margin = new System.Windows.Forms.Padding(4);
            this.metroTabPageOrderClerks.Name = "metroTabPageOrderClerks";
            this.metroTabPageOrderClerks.Size = new System.Drawing.Size(1918, 945);
            this.metroTabPageOrderClerks.TabIndex = 3;
            this.metroTabPageOrderClerks.Text = "Order Clerks";
            this.metroTabPageOrderClerks.VerticalScrollbarBarColor = true;
            this.metroTabPageOrderClerks.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageOrderClerks.VerticalScrollbarSize = 10;
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.White;
            this.groupBox10.Controls.Add(this.metroComboBoxOrderSearch);
            this.groupBox10.Controls.Add(this.metroLabelOrderInput);
            this.groupBox10.Controls.Add(this.metroButtonOrderSearch);
            this.groupBox10.Controls.Add(this.metroTextBoxOrderSearchId);
            this.groupBox10.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(1456, 24);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox10.Size = new System.Drawing.Size(432, 440);
            this.groupBox10.TabIndex = 82;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Search By";
            // 
            // metroComboBoxOrderSearch
            // 
            this.metroComboBoxOrderSearch.FormattingEnabled = true;
            this.metroComboBoxOrderSearch.ItemHeight = 23;
            this.metroComboBoxOrderSearch.Items.AddRange(new object[] {
            "Order ID"});
            this.metroComboBoxOrderSearch.Location = new System.Drawing.Point(40, 100);
            this.metroComboBoxOrderSearch.Margin = new System.Windows.Forms.Padding(4);
            this.metroComboBoxOrderSearch.Name = "metroComboBoxOrderSearch";
            this.metroComboBoxOrderSearch.Size = new System.Drawing.Size(328, 29);
            this.metroComboBoxOrderSearch.TabIndex = 90;
            this.metroComboBoxOrderSearch.UseSelectable = true;
            this.metroComboBoxOrderSearch.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxOrderSearch_SelectedIndexChanged);
            // 
            // metroLabelOrderInput
            // 
            this.metroLabelOrderInput.AutoSize = true;
            this.metroLabelOrderInput.Location = new System.Drawing.Point(40, 58);
            this.metroLabelOrderInput.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabelOrderInput.Name = "metroLabelOrderInput";
            this.metroLabelOrderInput.Size = new System.Drawing.Size(103, 19);
            this.metroLabelOrderInput.TabIndex = 91;
            this.metroLabelOrderInput.Text = "Messages Show:";
            // 
            // metroButtonOrderSearch
            // 
            this.metroButtonOrderSearch.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.metroButtonOrderSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonOrderSearch.BackgroundImage")));
            this.metroButtonOrderSearch.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonOrderSearch.Location = new System.Drawing.Point(36, 270);
            this.metroButtonOrderSearch.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonOrderSearch.Name = "metroButtonOrderSearch";
            this.metroButtonOrderSearch.Size = new System.Drawing.Size(328, 48);
            this.metroButtonOrderSearch.TabIndex = 85;
            this.metroButtonOrderSearch.Text = "&Search";
            this.metroButtonOrderSearch.UseSelectable = true;
            this.metroButtonOrderSearch.Click += new System.EventHandler(this.metroButtonOrderSearch_Click);
            // 
            // metroTextBoxOrderSearchId
            // 
            // 
            // 
            // 
            this.metroTextBoxOrderSearchId.CustomButton.Image = null;
            this.metroTextBoxOrderSearchId.CustomButton.Location = new System.Drawing.Point(288, 2);
            this.metroTextBoxOrderSearchId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOrderSearchId.CustomButton.Name = "";
            this.metroTextBoxOrderSearchId.CustomButton.Size = new System.Drawing.Size(37, 37);
            this.metroTextBoxOrderSearchId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxOrderSearchId.CustomButton.TabIndex = 1;
            this.metroTextBoxOrderSearchId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxOrderSearchId.CustomButton.UseSelectable = true;
            this.metroTextBoxOrderSearchId.CustomButton.Visible = false;
            this.metroTextBoxOrderSearchId.Lines = new string[0];
            this.metroTextBoxOrderSearchId.Location = new System.Drawing.Point(40, 176);
            this.metroTextBoxOrderSearchId.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxOrderSearchId.MaxLength = 32767;
            this.metroTextBoxOrderSearchId.Name = "metroTextBoxOrderSearchId";
            this.metroTextBoxOrderSearchId.PasswordChar = '\0';
            this.metroTextBoxOrderSearchId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxOrderSearchId.SelectedText = "";
            this.metroTextBoxOrderSearchId.SelectionLength = 0;
            this.metroTextBoxOrderSearchId.SelectionStart = 0;
            this.metroTextBoxOrderSearchId.ShortcutsEnabled = true;
            this.metroTextBoxOrderSearchId.Size = new System.Drawing.Size(328, 42);
            this.metroTextBoxOrderSearchId.TabIndex = 83;
            this.metroTextBoxOrderSearchId.Tag = "Search";
            this.metroTextBoxOrderSearchId.UseSelectable = true;
            this.metroTextBoxOrderSearchId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxOrderSearchId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox12.Controls.Add(this.metroListViewOrder);
            this.groupBox12.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox12.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.Location = new System.Drawing.Point(48, 488);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox12.Size = new System.Drawing.Size(1848, 396);
            this.groupBox12.TabIndex = 84;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "List Orders";
            // 
            // metroListViewOrder
            // 
            this.metroListViewOrder.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader26,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20});
            this.metroListViewOrder.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewOrder.FullRowSelect = true;
            this.metroListViewOrder.GridLines = true;
            this.metroListViewOrder.Location = new System.Drawing.Point(4, 44);
            this.metroListViewOrder.Margin = new System.Windows.Forms.Padding(4);
            this.metroListViewOrder.Name = "metroListViewOrder";
            this.metroListViewOrder.OwnerDraw = true;
            this.metroListViewOrder.Size = new System.Drawing.Size(1836, 324);
            this.metroListViewOrder.TabIndex = 81;
            this.metroListViewOrder.UseCompatibleStateImageBehavior = false;
            this.metroListViewOrder.UseSelectable = true;
            this.metroListViewOrder.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Order ID";
            this.columnHeader26.Width = 143;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Client ID";
            this.columnHeader12.Width = 172;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Item ID";
            this.columnHeader13.Width = 161;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Item Name";
            this.columnHeader14.Width = 211;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Unit Price";
            this.columnHeader15.Width = 196;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Quantity";
            this.columnHeader16.Width = 179;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Payment";
            this.columnHeader17.Width = 201;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Order Date";
            this.columnHeader18.Width = 185;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Shipping Date";
            this.columnHeader19.Width = 181;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Total Order";
            this.columnHeader20.Width = 210;
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox9.Controls.Add(this.metroTextBoxOrderShippingDate);
            this.groupBox9.Controls.Add(this.metroTextBoxOrderDate);
            this.groupBox9.Controls.Add(this.metroTextBoxOrderId);
            this.groupBox9.Controls.Add(this.metroLabel33);
            this.groupBox9.Controls.Add(this.groupBox11);
            this.groupBox9.Controls.Add(this.metroLabel20);
            this.groupBox9.Controls.Add(this.metroLabel21);
            this.groupBox9.Controls.Add(this.metroLabel22);
            this.groupBox9.Controls.Add(this.metroLabel);
            this.groupBox9.Controls.Add(this.metroTextBoxOderQuantity);
            this.groupBox9.Controls.Add(this.metroTextBoxOrderTotal);
            this.groupBox9.Controls.Add(this.metroLabel24);
            this.groupBox9.Controls.Add(this.metroTextBoxOdrtPayment);
            this.groupBox9.Controls.Add(this.metroTextBoxOderClientID);
            this.groupBox9.Controls.Add(this.metroTextBoxOderItemId);
            this.groupBox9.Controls.Add(this.metroTextBoxOderUnitPrice);
            this.groupBox9.Controls.Add(this.metroTextBoxOderItemName);
            this.groupBox9.Controls.Add(this.metroLabel25);
            this.groupBox9.Controls.Add(this.metroLabel26);
            this.groupBox9.Controls.Add(this.metroLabel27);
            this.groupBox9.Controls.Add(this.metroLabel28);
            this.groupBox9.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(48, 24);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox9.Size = new System.Drawing.Size(1376, 440);
            this.groupBox9.TabIndex = 81;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Order Info";
            // 
            // metroTextBoxOrderShippingDate
            // 
            // 
            // 
            // 
            this.metroTextBoxOrderShippingDate.CustomButton.Image = null;
            this.metroTextBoxOrderShippingDate.CustomButton.Location = new System.Drawing.Point(360, 2);
            this.metroTextBoxOrderShippingDate.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOrderShippingDate.CustomButton.Name = "";
            this.metroTextBoxOrderShippingDate.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxOrderShippingDate.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxOrderShippingDate.CustomButton.TabIndex = 1;
            this.metroTextBoxOrderShippingDate.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxOrderShippingDate.CustomButton.UseSelectable = true;
            this.metroTextBoxOrderShippingDate.CustomButton.Visible = false;
            this.metroTextBoxOrderShippingDate.Lines = new string[0];
            this.metroTextBoxOrderShippingDate.Location = new System.Drawing.Point(504, 252);
            this.metroTextBoxOrderShippingDate.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOrderShippingDate.MaxLength = 32767;
            this.metroTextBoxOrderShippingDate.Name = "metroTextBoxOrderShippingDate";
            this.metroTextBoxOrderShippingDate.PasswordChar = '\0';
            this.metroTextBoxOrderShippingDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxOrderShippingDate.SelectedText = "";
            this.metroTextBoxOrderShippingDate.SelectionLength = 0;
            this.metroTextBoxOrderShippingDate.SelectionStart = 0;
            this.metroTextBoxOrderShippingDate.ShortcutsEnabled = true;
            this.metroTextBoxOrderShippingDate.Size = new System.Drawing.Size(382, 24);
            this.metroTextBoxOrderShippingDate.TabIndex = 98;
            this.metroTextBoxOrderShippingDate.UseSelectable = true;
            this.metroTextBoxOrderShippingDate.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxOrderShippingDate.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxOrderDate
            // 
            // 
            // 
            // 
            this.metroTextBoxOrderDate.CustomButton.Image = null;
            this.metroTextBoxOrderDate.CustomButton.Location = new System.Drawing.Point(360, 2);
            this.metroTextBoxOrderDate.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOrderDate.CustomButton.Name = "";
            this.metroTextBoxOrderDate.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxOrderDate.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxOrderDate.CustomButton.TabIndex = 1;
            this.metroTextBoxOrderDate.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxOrderDate.CustomButton.UseSelectable = true;
            this.metroTextBoxOrderDate.CustomButton.Visible = false;
            this.metroTextBoxOrderDate.Lines = new string[0];
            this.metroTextBoxOrderDate.Location = new System.Drawing.Point(504, 160);
            this.metroTextBoxOrderDate.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOrderDate.MaxLength = 32767;
            this.metroTextBoxOrderDate.Name = "metroTextBoxOrderDate";
            this.metroTextBoxOrderDate.PasswordChar = '\0';
            this.metroTextBoxOrderDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxOrderDate.SelectedText = "";
            this.metroTextBoxOrderDate.SelectionLength = 0;
            this.metroTextBoxOrderDate.SelectionStart = 0;
            this.metroTextBoxOrderDate.ShortcutsEnabled = true;
            this.metroTextBoxOrderDate.Size = new System.Drawing.Size(382, 24);
            this.metroTextBoxOrderDate.TabIndex = 97;
            this.metroTextBoxOrderDate.UseSelectable = true;
            this.metroTextBoxOrderDate.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxOrderDate.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxOrderId
            // 
            // 
            // 
            // 
            this.metroTextBoxOrderId.CustomButton.Image = null;
            this.metroTextBoxOrderId.CustomButton.Location = new System.Drawing.Point(354, 2);
            this.metroTextBoxOrderId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOrderId.CustomButton.Name = "";
            this.metroTextBoxOrderId.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxOrderId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxOrderId.CustomButton.TabIndex = 1;
            this.metroTextBoxOrderId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxOrderId.CustomButton.UseSelectable = true;
            this.metroTextBoxOrderId.CustomButton.Visible = false;
            this.metroTextBoxOrderId.Lines = new string[0];
            this.metroTextBoxOrderId.Location = new System.Drawing.Point(36, 80);
            this.metroTextBoxOrderId.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxOrderId.MaxLength = 32767;
            this.metroTextBoxOrderId.Name = "metroTextBoxOrderId";
            this.metroTextBoxOrderId.PasswordChar = '\0';
            this.metroTextBoxOrderId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxOrderId.SelectedText = "";
            this.metroTextBoxOrderId.SelectionLength = 0;
            this.metroTextBoxOrderId.SelectionStart = 0;
            this.metroTextBoxOrderId.ShortcutsEnabled = true;
            this.metroTextBoxOrderId.Size = new System.Drawing.Size(376, 24);
            this.metroTextBoxOrderId.TabIndex = 96;
            this.metroTextBoxOrderId.Tag = "Search";
            this.metroTextBoxOrderId.UseSelectable = true;
            this.metroTextBoxOrderId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxOrderId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel33
            // 
            this.metroLabel33.AutoSize = true;
            this.metroLabel33.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel33.Location = new System.Drawing.Point(36, 56);
            this.metroLabel33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel33.Name = "metroLabel33";
            this.metroLabel33.Size = new System.Drawing.Size(54, 15);
            this.metroLabel33.TabIndex = 95;
            this.metroLabel33.Text = "Order ID:";
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.White;
            this.groupBox11.Controls.Add(this.metroButtonOrderList);
            this.groupBox11.Controls.Add(this.metroButtonOrderAdd);
            this.groupBox11.Controls.Add(this.metroButtonOderUpdate);
            this.groupBox11.Controls.Add(this.metroButtonOrderDelete);
            this.groupBox11.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(984, 36);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox11.Size = new System.Drawing.Size(356, 376);
            this.groupBox11.TabIndex = 83;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Buttom Bar";
            // 
            // metroButtonOrderList
            // 
            this.metroButtonOrderList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonOrderList.BackgroundImage")));
            this.metroButtonOrderList.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonOrderList.Location = new System.Drawing.Point(56, 306);
            this.metroButtonOrderList.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonOrderList.Name = "metroButtonOrderList";
            this.metroButtonOrderList.Size = new System.Drawing.Size(252, 50);
            this.metroButtonOrderList.TabIndex = 79;
            this.metroButtonOrderList.Text = "&List";
            this.metroButtonOrderList.UseSelectable = true;
            this.metroButtonOrderList.Click += new System.EventHandler(this.metroButtonOrderList_Click);
            // 
            // metroButtonOrderAdd
            // 
            this.metroButtonOrderAdd.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.metroButtonOrderAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonOrderAdd.BackgroundImage")));
            this.metroButtonOrderAdd.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonOrderAdd.Location = new System.Drawing.Point(56, 64);
            this.metroButtonOrderAdd.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonOrderAdd.Name = "metroButtonOrderAdd";
            this.metroButtonOrderAdd.Size = new System.Drawing.Size(252, 48);
            this.metroButtonOrderAdd.TabIndex = 75;
            this.metroButtonOrderAdd.Text = "&Add";
            this.metroButtonOrderAdd.UseSelectable = true;
            this.metroButtonOrderAdd.Click += new System.EventHandler(this.metroButtonOrderAdd_Click);
            // 
            // metroButtonOderUpdate
            // 
            this.metroButtonOderUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonOderUpdate.BackgroundImage")));
            this.metroButtonOderUpdate.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonOderUpdate.Location = new System.Drawing.Point(56, 148);
            this.metroButtonOderUpdate.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonOderUpdate.Name = "metroButtonOderUpdate";
            this.metroButtonOderUpdate.Size = new System.Drawing.Size(252, 48);
            this.metroButtonOderUpdate.TabIndex = 77;
            this.metroButtonOderUpdate.Text = "&Update";
            this.metroButtonOderUpdate.UseSelectable = true;
            this.metroButtonOderUpdate.Click += new System.EventHandler(this.metroButtonOderUpdate_Click);
            // 
            // metroButtonOrderDelete
            // 
            this.metroButtonOrderDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonOrderDelete.BackgroundImage")));
            this.metroButtonOrderDelete.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonOrderDelete.Location = new System.Drawing.Point(56, 232);
            this.metroButtonOrderDelete.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonOrderDelete.Name = "metroButtonOrderDelete";
            this.metroButtonOrderDelete.Size = new System.Drawing.Size(252, 52);
            this.metroButtonOrderDelete.TabIndex = 76;
            this.metroButtonOrderDelete.Text = "&Delete";
            this.metroButtonOrderDelete.UseSelectable = true;
            this.metroButtonOrderDelete.Click += new System.EventHandler(this.metroButtonOrderDelete_Click);
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.Location = new System.Drawing.Point(504, 208);
            this.metroLabel20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(94, 19);
            this.metroLabel20.TabIndex = 92;
            this.metroLabel20.Text = "Shipping Date:";
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.Location = new System.Drawing.Point(36, 368);
            this.metroLabel21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(61, 19);
            this.metroLabel21.TabIndex = 91;
            this.metroLabel21.Text = "Quantity:";
            // 
            // metroLabel22
            // 
            this.metroLabel22.AutoSize = true;
            this.metroLabel22.Location = new System.Drawing.Point(504, 296);
            this.metroLabel22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel22.Name = "metroLabel22";
            this.metroLabel22.Size = new System.Drawing.Size(112, 19);
            this.metroLabel22.TabIndex = 90;
            this.metroLabel22.Text = "Total Order Price:";
            // 
            // metroLabel
            // 
            this.metroLabel.AutoSize = true;
            this.metroLabel.Location = new System.Drawing.Point(504, 120);
            this.metroLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel.Name = "metroLabel";
            this.metroLabel.Size = new System.Drawing.Size(79, 19);
            this.metroLabel.TabIndex = 89;
            this.metroLabel.Text = "Order Date:";
            // 
            // metroTextBoxOderQuantity
            // 
            // 
            // 
            // 
            this.metroTextBoxOderQuantity.CustomButton.Image = null;
            this.metroTextBoxOderQuantity.CustomButton.Location = new System.Drawing.Point(354, 2);
            this.metroTextBoxOderQuantity.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOderQuantity.CustomButton.Name = "";
            this.metroTextBoxOderQuantity.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxOderQuantity.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxOderQuantity.CustomButton.TabIndex = 1;
            this.metroTextBoxOderQuantity.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxOderQuantity.CustomButton.UseSelectable = true;
            this.metroTextBoxOderQuantity.CustomButton.Visible = false;
            this.metroTextBoxOderQuantity.Lines = new string[0];
            this.metroTextBoxOderQuantity.Location = new System.Drawing.Point(36, 402);
            this.metroTextBoxOderQuantity.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOderQuantity.MaxLength = 32767;
            this.metroTextBoxOderQuantity.Name = "metroTextBoxOderQuantity";
            this.metroTextBoxOderQuantity.PasswordChar = '\0';
            this.metroTextBoxOderQuantity.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxOderQuantity.SelectedText = "";
            this.metroTextBoxOderQuantity.SelectionLength = 0;
            this.metroTextBoxOderQuantity.SelectionStart = 0;
            this.metroTextBoxOderQuantity.ShortcutsEnabled = true;
            this.metroTextBoxOderQuantity.Size = new System.Drawing.Size(376, 24);
            this.metroTextBoxOderQuantity.TabIndex = 88;
            this.metroTextBoxOderQuantity.UseSelectable = true;
            this.metroTextBoxOderQuantity.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxOderQuantity.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxOrderTotal
            // 
            // 
            // 
            // 
            this.metroTextBoxOrderTotal.CustomButton.Image = null;
            this.metroTextBoxOrderTotal.CustomButton.Location = new System.Drawing.Point(360, 2);
            this.metroTextBoxOrderTotal.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOrderTotal.CustomButton.Name = "";
            this.metroTextBoxOrderTotal.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxOrderTotal.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxOrderTotal.CustomButton.TabIndex = 1;
            this.metroTextBoxOrderTotal.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxOrderTotal.CustomButton.UseSelectable = true;
            this.metroTextBoxOrderTotal.CustomButton.Visible = false;
            this.metroTextBoxOrderTotal.Lines = new string[0];
            this.metroTextBoxOrderTotal.Location = new System.Drawing.Point(504, 340);
            this.metroTextBoxOrderTotal.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOrderTotal.MaxLength = 32767;
            this.metroTextBoxOrderTotal.Name = "metroTextBoxOrderTotal";
            this.metroTextBoxOrderTotal.PasswordChar = '\0';
            this.metroTextBoxOrderTotal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxOrderTotal.SelectedText = "";
            this.metroTextBoxOrderTotal.SelectionLength = 0;
            this.metroTextBoxOrderTotal.SelectionStart = 0;
            this.metroTextBoxOrderTotal.ShortcutsEnabled = true;
            this.metroTextBoxOrderTotal.Size = new System.Drawing.Size(382, 24);
            this.metroTextBoxOrderTotal.TabIndex = 87;
            this.metroTextBoxOrderTotal.UseSelectable = true;
            this.metroTextBoxOrderTotal.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxOrderTotal.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel24
            // 
            this.metroLabel24.AutoSize = true;
            this.metroLabel24.Location = new System.Drawing.Point(512, 60);
            this.metroLabel24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel24.Name = "metroLabel24";
            this.metroLabel24.Size = new System.Drawing.Size(62, 19);
            this.metroLabel24.TabIndex = 84;
            this.metroLabel24.Text = "Payment:";
            // 
            // metroTextBoxOdrtPayment
            // 
            // 
            // 
            // 
            this.metroTextBoxOdrtPayment.CustomButton.Image = null;
            this.metroTextBoxOdrtPayment.CustomButton.Location = new System.Drawing.Point(360, 2);
            this.metroTextBoxOdrtPayment.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOdrtPayment.CustomButton.Name = "";
            this.metroTextBoxOdrtPayment.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxOdrtPayment.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxOdrtPayment.CustomButton.TabIndex = 1;
            this.metroTextBoxOdrtPayment.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxOdrtPayment.CustomButton.UseSelectable = true;
            this.metroTextBoxOdrtPayment.CustomButton.Visible = false;
            this.metroTextBoxOdrtPayment.Lines = new string[0];
            this.metroTextBoxOdrtPayment.Location = new System.Drawing.Point(504, 80);
            this.metroTextBoxOdrtPayment.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOdrtPayment.MaxLength = 32767;
            this.metroTextBoxOdrtPayment.Name = "metroTextBoxOdrtPayment";
            this.metroTextBoxOdrtPayment.PasswordChar = '\0';
            this.metroTextBoxOdrtPayment.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxOdrtPayment.SelectedText = "";
            this.metroTextBoxOdrtPayment.SelectionLength = 0;
            this.metroTextBoxOdrtPayment.SelectionStart = 0;
            this.metroTextBoxOdrtPayment.ShortcutsEnabled = true;
            this.metroTextBoxOdrtPayment.Size = new System.Drawing.Size(382, 24);
            this.metroTextBoxOdrtPayment.TabIndex = 82;
            this.metroTextBoxOdrtPayment.UseSelectable = true;
            this.metroTextBoxOdrtPayment.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxOdrtPayment.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxOderClientID
            // 
            // 
            // 
            // 
            this.metroTextBoxOderClientID.CustomButton.Image = null;
            this.metroTextBoxOderClientID.CustomButton.Location = new System.Drawing.Point(354, 2);
            this.metroTextBoxOderClientID.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOderClientID.CustomButton.Name = "";
            this.metroTextBoxOderClientID.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxOderClientID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxOderClientID.CustomButton.TabIndex = 1;
            this.metroTextBoxOderClientID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxOderClientID.CustomButton.UseSelectable = true;
            this.metroTextBoxOderClientID.CustomButton.Visible = false;
            this.metroTextBoxOderClientID.Lines = new string[0];
            this.metroTextBoxOderClientID.Location = new System.Drawing.Point(36, 140);
            this.metroTextBoxOderClientID.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxOderClientID.MaxLength = 32767;
            this.metroTextBoxOderClientID.Name = "metroTextBoxOderClientID";
            this.metroTextBoxOderClientID.PasswordChar = '\0';
            this.metroTextBoxOderClientID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxOderClientID.SelectedText = "";
            this.metroTextBoxOderClientID.SelectionLength = 0;
            this.metroTextBoxOderClientID.SelectionStart = 0;
            this.metroTextBoxOderClientID.ShortcutsEnabled = true;
            this.metroTextBoxOderClientID.Size = new System.Drawing.Size(376, 24);
            this.metroTextBoxOderClientID.TabIndex = 80;
            this.metroTextBoxOderClientID.Tag = "Search";
            this.metroTextBoxOderClientID.UseSelectable = true;
            this.metroTextBoxOderClientID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxOderClientID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxOderItemId
            // 
            // 
            // 
            // 
            this.metroTextBoxOderItemId.CustomButton.Image = null;
            this.metroTextBoxOderItemId.CustomButton.Location = new System.Drawing.Point(354, 2);
            this.metroTextBoxOderItemId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOderItemId.CustomButton.Name = "";
            this.metroTextBoxOderItemId.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxOderItemId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxOderItemId.CustomButton.TabIndex = 1;
            this.metroTextBoxOderItemId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxOderItemId.CustomButton.UseSelectable = true;
            this.metroTextBoxOderItemId.CustomButton.Visible = false;
            this.metroTextBoxOderItemId.Lines = new string[0];
            this.metroTextBoxOderItemId.Location = new System.Drawing.Point(36, 210);
            this.metroTextBoxOderItemId.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxOderItemId.MaxLength = 32767;
            this.metroTextBoxOderItemId.Name = "metroTextBoxOderItemId";
            this.metroTextBoxOderItemId.PasswordChar = '\0';
            this.metroTextBoxOderItemId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxOderItemId.SelectedText = "";
            this.metroTextBoxOderItemId.SelectionLength = 0;
            this.metroTextBoxOderItemId.SelectionStart = 0;
            this.metroTextBoxOderItemId.ShortcutsEnabled = true;
            this.metroTextBoxOderItemId.Size = new System.Drawing.Size(376, 24);
            this.metroTextBoxOderItemId.TabIndex = 79;
            this.metroTextBoxOderItemId.Tag = "Search";
            this.metroTextBoxOderItemId.UseSelectable = true;
            this.metroTextBoxOderItemId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxOderItemId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxOderUnitPrice
            // 
            // 
            // 
            // 
            this.metroTextBoxOderUnitPrice.CustomButton.Image = null;
            this.metroTextBoxOderUnitPrice.CustomButton.Location = new System.Drawing.Point(354, 2);
            this.metroTextBoxOderUnitPrice.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOderUnitPrice.CustomButton.Name = "";
            this.metroTextBoxOderUnitPrice.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxOderUnitPrice.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxOderUnitPrice.CustomButton.TabIndex = 1;
            this.metroTextBoxOderUnitPrice.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxOderUnitPrice.CustomButton.UseSelectable = true;
            this.metroTextBoxOderUnitPrice.CustomButton.Visible = false;
            this.metroTextBoxOderUnitPrice.Lines = new string[0];
            this.metroTextBoxOderUnitPrice.Location = new System.Drawing.Point(36, 338);
            this.metroTextBoxOderUnitPrice.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxOderUnitPrice.MaxLength = 32767;
            this.metroTextBoxOderUnitPrice.Name = "metroTextBoxOderUnitPrice";
            this.metroTextBoxOderUnitPrice.PasswordChar = '\0';
            this.metroTextBoxOderUnitPrice.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxOderUnitPrice.SelectedText = "";
            this.metroTextBoxOderUnitPrice.SelectionLength = 0;
            this.metroTextBoxOderUnitPrice.SelectionStart = 0;
            this.metroTextBoxOderUnitPrice.ShortcutsEnabled = true;
            this.metroTextBoxOderUnitPrice.Size = new System.Drawing.Size(376, 24);
            this.metroTextBoxOderUnitPrice.TabIndex = 78;
            this.metroTextBoxOderUnitPrice.Tag = "Search";
            this.metroTextBoxOderUnitPrice.UseSelectable = true;
            this.metroTextBoxOderUnitPrice.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxOderUnitPrice.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxOderItemName
            // 
            // 
            // 
            // 
            this.metroTextBoxOderItemName.CustomButton.Image = null;
            this.metroTextBoxOderItemName.CustomButton.Location = new System.Drawing.Point(354, 2);
            this.metroTextBoxOderItemName.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxOderItemName.CustomButton.Name = "";
            this.metroTextBoxOderItemName.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxOderItemName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxOderItemName.CustomButton.TabIndex = 1;
            this.metroTextBoxOderItemName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxOderItemName.CustomButton.UseSelectable = true;
            this.metroTextBoxOderItemName.CustomButton.Visible = false;
            this.metroTextBoxOderItemName.Lines = new string[0];
            this.metroTextBoxOderItemName.Location = new System.Drawing.Point(36, 268);
            this.metroTextBoxOderItemName.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxOderItemName.MaxLength = 32767;
            this.metroTextBoxOderItemName.Name = "metroTextBoxOderItemName";
            this.metroTextBoxOderItemName.PasswordChar = '\0';
            this.metroTextBoxOderItemName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxOderItemName.SelectedText = "";
            this.metroTextBoxOderItemName.SelectionLength = 0;
            this.metroTextBoxOderItemName.SelectionStart = 0;
            this.metroTextBoxOderItemName.ShortcutsEnabled = true;
            this.metroTextBoxOderItemName.Size = new System.Drawing.Size(376, 24);
            this.metroTextBoxOderItemName.TabIndex = 77;
            this.metroTextBoxOderItemName.Tag = "Search";
            this.metroTextBoxOderItemName.UseSelectable = true;
            this.metroTextBoxOderItemName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxOderItemName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel25
            // 
            this.metroLabel25.AutoSize = true;
            this.metroLabel25.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel25.Location = new System.Drawing.Point(36, 116);
            this.metroLabel25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(51, 15);
            this.metroLabel25.TabIndex = 76;
            this.metroLabel25.Text = "Client ID:";
            // 
            // metroLabel26
            // 
            this.metroLabel26.AutoSize = true;
            this.metroLabel26.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel26.Location = new System.Drawing.Point(36, 176);
            this.metroLabel26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel26.Name = "metroLabel26";
            this.metroLabel26.Size = new System.Drawing.Size(47, 15);
            this.metroLabel26.TabIndex = 75;
            this.metroLabel26.Text = "Item ID:";
            // 
            // metroLabel27
            // 
            this.metroLabel27.AutoSize = true;
            this.metroLabel27.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel27.Location = new System.Drawing.Point(36, 240);
            this.metroLabel27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel27.Name = "metroLabel27";
            this.metroLabel27.Size = new System.Drawing.Size(70, 15);
            this.metroLabel27.TabIndex = 74;
            this.metroLabel27.Text = "Item Name: ";
            // 
            // metroLabel28
            // 
            this.metroLabel28.AutoSize = true;
            this.metroLabel28.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel28.Location = new System.Drawing.Point(36, 300);
            this.metroLabel28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel28.Name = "metroLabel28";
            this.metroLabel28.Size = new System.Drawing.Size(60, 15);
            this.metroLabel28.TabIndex = 73;
            this.metroLabel28.Text = "Unit Price: ";
            // 
            // metroTabPageAccountant
            // 
            this.metroTabPageAccountant.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.metroTabPageAccountant.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroTabPageAccountant.BackgroundImage")));
            this.metroTabPageAccountant.Controls.Add(this.groupBox13);
            this.metroTabPageAccountant.Controls.Add(this.groupBox14);
            this.metroTabPageAccountant.HorizontalScrollbarBarColor = true;
            this.metroTabPageAccountant.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageAccountant.HorizontalScrollbarSize = 10;
            this.metroTabPageAccountant.Location = new System.Drawing.Point(4, 37);
            this.metroTabPageAccountant.Margin = new System.Windows.Forms.Padding(4);
            this.metroTabPageAccountant.Name = "metroTabPageAccountant";
            this.metroTabPageAccountant.Size = new System.Drawing.Size(1918, 945);
            this.metroTabPageAccountant.TabIndex = 7;
            this.metroTabPageAccountant.Text = "Accountant";
            this.metroTabPageAccountant.VerticalScrollbarBarColor = true;
            this.metroTabPageAccountant.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageAccountant.VerticalScrollbarSize = 10;
            // 
            // groupBox13
            // 
            this.groupBox13.BackColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox13.Controls.Add(this.metroListViewInvoice);
            this.groupBox13.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox13.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.Location = new System.Drawing.Point(28, 419);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox13.Size = new System.Drawing.Size(1848, 422);
            this.groupBox13.TabIndex = 86;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "List Invoice";
            // 
            // metroListViewInvoice
            // 
            this.metroListViewInvoice.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25});
            this.metroListViewInvoice.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewInvoice.FullRowSelect = true;
            this.metroListViewInvoice.GridLines = true;
            this.metroListViewInvoice.Location = new System.Drawing.Point(0, 66);
            this.metroListViewInvoice.Margin = new System.Windows.Forms.Padding(4);
            this.metroListViewInvoice.Name = "metroListViewInvoice";
            this.metroListViewInvoice.OwnerDraw = true;
            this.metroListViewInvoice.Size = new System.Drawing.Size(1836, 316);
            this.metroListViewInvoice.TabIndex = 81;
            this.metroListViewInvoice.UseCompatibleStateImageBehavior = false;
            this.metroListViewInvoice.UseSelectable = true;
            this.metroListViewInvoice.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Invoice Number";
            this.columnHeader21.Width = 312;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Order ID";
            this.columnHeader22.Width = 323;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Item ID";
            this.columnHeader23.Width = 304;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Client Name";
            this.columnHeader24.Width = 426;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Oeder Date";
            this.columnHeader25.Width = 479;
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox14.Controls.Add(this.groupBox16);
            this.groupBox14.Controls.Add(this.groupBox15);
            this.groupBox14.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox14.Location = new System.Drawing.Point(28, 32);
            this.groupBox14.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox14.Size = new System.Drawing.Size(1856, 339);
            this.groupBox14.TabIndex = 85;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Invoice Info";
            // 
            // groupBox16
            // 
            this.groupBox16.BackColor = System.Drawing.Color.White;
            this.groupBox16.Controls.Add(this.metroButtonInvoiceList);
            this.groupBox16.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox16.Location = new System.Drawing.Point(255, 74);
            this.groupBox16.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox16.Size = new System.Drawing.Size(527, 231);
            this.groupBox16.TabIndex = 84;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Buttom Bar";
            // 
            // metroButtonInvoiceList
            // 
            this.metroButtonInvoiceList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonInvoiceList.BackgroundImage")));
            this.metroButtonInvoiceList.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonInvoiceList.Location = new System.Drawing.Point(52, 113);
            this.metroButtonInvoiceList.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonInvoiceList.Name = "metroButtonInvoiceList";
            this.metroButtonInvoiceList.Size = new System.Drawing.Size(392, 50);
            this.metroButtonInvoiceList.TabIndex = 79;
            this.metroButtonInvoiceList.Text = "&List";
            this.metroButtonInvoiceList.UseSelectable = true;
            this.metroButtonInvoiceList.Click += new System.EventHandler(this.metroButtonInvoiceList_Click);
            // 
            // groupBox15
            // 
            this.groupBox15.BackColor = System.Drawing.Color.White;
            this.groupBox15.Controls.Add(this.metroComboBoxInvoiceSearch);
            this.groupBox15.Controls.Add(this.metroLabelInvoiceInput);
            this.groupBox15.Controls.Add(this.metroTextBoxInvoiceSearchId);
            this.groupBox15.Controls.Add(this.metroButtonInvoiceSearch);
            this.groupBox15.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox15.Location = new System.Drawing.Point(1003, 40);
            this.groupBox15.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox15.Size = new System.Drawing.Size(793, 287);
            this.groupBox15.TabIndex = 83;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Search By";
            // 
            // metroComboBoxInvoiceSearch
            // 
            this.metroComboBoxInvoiceSearch.FormattingEnabled = true;
            this.metroComboBoxInvoiceSearch.ItemHeight = 23;
            this.metroComboBoxInvoiceSearch.Items.AddRange(new object[] {
            "Invoice Number",
            "Client Name"});
            this.metroComboBoxInvoiceSearch.Location = new System.Drawing.Point(194, 84);
            this.metroComboBoxInvoiceSearch.Margin = new System.Windows.Forms.Padding(4);
            this.metroComboBoxInvoiceSearch.Name = "metroComboBoxInvoiceSearch";
            this.metroComboBoxInvoiceSearch.Size = new System.Drawing.Size(460, 29);
            this.metroComboBoxInvoiceSearch.TabIndex = 92;
            this.metroComboBoxInvoiceSearch.UseSelectable = true;
            this.metroComboBoxInvoiceSearch.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxInvoiceSearch_SelectedIndexChanged);
            // 
            // metroLabelInvoiceInput
            // 
            this.metroLabelInvoiceInput.AutoSize = true;
            this.metroLabelInvoiceInput.Location = new System.Drawing.Point(194, 43);
            this.metroLabelInvoiceInput.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabelInvoiceInput.Name = "metroLabelInvoiceInput";
            this.metroLabelInvoiceInput.Size = new System.Drawing.Size(103, 19);
            this.metroLabelInvoiceInput.TabIndex = 93;
            this.metroLabelInvoiceInput.Text = "Messages Show:";
            // 
            // metroTextBoxInvoiceSearchId
            // 
            // 
            // 
            // 
            this.metroTextBoxInvoiceSearchId.CustomButton.Image = null;
            this.metroTextBoxInvoiceSearchId.CustomButton.Location = new System.Drawing.Point(420, 2);
            this.metroTextBoxInvoiceSearchId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxInvoiceSearchId.CustomButton.Name = "";
            this.metroTextBoxInvoiceSearchId.CustomButton.Size = new System.Drawing.Size(37, 37);
            this.metroTextBoxInvoiceSearchId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxInvoiceSearchId.CustomButton.TabIndex = 1;
            this.metroTextBoxInvoiceSearchId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxInvoiceSearchId.CustomButton.UseSelectable = true;
            this.metroTextBoxInvoiceSearchId.CustomButton.Visible = false;
            this.metroTextBoxInvoiceSearchId.Lines = new string[0];
            this.metroTextBoxInvoiceSearchId.Location = new System.Drawing.Point(194, 155);
            this.metroTextBoxInvoiceSearchId.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxInvoiceSearchId.MaxLength = 32767;
            this.metroTextBoxInvoiceSearchId.Name = "metroTextBoxInvoiceSearchId";
            this.metroTextBoxInvoiceSearchId.PasswordChar = '\0';
            this.metroTextBoxInvoiceSearchId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxInvoiceSearchId.SelectedText = "";
            this.metroTextBoxInvoiceSearchId.SelectionLength = 0;
            this.metroTextBoxInvoiceSearchId.SelectionStart = 0;
            this.metroTextBoxInvoiceSearchId.ShortcutsEnabled = true;
            this.metroTextBoxInvoiceSearchId.Size = new System.Drawing.Size(460, 42);
            this.metroTextBoxInvoiceSearchId.TabIndex = 83;
            this.metroTextBoxInvoiceSearchId.Tag = "Search";
            this.metroTextBoxInvoiceSearchId.UseSelectable = true;
            this.metroTextBoxInvoiceSearchId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxInvoiceSearchId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonInvoiceSearch
            // 
            this.metroButtonInvoiceSearch.BackColor = System.Drawing.Color.White;
            this.metroButtonInvoiceSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonInvoiceSearch.BackgroundImage")));
            this.metroButtonInvoiceSearch.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonInvoiceSearch.Location = new System.Drawing.Point(190, 225);
            this.metroButtonInvoiceSearch.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonInvoiceSearch.Name = "metroButtonInvoiceSearch";
            this.metroButtonInvoiceSearch.Size = new System.Drawing.Size(464, 50);
            this.metroButtonInvoiceSearch.TabIndex = 11;
            this.metroButtonInvoiceSearch.Text = "&Search";
            this.metroButtonInvoiceSearch.UseSelectable = true;
            this.metroButtonInvoiceSearch.Click += new System.EventHandler(this.metroButtonInvoiceSearch_Click);
            // 
            // metroTabPageAuthor
            // 
            this.metroTabPageAuthor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroTabPageAuthor.BackgroundImage")));
            this.metroTabPageAuthor.Controls.Add(this.groupBox18);
            this.metroTabPageAuthor.Controls.Add(this.groupBox19);
            this.metroTabPageAuthor.Controls.Add(this.groupBox21);
            this.metroTabPageAuthor.HorizontalScrollbarBarColor = true;
            this.metroTabPageAuthor.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageAuthor.HorizontalScrollbarSize = 10;
            this.metroTabPageAuthor.Location = new System.Drawing.Point(4, 37);
            this.metroTabPageAuthor.Margin = new System.Windows.Forms.Padding(4);
            this.metroTabPageAuthor.Name = "metroTabPageAuthor";
            this.metroTabPageAuthor.Size = new System.Drawing.Size(1918, 945);
            this.metroTabPageAuthor.TabIndex = 5;
            this.metroTabPageAuthor.Text = "Inventory For Author Management";
            this.metroTabPageAuthor.VerticalScrollbarBarColor = true;
            this.metroTabPageAuthor.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageAuthor.VerticalScrollbarSize = 10;
            // 
            // groupBox18
            // 
            this.groupBox18.BackColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox18.Controls.Add(this.metroListViewAuthor);
            this.groupBox18.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox18.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox18.Location = new System.Drawing.Point(24, 496);
            this.groupBox18.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox18.Size = new System.Drawing.Size(1848, 396);
            this.groupBox18.TabIndex = 89;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "List Author";
            // 
            // metroListViewAuthor
            // 
            this.metroListViewAuthor.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader27,
            this.columnHeader28,
            this.columnHeader29,
            this.columnHeader30,
            this.columnHeader31,
            this.columnHeader32});
            this.metroListViewAuthor.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewAuthor.FullRowSelect = true;
            this.metroListViewAuthor.GridLines = true;
            this.metroListViewAuthor.Location = new System.Drawing.Point(4, 44);
            this.metroListViewAuthor.Margin = new System.Windows.Forms.Padding(4);
            this.metroListViewAuthor.Name = "metroListViewAuthor";
            this.metroListViewAuthor.OwnerDraw = true;
            this.metroListViewAuthor.Size = new System.Drawing.Size(1836, 316);
            this.metroListViewAuthor.TabIndex = 81;
            this.metroListViewAuthor.UseCompatibleStateImageBehavior = false;
            this.metroListViewAuthor.UseSelectable = true;
            this.metroListViewAuthor.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Author ID";
            this.columnHeader27.Width = 312;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Publisher ID";
            this.columnHeader28.Width = 323;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "Book ISBN";
            this.columnHeader29.Width = 304;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "First Name";
            this.columnHeader30.Width = 426;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "Last Name";
            this.columnHeader31.Width = 479;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "Email";
            // 
            // groupBox19
            // 
            this.groupBox19.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox19.Controls.Add(this.metroTextBoxAuthorEmail);
            this.groupBox19.Controls.Add(this.metroLabel39);
            this.groupBox19.Controls.Add(this.metroTextBoxAuthorLN);
            this.groupBox19.Controls.Add(this.metroLabel34);
            this.groupBox19.Controls.Add(this.metroTextBoxAuthorPublisherId);
            this.groupBox19.Controls.Add(this.metroTextBoxAuthorId);
            this.groupBox19.Controls.Add(this.groupBox20);
            this.groupBox19.Controls.Add(this.metroTextBoxAuthorBookISBN);
            this.groupBox19.Controls.Add(this.metroTextBoxAuthorFN);
            this.groupBox19.Controls.Add(this.metroLabel35);
            this.groupBox19.Controls.Add(this.metroLabel36);
            this.groupBox19.Controls.Add(this.metroLabel37);
            this.groupBox19.Controls.Add(this.metroLabel38);
            this.groupBox19.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox19.Location = new System.Drawing.Point(32, 25);
            this.groupBox19.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox19.Size = new System.Drawing.Size(1238, 442);
            this.groupBox19.TabIndex = 88;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Author Info";
            // 
            // metroTextBoxAuthorEmail
            // 
            // 
            // 
            // 
            this.metroTextBoxAuthorEmail.CustomButton.Image = null;
            this.metroTextBoxAuthorEmail.CustomButton.Location = new System.Drawing.Point(464, 2);
            this.metroTextBoxAuthorEmail.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxAuthorEmail.CustomButton.Name = "";
            this.metroTextBoxAuthorEmail.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxAuthorEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxAuthorEmail.CustomButton.TabIndex = 1;
            this.metroTextBoxAuthorEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxAuthorEmail.CustomButton.UseSelectable = true;
            this.metroTextBoxAuthorEmail.CustomButton.Visible = false;
            this.metroTextBoxAuthorEmail.Lines = new string[] {
        "      "};
            this.metroTextBoxAuthorEmail.Location = new System.Drawing.Point(92, 408);
            this.metroTextBoxAuthorEmail.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxAuthorEmail.MaxLength = 32767;
            this.metroTextBoxAuthorEmail.Name = "metroTextBoxAuthorEmail";
            this.metroTextBoxAuthorEmail.PasswordChar = '\0';
            this.metroTextBoxAuthorEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxAuthorEmail.SelectedText = "";
            this.metroTextBoxAuthorEmail.SelectionLength = 0;
            this.metroTextBoxAuthorEmail.SelectionStart = 0;
            this.metroTextBoxAuthorEmail.ShortcutsEnabled = true;
            this.metroTextBoxAuthorEmail.Size = new System.Drawing.Size(486, 24);
            this.metroTextBoxAuthorEmail.TabIndex = 87;
            this.metroTextBoxAuthorEmail.Tag = "Search";
            this.metroTextBoxAuthorEmail.Text = "      ";
            this.metroTextBoxAuthorEmail.UseSelectable = true;
            this.metroTextBoxAuthorEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxAuthorEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel39
            // 
            this.metroLabel39.AutoSize = true;
            this.metroLabel39.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel39.Location = new System.Drawing.Point(92, 376);
            this.metroLabel39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel39.Name = "metroLabel39";
            this.metroLabel39.Size = new System.Drawing.Size(39, 15);
            this.metroLabel39.TabIndex = 86;
            this.metroLabel39.Text = "Email: ";
            // 
            // metroTextBoxAuthorLN
            // 
            // 
            // 
            // 
            this.metroTextBoxAuthorLN.CustomButton.Image = null;
            this.metroTextBoxAuthorLN.CustomButton.Location = new System.Drawing.Point(464, 2);
            this.metroTextBoxAuthorLN.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxAuthorLN.CustomButton.Name = "";
            this.metroTextBoxAuthorLN.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxAuthorLN.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxAuthorLN.CustomButton.TabIndex = 1;
            this.metroTextBoxAuthorLN.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxAuthorLN.CustomButton.UseSelectable = true;
            this.metroTextBoxAuthorLN.CustomButton.Visible = false;
            this.metroTextBoxAuthorLN.Lines = new string[] {
        "      "};
            this.metroTextBoxAuthorLN.Location = new System.Drawing.Point(92, 346);
            this.metroTextBoxAuthorLN.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxAuthorLN.MaxLength = 32767;
            this.metroTextBoxAuthorLN.Name = "metroTextBoxAuthorLN";
            this.metroTextBoxAuthorLN.PasswordChar = '\0';
            this.metroTextBoxAuthorLN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxAuthorLN.SelectedText = "";
            this.metroTextBoxAuthorLN.SelectionLength = 0;
            this.metroTextBoxAuthorLN.SelectionStart = 0;
            this.metroTextBoxAuthorLN.ShortcutsEnabled = true;
            this.metroTextBoxAuthorLN.Size = new System.Drawing.Size(486, 24);
            this.metroTextBoxAuthorLN.TabIndex = 85;
            this.metroTextBoxAuthorLN.Tag = "Search";
            this.metroTextBoxAuthorLN.Text = "      ";
            this.metroTextBoxAuthorLN.UseSelectable = true;
            this.metroTextBoxAuthorLN.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxAuthorLN.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel34
            // 
            this.metroLabel34.AutoSize = true;
            this.metroLabel34.Location = new System.Drawing.Point(92, 108);
            this.metroLabel34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel34.Name = "metroLabel34";
            this.metroLabel34.Size = new System.Drawing.Size(81, 19);
            this.metroLabel34.TabIndex = 82;
            this.metroLabel34.Text = "Publisher ID:";
            // 
            // metroTextBoxAuthorPublisherId
            // 
            // 
            // 
            // 
            this.metroTextBoxAuthorPublisherId.CustomButton.Image = null;
            this.metroTextBoxAuthorPublisherId.CustomButton.Location = new System.Drawing.Point(464, 2);
            this.metroTextBoxAuthorPublisherId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxAuthorPublisherId.CustomButton.Name = "";
            this.metroTextBoxAuthorPublisherId.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxAuthorPublisherId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxAuthorPublisherId.CustomButton.TabIndex = 1;
            this.metroTextBoxAuthorPublisherId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxAuthorPublisherId.CustomButton.UseSelectable = true;
            this.metroTextBoxAuthorPublisherId.CustomButton.Visible = false;
            this.metroTextBoxAuthorPublisherId.Lines = new string[0];
            this.metroTextBoxAuthorPublisherId.Location = new System.Drawing.Point(92, 146);
            this.metroTextBoxAuthorPublisherId.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxAuthorPublisherId.MaxLength = 32767;
            this.metroTextBoxAuthorPublisherId.Name = "metroTextBoxAuthorPublisherId";
            this.metroTextBoxAuthorPublisherId.PasswordChar = '\0';
            this.metroTextBoxAuthorPublisherId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxAuthorPublisherId.SelectedText = "";
            this.metroTextBoxAuthorPublisherId.SelectionLength = 0;
            this.metroTextBoxAuthorPublisherId.SelectionStart = 0;
            this.metroTextBoxAuthorPublisherId.ShortcutsEnabled = true;
            this.metroTextBoxAuthorPublisherId.Size = new System.Drawing.Size(486, 24);
            this.metroTextBoxAuthorPublisherId.TabIndex = 81;
            this.metroTextBoxAuthorPublisherId.UseSelectable = true;
            this.metroTextBoxAuthorPublisherId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxAuthorPublisherId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxAuthorId
            // 
            // 
            // 
            // 
            this.metroTextBoxAuthorId.CustomButton.Image = null;
            this.metroTextBoxAuthorId.CustomButton.Location = new System.Drawing.Point(464, 2);
            this.metroTextBoxAuthorId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxAuthorId.CustomButton.Name = "";
            this.metroTextBoxAuthorId.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxAuthorId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxAuthorId.CustomButton.TabIndex = 1;
            this.metroTextBoxAuthorId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxAuthorId.CustomButton.UseSelectable = true;
            this.metroTextBoxAuthorId.CustomButton.Visible = false;
            this.metroTextBoxAuthorId.Lines = new string[0];
            this.metroTextBoxAuthorId.Location = new System.Drawing.Point(92, 80);
            this.metroTextBoxAuthorId.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxAuthorId.MaxLength = 32767;
            this.metroTextBoxAuthorId.Name = "metroTextBoxAuthorId";
            this.metroTextBoxAuthorId.PasswordChar = '\0';
            this.metroTextBoxAuthorId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxAuthorId.SelectedText = "";
            this.metroTextBoxAuthorId.SelectionLength = 0;
            this.metroTextBoxAuthorId.SelectionStart = 0;
            this.metroTextBoxAuthorId.ShortcutsEnabled = true;
            this.metroTextBoxAuthorId.Size = new System.Drawing.Size(486, 24);
            this.metroTextBoxAuthorId.TabIndex = 80;
            this.metroTextBoxAuthorId.Tag = "Search";
            this.metroTextBoxAuthorId.UseSelectable = true;
            this.metroTextBoxAuthorId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxAuthorId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // groupBox20
            // 
            this.groupBox20.BackColor = System.Drawing.Color.White;
            this.groupBox20.Controls.Add(this.metroButtonAuthorUpdate);
            this.groupBox20.Controls.Add(this.metroButtonAuthorList);
            this.groupBox20.Controls.Add(this.metroButtonAuthorAdd);
            this.groupBox20.Controls.Add(this.metroButtonAuthorDelete);
            this.groupBox20.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox20.Location = new System.Drawing.Point(676, 24);
            this.groupBox20.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox20.Size = new System.Drawing.Size(520, 404);
            this.groupBox20.TabIndex = 84;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Buttom Bar";
            // 
            // metroButtonAuthorUpdate
            // 
            this.metroButtonAuthorUpdate.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.metroButtonAuthorUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonAuthorUpdate.BackgroundImage")));
            this.metroButtonAuthorUpdate.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonAuthorUpdate.Location = new System.Drawing.Point(64, 156);
            this.metroButtonAuthorUpdate.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonAuthorUpdate.Name = "metroButtonAuthorUpdate";
            this.metroButtonAuthorUpdate.Size = new System.Drawing.Size(392, 48);
            this.metroButtonAuthorUpdate.TabIndex = 80;
            this.metroButtonAuthorUpdate.Text = "&Update";
            this.metroButtonAuthorUpdate.UseSelectable = true;
            this.metroButtonAuthorUpdate.Click += new System.EventHandler(this.metroButtonAuthorUpdate_Click);
            // 
            // metroButtonAuthorList
            // 
            this.metroButtonAuthorList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonAuthorList.BackgroundImage")));
            this.metroButtonAuthorList.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonAuthorList.Location = new System.Drawing.Point(64, 304);
            this.metroButtonAuthorList.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonAuthorList.Name = "metroButtonAuthorList";
            this.metroButtonAuthorList.Size = new System.Drawing.Size(392, 50);
            this.metroButtonAuthorList.TabIndex = 79;
            this.metroButtonAuthorList.Text = "&List";
            this.metroButtonAuthorList.UseSelectable = true;
            this.metroButtonAuthorList.Click += new System.EventHandler(this.metroButtonAuthorList_Click);
            // 
            // metroButtonAuthorAdd
            // 
            this.metroButtonAuthorAdd.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.metroButtonAuthorAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonAuthorAdd.BackgroundImage")));
            this.metroButtonAuthorAdd.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonAuthorAdd.Location = new System.Drawing.Point(64, 84);
            this.metroButtonAuthorAdd.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonAuthorAdd.Name = "metroButtonAuthorAdd";
            this.metroButtonAuthorAdd.Size = new System.Drawing.Size(392, 48);
            this.metroButtonAuthorAdd.TabIndex = 75;
            this.metroButtonAuthorAdd.Text = "&Add";
            this.metroButtonAuthorAdd.UseSelectable = true;
            this.metroButtonAuthorAdd.Click += new System.EventHandler(this.metroButtonAuthorAdd_Click);
            // 
            // metroButtonAuthorDelete
            // 
            this.metroButtonAuthorDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonAuthorDelete.BackgroundImage")));
            this.metroButtonAuthorDelete.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonAuthorDelete.Location = new System.Drawing.Point(64, 224);
            this.metroButtonAuthorDelete.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonAuthorDelete.Name = "metroButtonAuthorDelete";
            this.metroButtonAuthorDelete.Size = new System.Drawing.Size(392, 52);
            this.metroButtonAuthorDelete.TabIndex = 76;
            this.metroButtonAuthorDelete.Text = "&Delete";
            this.metroButtonAuthorDelete.UseSelectable = true;
            this.metroButtonAuthorDelete.Click += new System.EventHandler(this.metroButtonAuthorDelete_Click);
            // 
            // metroTextBoxAuthorBookISBN
            // 
            // 
            // 
            // 
            this.metroTextBoxAuthorBookISBN.CustomButton.Image = null;
            this.metroTextBoxAuthorBookISBN.CustomButton.Location = new System.Drawing.Point(464, 2);
            this.metroTextBoxAuthorBookISBN.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxAuthorBookISBN.CustomButton.Name = "";
            this.metroTextBoxAuthorBookISBN.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxAuthorBookISBN.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxAuthorBookISBN.CustomButton.TabIndex = 1;
            this.metroTextBoxAuthorBookISBN.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxAuthorBookISBN.CustomButton.UseSelectable = true;
            this.metroTextBoxAuthorBookISBN.CustomButton.Visible = false;
            this.metroTextBoxAuthorBookISBN.Lines = new string[0];
            this.metroTextBoxAuthorBookISBN.Location = new System.Drawing.Point(92, 212);
            this.metroTextBoxAuthorBookISBN.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxAuthorBookISBN.MaxLength = 32767;
            this.metroTextBoxAuthorBookISBN.Name = "metroTextBoxAuthorBookISBN";
            this.metroTextBoxAuthorBookISBN.PasswordChar = '\0';
            this.metroTextBoxAuthorBookISBN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxAuthorBookISBN.SelectedText = "";
            this.metroTextBoxAuthorBookISBN.SelectionLength = 0;
            this.metroTextBoxAuthorBookISBN.SelectionStart = 0;
            this.metroTextBoxAuthorBookISBN.ShortcutsEnabled = true;
            this.metroTextBoxAuthorBookISBN.Size = new System.Drawing.Size(486, 24);
            this.metroTextBoxAuthorBookISBN.TabIndex = 79;
            this.metroTextBoxAuthorBookISBN.Tag = "Search";
            this.metroTextBoxAuthorBookISBN.UseSelectable = true;
            this.metroTextBoxAuthorBookISBN.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxAuthorBookISBN.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxAuthorFN
            // 
            // 
            // 
            // 
            this.metroTextBoxAuthorFN.CustomButton.Image = null;
            this.metroTextBoxAuthorFN.CustomButton.Location = new System.Drawing.Point(464, 2);
            this.metroTextBoxAuthorFN.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxAuthorFN.CustomButton.Name = "";
            this.metroTextBoxAuthorFN.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxAuthorFN.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxAuthorFN.CustomButton.TabIndex = 1;
            this.metroTextBoxAuthorFN.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxAuthorFN.CustomButton.UseSelectable = true;
            this.metroTextBoxAuthorFN.CustomButton.Visible = false;
            this.metroTextBoxAuthorFN.Lines = new string[0];
            this.metroTextBoxAuthorFN.Location = new System.Drawing.Point(92, 280);
            this.metroTextBoxAuthorFN.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxAuthorFN.MaxLength = 32767;
            this.metroTextBoxAuthorFN.Name = "metroTextBoxAuthorFN";
            this.metroTextBoxAuthorFN.PasswordChar = '\0';
            this.metroTextBoxAuthorFN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxAuthorFN.SelectedText = "";
            this.metroTextBoxAuthorFN.SelectionLength = 0;
            this.metroTextBoxAuthorFN.SelectionStart = 0;
            this.metroTextBoxAuthorFN.ShortcutsEnabled = true;
            this.metroTextBoxAuthorFN.Size = new System.Drawing.Size(486, 24);
            this.metroTextBoxAuthorFN.TabIndex = 77;
            this.metroTextBoxAuthorFN.Tag = "Search";
            this.metroTextBoxAuthorFN.UseSelectable = true;
            this.metroTextBoxAuthorFN.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxAuthorFN.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel35
            // 
            this.metroLabel35.AutoSize = true;
            this.metroLabel35.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel35.Location = new System.Drawing.Point(92, 44);
            this.metroLabel35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel35.Name = "metroLabel35";
            this.metroLabel35.Size = new System.Drawing.Size(59, 15);
            this.metroLabel35.TabIndex = 76;
            this.metroLabel35.Text = "Author ID:";
            // 
            // metroLabel36
            // 
            this.metroLabel36.AutoSize = true;
            this.metroLabel36.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel36.Location = new System.Drawing.Point(92, 182);
            this.metroLabel36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel36.Name = "metroLabel36";
            this.metroLabel36.Size = new System.Drawing.Size(64, 15);
            this.metroLabel36.TabIndex = 75;
            this.metroLabel36.Text = "Book ISBN:";
            // 
            // metroLabel37
            // 
            this.metroLabel37.AutoSize = true;
            this.metroLabel37.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel37.Location = new System.Drawing.Point(92, 240);
            this.metroLabel37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel37.Name = "metroLabel37";
            this.metroLabel37.Size = new System.Drawing.Size(68, 15);
            this.metroLabel37.TabIndex = 74;
            this.metroLabel37.Text = "First Name: ";
            // 
            // metroLabel38
            // 
            this.metroLabel38.AutoSize = true;
            this.metroLabel38.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel38.Location = new System.Drawing.Point(92, 308);
            this.metroLabel38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel38.Name = "metroLabel38";
            this.metroLabel38.Size = new System.Drawing.Size(67, 15);
            this.metroLabel38.TabIndex = 73;
            this.metroLabel38.Text = "Last Name: ";
            // 
            // groupBox21
            // 
            this.groupBox21.BackColor = System.Drawing.Color.White;
            this.groupBox21.Controls.Add(this.metroComboBoxAuthorSearch);
            this.groupBox21.Controls.Add(this.metroLabelAuthorInput);
            this.groupBox21.Controls.Add(this.metroTextBoxAuthorSearchId);
            this.groupBox21.Controls.Add(this.metroButtonAuthorSearch);
            this.groupBox21.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox21.Location = new System.Drawing.Point(1320, 27);
            this.groupBox21.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox21.Size = new System.Drawing.Size(552, 430);
            this.groupBox21.TabIndex = 87;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Search By";
            // 
            // metroComboBoxAuthorSearch
            // 
            this.metroComboBoxAuthorSearch.FormattingEnabled = true;
            this.metroComboBoxAuthorSearch.ItemHeight = 23;
            this.metroComboBoxAuthorSearch.Items.AddRange(new object[] {
            "Author ID",
            "First Name"});
            this.metroComboBoxAuthorSearch.Location = new System.Drawing.Point(28, 140);
            this.metroComboBoxAuthorSearch.Margin = new System.Windows.Forms.Padding(4);
            this.metroComboBoxAuthorSearch.Name = "metroComboBoxAuthorSearch";
            this.metroComboBoxAuthorSearch.Size = new System.Drawing.Size(460, 29);
            this.metroComboBoxAuthorSearch.TabIndex = 92;
            this.metroComboBoxAuthorSearch.UseSelectable = true;
            this.metroComboBoxAuthorSearch.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxAuthorSearch_SelectedIndexChanged);
            // 
            // metroLabelAuthorInput
            // 
            this.metroLabelAuthorInput.AutoSize = true;
            this.metroLabelAuthorInput.Location = new System.Drawing.Point(28, 98);
            this.metroLabelAuthorInput.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabelAuthorInput.Name = "metroLabelAuthorInput";
            this.metroLabelAuthorInput.Size = new System.Drawing.Size(103, 19);
            this.metroLabelAuthorInput.TabIndex = 93;
            this.metroLabelAuthorInput.Text = "Messages Show:";
            // 
            // metroTextBoxAuthorSearchId
            // 
            // 
            // 
            // 
            this.metroTextBoxAuthorSearchId.CustomButton.Image = null;
            this.metroTextBoxAuthorSearchId.CustomButton.Location = new System.Drawing.Point(420, 2);
            this.metroTextBoxAuthorSearchId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxAuthorSearchId.CustomButton.Name = "";
            this.metroTextBoxAuthorSearchId.CustomButton.Size = new System.Drawing.Size(37, 37);
            this.metroTextBoxAuthorSearchId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxAuthorSearchId.CustomButton.TabIndex = 1;
            this.metroTextBoxAuthorSearchId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxAuthorSearchId.CustomButton.UseSelectable = true;
            this.metroTextBoxAuthorSearchId.CustomButton.Visible = false;
            this.metroTextBoxAuthorSearchId.Lines = new string[0];
            this.metroTextBoxAuthorSearchId.Location = new System.Drawing.Point(28, 224);
            this.metroTextBoxAuthorSearchId.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxAuthorSearchId.MaxLength = 32767;
            this.metroTextBoxAuthorSearchId.Name = "metroTextBoxAuthorSearchId";
            this.metroTextBoxAuthorSearchId.PasswordChar = '\0';
            this.metroTextBoxAuthorSearchId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxAuthorSearchId.SelectedText = "";
            this.metroTextBoxAuthorSearchId.SelectionLength = 0;
            this.metroTextBoxAuthorSearchId.SelectionStart = 0;
            this.metroTextBoxAuthorSearchId.ShortcutsEnabled = true;
            this.metroTextBoxAuthorSearchId.Size = new System.Drawing.Size(460, 42);
            this.metroTextBoxAuthorSearchId.TabIndex = 83;
            this.metroTextBoxAuthorSearchId.Tag = "Search";
            this.metroTextBoxAuthorSearchId.UseSelectable = true;
            this.metroTextBoxAuthorSearchId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxAuthorSearchId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonAuthorSearch
            // 
            this.metroButtonAuthorSearch.BackColor = System.Drawing.Color.White;
            this.metroButtonAuthorSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonAuthorSearch.BackgroundImage")));
            this.metroButtonAuthorSearch.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonAuthorSearch.Location = new System.Drawing.Point(24, 296);
            this.metroButtonAuthorSearch.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonAuthorSearch.Name = "metroButtonAuthorSearch";
            this.metroButtonAuthorSearch.Size = new System.Drawing.Size(464, 50);
            this.metroButtonAuthorSearch.TabIndex = 11;
            this.metroButtonAuthorSearch.Text = "&Search";
            this.metroButtonAuthorSearch.UseSelectable = true;
            this.metroButtonAuthorSearch.Click += new System.EventHandler(this.metroButtonAuthorSearch_Click);
            // 
            // metroTabPageItem
            // 
            this.metroTabPageItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroTabPageItem.BackgroundImage")));
            this.metroTabPageItem.Controls.Add(this.groupBox25);
            this.metroTabPageItem.Controls.Add(this.groupBox26);
            this.metroTabPageItem.Controls.Add(this.groupBox28);
            this.metroTabPageItem.HorizontalScrollbarBarColor = true;
            this.metroTabPageItem.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageItem.HorizontalScrollbarSize = 10;
            this.metroTabPageItem.Location = new System.Drawing.Point(4, 37);
            this.metroTabPageItem.Margin = new System.Windows.Forms.Padding(4);
            this.metroTabPageItem.Name = "metroTabPageItem";
            this.metroTabPageItem.Size = new System.Drawing.Size(1918, 945);
            this.metroTabPageItem.TabIndex = 4;
            this.metroTabPageItem.Text = "Inventory For Item Management";
            this.metroTabPageItem.VerticalScrollbarBarColor = true;
            this.metroTabPageItem.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageItem.VerticalScrollbarSize = 10;
            // 
            // groupBox25
            // 
            this.groupBox25.BackColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox25.Controls.Add(this.metroListViewBook);
            this.groupBox25.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox25.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox25.Location = new System.Drawing.Point(43, 473);
            this.groupBox25.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox25.Size = new System.Drawing.Size(1848, 396);
            this.groupBox25.TabIndex = 92;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "List Book";
            // 
            // metroListViewBook
            // 
            this.metroListViewBook.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader33,
            this.columnHeader34,
            this.columnHeader35,
            this.columnHeader36,
            this.columnHeader37,
            this.columnHeader38,
            this.columnHeader39,
            this.columnHeader41});
            this.metroListViewBook.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewBook.FullRowSelect = true;
            this.metroListViewBook.GridLines = true;
            this.metroListViewBook.Location = new System.Drawing.Point(8, 63);
            this.metroListViewBook.Margin = new System.Windows.Forms.Padding(4);
            this.metroListViewBook.Name = "metroListViewBook";
            this.metroListViewBook.OwnerDraw = true;
            this.metroListViewBook.Size = new System.Drawing.Size(1836, 316);
            this.metroListViewBook.TabIndex = 81;
            this.metroListViewBook.UseCompatibleStateImageBehavior = false;
            this.metroListViewBook.UseSelectable = true;
            this.metroListViewBook.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "Book ISBN";
            this.columnHeader33.Width = 252;
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "Publisher ID";
            this.columnHeader34.Width = 187;
            // 
            // columnHeader35
            // 
            this.columnHeader35.Text = "Category ID";
            this.columnHeader35.Width = 185;
            // 
            // columnHeader36
            // 
            this.columnHeader36.Text = "Book Title";
            this.columnHeader36.Width = 304;
            // 
            // columnHeader37
            // 
            this.columnHeader37.Text = "Book Price";
            this.columnHeader37.Width = 175;
            // 
            // columnHeader38
            // 
            this.columnHeader38.Text = "Book Quantity On Hand";
            this.columnHeader38.Width = 358;
            // 
            // columnHeader39
            // 
            this.columnHeader39.Text = "Book Order Quantity";
            this.columnHeader39.Width = 325;
            // 
            // columnHeader41
            // 
            this.columnHeader41.Text = "Book Published Date";
            this.columnHeader41.Width = 246;
            // 
            // groupBox26
            // 
            this.groupBox26.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox26.Controls.Add(this.metroTextBoxBookOrderQuantity);
            this.groupBox26.Controls.Add(this.metroLabel40);
            this.groupBox26.Controls.Add(this.metroTextBoxBookQOH);
            this.groupBox26.Controls.Add(this.metroTextBoxBookPublishedDate);
            this.groupBox26.Controls.Add(this.metroLabel47);
            this.groupBox26.Controls.Add(this.metroLabel49);
            this.groupBox26.Controls.Add(this.metroTextBoxBookPrice);
            this.groupBox26.Controls.Add(this.metroLabel41);
            this.groupBox26.Controls.Add(this.metroTextBoxBookPublisherId);
            this.groupBox26.Controls.Add(this.metroTextBoxBookISBN);
            this.groupBox26.Controls.Add(this.groupBox27);
            this.groupBox26.Controls.Add(this.metroTextBoxBookCategoryId);
            this.groupBox26.Controls.Add(this.metroTextBoxBookBookTitle);
            this.groupBox26.Controls.Add(this.metroLabel42);
            this.groupBox26.Controls.Add(this.metroLabel43);
            this.groupBox26.Controls.Add(this.metroLabel44);
            this.groupBox26.Controls.Add(this.metroLabel45);
            this.groupBox26.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox26.Location = new System.Drawing.Point(43, 39);
            this.groupBox26.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox26.Size = new System.Drawing.Size(1426, 387);
            this.groupBox26.TabIndex = 91;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Book Info";
            // 
            // metroTextBoxBookOrderQuantity
            // 
            // 
            // 
            // 
            this.metroTextBoxBookOrderQuantity.CustomButton.Image = null;
            this.metroTextBoxBookOrderQuantity.CustomButton.Location = new System.Drawing.Point(393, 2);
            this.metroTextBoxBookOrderQuantity.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxBookOrderQuantity.CustomButton.Name = "";
            this.metroTextBoxBookOrderQuantity.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxBookOrderQuantity.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxBookOrderQuantity.CustomButton.TabIndex = 1;
            this.metroTextBoxBookOrderQuantity.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxBookOrderQuantity.CustomButton.UseSelectable = true;
            this.metroTextBoxBookOrderQuantity.CustomButton.Visible = false;
            this.metroTextBoxBookOrderQuantity.Lines = new string[] {
        "      "};
            this.metroTextBoxBookOrderQuantity.Location = new System.Drawing.Point(499, 257);
            this.metroTextBoxBookOrderQuantity.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxBookOrderQuantity.MaxLength = 32767;
            this.metroTextBoxBookOrderQuantity.Name = "metroTextBoxBookOrderQuantity";
            this.metroTextBoxBookOrderQuantity.PasswordChar = '\0';
            this.metroTextBoxBookOrderQuantity.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxBookOrderQuantity.SelectedText = "";
            this.metroTextBoxBookOrderQuantity.SelectionLength = 0;
            this.metroTextBoxBookOrderQuantity.SelectionStart = 0;
            this.metroTextBoxBookOrderQuantity.ShortcutsEnabled = true;
            this.metroTextBoxBookOrderQuantity.Size = new System.Drawing.Size(415, 24);
            this.metroTextBoxBookOrderQuantity.TabIndex = 95;
            this.metroTextBoxBookOrderQuantity.Tag = "Search";
            this.metroTextBoxBookOrderQuantity.Text = "      ";
            this.metroTextBoxBookOrderQuantity.UseSelectable = true;
            this.metroTextBoxBookOrderQuantity.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxBookOrderQuantity.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel40
            // 
            this.metroLabel40.AutoSize = true;
            this.metroLabel40.Location = new System.Drawing.Point(499, 225);
            this.metroLabel40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel40.Name = "metroLabel40";
            this.metroLabel40.Size = new System.Drawing.Size(135, 19);
            this.metroLabel40.TabIndex = 94;
            this.metroLabel40.Text = "Book Order Quantity:";
            // 
            // metroTextBoxBookQOH
            // 
            // 
            // 
            // 
            this.metroTextBoxBookQOH.CustomButton.Image = null;
            this.metroTextBoxBookQOH.CustomButton.Location = new System.Drawing.Point(393, 2);
            this.metroTextBoxBookQOH.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxBookQOH.CustomButton.Name = "";
            this.metroTextBoxBookQOH.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxBookQOH.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxBookQOH.CustomButton.TabIndex = 1;
            this.metroTextBoxBookQOH.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxBookQOH.CustomButton.UseSelectable = true;
            this.metroTextBoxBookQOH.CustomButton.Visible = false;
            this.metroTextBoxBookQOH.Lines = new string[0];
            this.metroTextBoxBookQOH.Location = new System.Drawing.Point(499, 180);
            this.metroTextBoxBookQOH.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxBookQOH.MaxLength = 32767;
            this.metroTextBoxBookQOH.Name = "metroTextBoxBookQOH";
            this.metroTextBoxBookQOH.PasswordChar = '\0';
            this.metroTextBoxBookQOH.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxBookQOH.SelectedText = "";
            this.metroTextBoxBookQOH.SelectionLength = 0;
            this.metroTextBoxBookQOH.SelectionStart = 0;
            this.metroTextBoxBookQOH.ShortcutsEnabled = true;
            this.metroTextBoxBookQOH.Size = new System.Drawing.Size(415, 24);
            this.metroTextBoxBookQOH.TabIndex = 92;
            this.metroTextBoxBookQOH.Tag = "Search";
            this.metroTextBoxBookQOH.UseSelectable = true;
            this.metroTextBoxBookQOH.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxBookQOH.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxBookPublishedDate
            // 
            // 
            // 
            // 
            this.metroTextBoxBookPublishedDate.CustomButton.Image = null;
            this.metroTextBoxBookPublishedDate.CustomButton.Location = new System.Drawing.Point(393, 2);
            this.metroTextBoxBookPublishedDate.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxBookPublishedDate.CustomButton.Name = "";
            this.metroTextBoxBookPublishedDate.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxBookPublishedDate.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxBookPublishedDate.CustomButton.TabIndex = 1;
            this.metroTextBoxBookPublishedDate.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxBookPublishedDate.CustomButton.UseSelectable = true;
            this.metroTextBoxBookPublishedDate.CustomButton.Visible = false;
            this.metroTextBoxBookPublishedDate.Lines = new string[0];
            this.metroTextBoxBookPublishedDate.Location = new System.Drawing.Point(499, 334);
            this.metroTextBoxBookPublishedDate.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxBookPublishedDate.MaxLength = 32767;
            this.metroTextBoxBookPublishedDate.Name = "metroTextBoxBookPublishedDate";
            this.metroTextBoxBookPublishedDate.PasswordChar = '\0';
            this.metroTextBoxBookPublishedDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxBookPublishedDate.SelectedText = "";
            this.metroTextBoxBookPublishedDate.SelectionLength = 0;
            this.metroTextBoxBookPublishedDate.SelectionStart = 0;
            this.metroTextBoxBookPublishedDate.ShortcutsEnabled = true;
            this.metroTextBoxBookPublishedDate.Size = new System.Drawing.Size(415, 24);
            this.metroTextBoxBookPublishedDate.TabIndex = 90;
            this.metroTextBoxBookPublishedDate.Tag = "Search";
            this.metroTextBoxBookPublishedDate.UseSelectable = true;
            this.metroTextBoxBookPublishedDate.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxBookPublishedDate.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel47
            // 
            this.metroLabel47.AutoSize = true;
            this.metroLabel47.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel47.Location = new System.Drawing.Point(499, 144);
            this.metroLabel47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel47.Name = "metroLabel47";
            this.metroLabel47.Size = new System.Drawing.Size(123, 15);
            this.metroLabel47.TabIndex = 89;
            this.metroLabel47.Text = "Book Quanty On Hand:";
            // 
            // metroLabel49
            // 
            this.metroLabel49.AutoSize = true;
            this.metroLabel49.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel49.Location = new System.Drawing.Point(499, 302);
            this.metroLabel49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel49.Name = "metroLabel49";
            this.metroLabel49.Size = new System.Drawing.Size(117, 15);
            this.metroLabel49.TabIndex = 87;
            this.metroLabel49.Text = "Book Published Date: ";
            // 
            // metroTextBoxBookPrice
            // 
            // 
            // 
            // 
            this.metroTextBoxBookPrice.CustomButton.Image = null;
            this.metroTextBoxBookPrice.CustomButton.Location = new System.Drawing.Point(393, 2);
            this.metroTextBoxBookPrice.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxBookPrice.CustomButton.Name = "";
            this.metroTextBoxBookPrice.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxBookPrice.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxBookPrice.CustomButton.TabIndex = 1;
            this.metroTextBoxBookPrice.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxBookPrice.CustomButton.UseSelectable = true;
            this.metroTextBoxBookPrice.CustomButton.Visible = false;
            this.metroTextBoxBookPrice.Lines = new string[] {
        "      "};
            this.metroTextBoxBookPrice.Location = new System.Drawing.Point(499, 94);
            this.metroTextBoxBookPrice.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxBookPrice.MaxLength = 32767;
            this.metroTextBoxBookPrice.Name = "metroTextBoxBookPrice";
            this.metroTextBoxBookPrice.PasswordChar = '\0';
            this.metroTextBoxBookPrice.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxBookPrice.SelectedText = "";
            this.metroTextBoxBookPrice.SelectionLength = 0;
            this.metroTextBoxBookPrice.SelectionStart = 0;
            this.metroTextBoxBookPrice.ShortcutsEnabled = true;
            this.metroTextBoxBookPrice.Size = new System.Drawing.Size(415, 24);
            this.metroTextBoxBookPrice.TabIndex = 85;
            this.metroTextBoxBookPrice.Tag = "Search";
            this.metroTextBoxBookPrice.Text = "      ";
            this.metroTextBoxBookPrice.UseSelectable = true;
            this.metroTextBoxBookPrice.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxBookPrice.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel41
            // 
            this.metroLabel41.AutoSize = true;
            this.metroLabel41.Location = new System.Drawing.Point(37, 142);
            this.metroLabel41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel41.Name = "metroLabel41";
            this.metroLabel41.Size = new System.Drawing.Size(81, 19);
            this.metroLabel41.TabIndex = 82;
            this.metroLabel41.Text = "Publisher ID:";
            // 
            // metroTextBoxBookPublisherId
            // 
            // 
            // 
            // 
            this.metroTextBoxBookPublisherId.CustomButton.Image = null;
            this.metroTextBoxBookPublisherId.CustomButton.Location = new System.Drawing.Point(393, 2);
            this.metroTextBoxBookPublisherId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxBookPublisherId.CustomButton.Name = "";
            this.metroTextBoxBookPublisherId.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxBookPublisherId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxBookPublisherId.CustomButton.TabIndex = 1;
            this.metroTextBoxBookPublisherId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxBookPublisherId.CustomButton.UseSelectable = true;
            this.metroTextBoxBookPublisherId.CustomButton.Visible = false;
            this.metroTextBoxBookPublisherId.Lines = new string[0];
            this.metroTextBoxBookPublisherId.Location = new System.Drawing.Point(37, 180);
            this.metroTextBoxBookPublisherId.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxBookPublisherId.MaxLength = 32767;
            this.metroTextBoxBookPublisherId.Name = "metroTextBoxBookPublisherId";
            this.metroTextBoxBookPublisherId.PasswordChar = '\0';
            this.metroTextBoxBookPublisherId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxBookPublisherId.SelectedText = "";
            this.metroTextBoxBookPublisherId.SelectionLength = 0;
            this.metroTextBoxBookPublisherId.SelectionStart = 0;
            this.metroTextBoxBookPublisherId.ShortcutsEnabled = true;
            this.metroTextBoxBookPublisherId.Size = new System.Drawing.Size(415, 24);
            this.metroTextBoxBookPublisherId.TabIndex = 81;
            this.metroTextBoxBookPublisherId.UseSelectable = true;
            this.metroTextBoxBookPublisherId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxBookPublisherId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxBookISBN
            // 
            // 
            // 
            // 
            this.metroTextBoxBookISBN.CustomButton.Image = null;
            this.metroTextBoxBookISBN.CustomButton.Location = new System.Drawing.Point(393, 2);
            this.metroTextBoxBookISBN.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxBookISBN.CustomButton.Name = "";
            this.metroTextBoxBookISBN.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxBookISBN.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxBookISBN.CustomButton.TabIndex = 1;
            this.metroTextBoxBookISBN.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxBookISBN.CustomButton.UseSelectable = true;
            this.metroTextBoxBookISBN.CustomButton.Visible = false;
            this.metroTextBoxBookISBN.Lines = new string[0];
            this.metroTextBoxBookISBN.Location = new System.Drawing.Point(37, 94);
            this.metroTextBoxBookISBN.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxBookISBN.MaxLength = 32767;
            this.metroTextBoxBookISBN.Name = "metroTextBoxBookISBN";
            this.metroTextBoxBookISBN.PasswordChar = '\0';
            this.metroTextBoxBookISBN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxBookISBN.SelectedText = "";
            this.metroTextBoxBookISBN.SelectionLength = 0;
            this.metroTextBoxBookISBN.SelectionStart = 0;
            this.metroTextBoxBookISBN.ShortcutsEnabled = true;
            this.metroTextBoxBookISBN.Size = new System.Drawing.Size(415, 24);
            this.metroTextBoxBookISBN.TabIndex = 80;
            this.metroTextBoxBookISBN.Tag = "Search";
            this.metroTextBoxBookISBN.UseSelectable = true;
            this.metroTextBoxBookISBN.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxBookISBN.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // groupBox27
            // 
            this.groupBox27.BackColor = System.Drawing.Color.White;
            this.groupBox27.Controls.Add(this.metroButtonBookUpdate);
            this.groupBox27.Controls.Add(this.metroButtonBookList);
            this.groupBox27.Controls.Add(this.metroButtonBookAdd);
            this.groupBox27.Controls.Add(this.metroButtonBookDelete);
            this.groupBox27.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox27.Location = new System.Drawing.Point(995, 33);
            this.groupBox27.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox27.Size = new System.Drawing.Size(379, 336);
            this.groupBox27.TabIndex = 84;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Buttom Bar";
            // 
            // metroButtonBookUpdate
            // 
            this.metroButtonBookUpdate.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.metroButtonBookUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonBookUpdate.BackgroundImage")));
            this.metroButtonBookUpdate.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonBookUpdate.Location = new System.Drawing.Point(46, 117);
            this.metroButtonBookUpdate.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonBookUpdate.Name = "metroButtonBookUpdate";
            this.metroButtonBookUpdate.Size = new System.Drawing.Size(262, 48);
            this.metroButtonBookUpdate.TabIndex = 80;
            this.metroButtonBookUpdate.Text = "&Update";
            this.metroButtonBookUpdate.UseSelectable = true;
            this.metroButtonBookUpdate.Click += new System.EventHandler(this.metroButtonBookUpdate_Click);
            // 
            // metroButtonBookList
            // 
            this.metroButtonBookList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonBookList.BackgroundImage")));
            this.metroButtonBookList.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonBookList.Location = new System.Drawing.Point(46, 265);
            this.metroButtonBookList.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonBookList.Name = "metroButtonBookList";
            this.metroButtonBookList.Size = new System.Drawing.Size(262, 50);
            this.metroButtonBookList.TabIndex = 79;
            this.metroButtonBookList.Text = "&List";
            this.metroButtonBookList.UseSelectable = true;
            this.metroButtonBookList.Click += new System.EventHandler(this.metroButtonBookList_Click);
            // 
            // metroButtonBookAdd
            // 
            this.metroButtonBookAdd.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.metroButtonBookAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonBookAdd.BackgroundImage")));
            this.metroButtonBookAdd.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonBookAdd.Location = new System.Drawing.Point(46, 45);
            this.metroButtonBookAdd.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonBookAdd.Name = "metroButtonBookAdd";
            this.metroButtonBookAdd.Size = new System.Drawing.Size(262, 48);
            this.metroButtonBookAdd.TabIndex = 75;
            this.metroButtonBookAdd.Text = "&Add";
            this.metroButtonBookAdd.UseSelectable = true;
            this.metroButtonBookAdd.Click += new System.EventHandler(this.metroButtonBookAdd_Click);
            // 
            // metroButtonBookDelete
            // 
            this.metroButtonBookDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonBookDelete.BackgroundImage")));
            this.metroButtonBookDelete.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonBookDelete.Location = new System.Drawing.Point(46, 185);
            this.metroButtonBookDelete.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonBookDelete.Name = "metroButtonBookDelete";
            this.metroButtonBookDelete.Size = new System.Drawing.Size(262, 52);
            this.metroButtonBookDelete.TabIndex = 76;
            this.metroButtonBookDelete.Text = "&Delete";
            this.metroButtonBookDelete.UseSelectable = true;
            this.metroButtonBookDelete.Click += new System.EventHandler(this.metroButtonBookDelete_Click);
            // 
            // metroTextBoxBookCategoryId
            // 
            // 
            // 
            // 
            this.metroTextBoxBookCategoryId.CustomButton.Image = null;
            this.metroTextBoxBookCategoryId.CustomButton.Location = new System.Drawing.Point(393, 2);
            this.metroTextBoxBookCategoryId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxBookCategoryId.CustomButton.Name = "";
            this.metroTextBoxBookCategoryId.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxBookCategoryId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxBookCategoryId.CustomButton.TabIndex = 1;
            this.metroTextBoxBookCategoryId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxBookCategoryId.CustomButton.UseSelectable = true;
            this.metroTextBoxBookCategoryId.CustomButton.Visible = false;
            this.metroTextBoxBookCategoryId.Lines = new string[0];
            this.metroTextBoxBookCategoryId.Location = new System.Drawing.Point(37, 257);
            this.metroTextBoxBookCategoryId.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxBookCategoryId.MaxLength = 32767;
            this.metroTextBoxBookCategoryId.Name = "metroTextBoxBookCategoryId";
            this.metroTextBoxBookCategoryId.PasswordChar = '\0';
            this.metroTextBoxBookCategoryId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxBookCategoryId.SelectedText = "";
            this.metroTextBoxBookCategoryId.SelectionLength = 0;
            this.metroTextBoxBookCategoryId.SelectionStart = 0;
            this.metroTextBoxBookCategoryId.ShortcutsEnabled = true;
            this.metroTextBoxBookCategoryId.Size = new System.Drawing.Size(415, 24);
            this.metroTextBoxBookCategoryId.TabIndex = 79;
            this.metroTextBoxBookCategoryId.Tag = "Search";
            this.metroTextBoxBookCategoryId.UseSelectable = true;
            this.metroTextBoxBookCategoryId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxBookCategoryId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxBookBookTitle
            // 
            // 
            // 
            // 
            this.metroTextBoxBookBookTitle.CustomButton.Image = null;
            this.metroTextBoxBookBookTitle.CustomButton.Location = new System.Drawing.Point(393, 2);
            this.metroTextBoxBookBookTitle.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxBookBookTitle.CustomButton.Name = "";
            this.metroTextBoxBookBookTitle.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxBookBookTitle.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxBookBookTitle.CustomButton.TabIndex = 1;
            this.metroTextBoxBookBookTitle.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxBookBookTitle.CustomButton.UseSelectable = true;
            this.metroTextBoxBookBookTitle.CustomButton.Visible = false;
            this.metroTextBoxBookBookTitle.Lines = new string[0];
            this.metroTextBoxBookBookTitle.Location = new System.Drawing.Point(37, 334);
            this.metroTextBoxBookBookTitle.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxBookBookTitle.MaxLength = 32767;
            this.metroTextBoxBookBookTitle.Name = "metroTextBoxBookBookTitle";
            this.metroTextBoxBookBookTitle.PasswordChar = '\0';
            this.metroTextBoxBookBookTitle.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxBookBookTitle.SelectedText = "";
            this.metroTextBoxBookBookTitle.SelectionLength = 0;
            this.metroTextBoxBookBookTitle.SelectionStart = 0;
            this.metroTextBoxBookBookTitle.ShortcutsEnabled = true;
            this.metroTextBoxBookBookTitle.Size = new System.Drawing.Size(415, 24);
            this.metroTextBoxBookBookTitle.TabIndex = 77;
            this.metroTextBoxBookBookTitle.Tag = "Search";
            this.metroTextBoxBookBookTitle.UseSelectable = true;
            this.metroTextBoxBookBookTitle.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxBookBookTitle.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel42
            // 
            this.metroLabel42.AutoSize = true;
            this.metroLabel42.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel42.Location = new System.Drawing.Point(37, 58);
            this.metroLabel42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel42.Name = "metroLabel42";
            this.metroLabel42.Size = new System.Drawing.Size(61, 15);
            this.metroLabel42.TabIndex = 76;
            this.metroLabel42.Text = "BookISBN:";
            // 
            // metroLabel43
            // 
            this.metroLabel43.AutoSize = true;
            this.metroLabel43.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel43.Location = new System.Drawing.Point(37, 227);
            this.metroLabel43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel43.Name = "metroLabel43";
            this.metroLabel43.Size = new System.Drawing.Size(71, 15);
            this.metroLabel43.TabIndex = 75;
            this.metroLabel43.Text = "Category ID:";
            // 
            // metroLabel44
            // 
            this.metroLabel44.AutoSize = true;
            this.metroLabel44.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel44.Location = new System.Drawing.Point(37, 307);
            this.metroLabel44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel44.Name = "metroLabel44";
            this.metroLabel44.Size = new System.Drawing.Size(62, 15);
            this.metroLabel44.TabIndex = 74;
            this.metroLabel44.Text = "Book Title: ";
            // 
            // metroLabel45
            // 
            this.metroLabel45.AutoSize = true;
            this.metroLabel45.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel45.Location = new System.Drawing.Point(499, 56);
            this.metroLabel45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel45.Name = "metroLabel45";
            this.metroLabel45.Size = new System.Drawing.Size(66, 15);
            this.metroLabel45.TabIndex = 73;
            this.metroLabel45.Text = "Book Price: ";
            // 
            // groupBox28
            // 
            this.groupBox28.BackColor = System.Drawing.Color.White;
            this.groupBox28.Controls.Add(this.metroComboBoxBookSearch);
            this.groupBox28.Controls.Add(this.metroLabelBookInput);
            this.groupBox28.Controls.Add(this.metroTextBoxBookSearchId);
            this.groupBox28.Controls.Add(this.metroButtonBookSearch);
            this.groupBox28.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox28.Location = new System.Drawing.Point(1507, 41);
            this.groupBox28.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox28.Size = new System.Drawing.Size(376, 385);
            this.groupBox28.TabIndex = 90;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Search By";
            // 
            // metroComboBoxBookSearch
            // 
            this.metroComboBoxBookSearch.FormattingEnabled = true;
            this.metroComboBoxBookSearch.ItemHeight = 23;
            this.metroComboBoxBookSearch.Items.AddRange(new object[] {
            "Book ISBN",
            "Book Title"});
            this.metroComboBoxBookSearch.Location = new System.Drawing.Point(35, 105);
            this.metroComboBoxBookSearch.Margin = new System.Windows.Forms.Padding(4);
            this.metroComboBoxBookSearch.Name = "metroComboBoxBookSearch";
            this.metroComboBoxBookSearch.Size = new System.Drawing.Size(300, 29);
            this.metroComboBoxBookSearch.TabIndex = 92;
            this.metroComboBoxBookSearch.UseSelectable = true;
            this.metroComboBoxBookSearch.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxBookSearch_SelectedIndexChanged);
            // 
            // metroLabelBookInput
            // 
            this.metroLabelBookInput.AutoSize = true;
            this.metroLabelBookInput.Location = new System.Drawing.Point(35, 63);
            this.metroLabelBookInput.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabelBookInput.Name = "metroLabelBookInput";
            this.metroLabelBookInput.Size = new System.Drawing.Size(103, 19);
            this.metroLabelBookInput.TabIndex = 93;
            this.metroLabelBookInput.Text = "Messages Show:";
            // 
            // metroTextBoxBookSearchId
            // 
            // 
            // 
            // 
            this.metroTextBoxBookSearchId.CustomButton.Image = null;
            this.metroTextBoxBookSearchId.CustomButton.Location = new System.Drawing.Point(260, 2);
            this.metroTextBoxBookSearchId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxBookSearchId.CustomButton.Name = "";
            this.metroTextBoxBookSearchId.CustomButton.Size = new System.Drawing.Size(37, 37);
            this.metroTextBoxBookSearchId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxBookSearchId.CustomButton.TabIndex = 1;
            this.metroTextBoxBookSearchId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxBookSearchId.CustomButton.UseSelectable = true;
            this.metroTextBoxBookSearchId.CustomButton.Visible = false;
            this.metroTextBoxBookSearchId.Lines = new string[0];
            this.metroTextBoxBookSearchId.Location = new System.Drawing.Point(35, 189);
            this.metroTextBoxBookSearchId.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxBookSearchId.MaxLength = 32767;
            this.metroTextBoxBookSearchId.Name = "metroTextBoxBookSearchId";
            this.metroTextBoxBookSearchId.PasswordChar = '\0';
            this.metroTextBoxBookSearchId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxBookSearchId.SelectedText = "";
            this.metroTextBoxBookSearchId.SelectionLength = 0;
            this.metroTextBoxBookSearchId.SelectionStart = 0;
            this.metroTextBoxBookSearchId.ShortcutsEnabled = true;
            this.metroTextBoxBookSearchId.Size = new System.Drawing.Size(300, 42);
            this.metroTextBoxBookSearchId.TabIndex = 83;
            this.metroTextBoxBookSearchId.Tag = "Search";
            this.metroTextBoxBookSearchId.UseSelectable = true;
            this.metroTextBoxBookSearchId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxBookSearchId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonBookSearch
            // 
            this.metroButtonBookSearch.BackColor = System.Drawing.Color.White;
            this.metroButtonBookSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonBookSearch.BackgroundImage")));
            this.metroButtonBookSearch.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonBookSearch.Location = new System.Drawing.Point(31, 261);
            this.metroButtonBookSearch.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonBookSearch.Name = "metroButtonBookSearch";
            this.metroButtonBookSearch.Size = new System.Drawing.Size(304, 50);
            this.metroButtonBookSearch.TabIndex = 11;
            this.metroButtonBookSearch.Text = "&Search";
            this.metroButtonBookSearch.UseSelectable = true;
            this.metroButtonBookSearch.Click += new System.EventHandler(this.metroButtonBookSearch_Click);
            // 
            // metroTabPageCategory
            // 
            this.metroTabPageCategory.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroTabPageCategory.BackgroundImage")));
            this.metroTabPageCategory.Controls.Add(this.groupBox29);
            this.metroTabPageCategory.Controls.Add(this.groupBox30);
            this.metroTabPageCategory.Controls.Add(this.groupBox32);
            this.metroTabPageCategory.Font = new System.Drawing.Font("SimSun", 6.375F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.metroTabPageCategory.ForeColor = System.Drawing.Color.Yellow;
            this.metroTabPageCategory.HorizontalScrollbarBarColor = true;
            this.metroTabPageCategory.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageCategory.HorizontalScrollbarSize = 10;
            this.metroTabPageCategory.Location = new System.Drawing.Point(4, 37);
            this.metroTabPageCategory.Margin = new System.Windows.Forms.Padding(4);
            this.metroTabPageCategory.Name = "metroTabPageCategory";
            this.metroTabPageCategory.Size = new System.Drawing.Size(1918, 945);
            this.metroTabPageCategory.TabIndex = 6;
            this.metroTabPageCategory.Text = "Inventory For Category Management";
            this.metroTabPageCategory.VerticalScrollbarBarColor = true;
            this.metroTabPageCategory.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageCategory.VerticalScrollbarSize = 10;
            // 
            // groupBox29
            // 
            this.groupBox29.BackColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox29.Controls.Add(this.metroListViewCategory);
            this.groupBox29.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox29.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox29.Location = new System.Drawing.Point(35, 469);
            this.groupBox29.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox29.Size = new System.Drawing.Size(1848, 422);
            this.groupBox29.TabIndex = 85;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "List Category";
            // 
            // metroListViewCategory
            // 
            this.metroListViewCategory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader40,
            this.columnHeader42});
            this.metroListViewCategory.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewCategory.FullRowSelect = true;
            this.metroListViewCategory.GridLines = true;
            this.metroListViewCategory.Location = new System.Drawing.Point(0, 66);
            this.metroListViewCategory.Margin = new System.Windows.Forms.Padding(4);
            this.metroListViewCategory.Name = "metroListViewCategory";
            this.metroListViewCategory.OwnerDraw = true;
            this.metroListViewCategory.Size = new System.Drawing.Size(1836, 316);
            this.metroListViewCategory.TabIndex = 81;
            this.metroListViewCategory.UseCompatibleStateImageBehavior = false;
            this.metroListViewCategory.UseSelectable = true;
            this.metroListViewCategory.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader40
            // 
            this.columnHeader40.Text = "Category ID";
            this.columnHeader40.Width = 1014;
            // 
            // columnHeader42
            // 
            this.columnHeader42.Text = "Category Name";
            this.columnHeader42.Width = 831;
            // 
            // groupBox30
            // 
            this.groupBox30.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox30.Controls.Add(this.metroLabel46);
            this.groupBox30.Controls.Add(this.metroTextBoxCategoryName);
            this.groupBox30.Controls.Add(this.metroTextBoxCategoryId);
            this.groupBox30.Controls.Add(this.groupBox31);
            this.groupBox30.Controls.Add(this.metroLabel48);
            this.groupBox30.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox30.Location = new System.Drawing.Point(35, 53);
            this.groupBox30.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox30.Size = new System.Drawing.Size(1268, 380);
            this.groupBox30.TabIndex = 84;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Category Info";
            // 
            // metroLabel46
            // 
            this.metroLabel46.AutoSize = true;
            this.metroLabel46.Location = new System.Drawing.Point(101, 201);
            this.metroLabel46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel46.Name = "metroLabel46";
            this.metroLabel46.Size = new System.Drawing.Size(107, 19);
            this.metroLabel46.TabIndex = 82;
            this.metroLabel46.Text = "Category Name:";
            // 
            // metroTextBoxCategoryName
            // 
            // 
            // 
            // 
            this.metroTextBoxCategoryName.CustomButton.Image = null;
            this.metroTextBoxCategoryName.CustomButton.Location = new System.Drawing.Point(510, 2);
            this.metroTextBoxCategoryName.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxCategoryName.CustomButton.Name = "";
            this.metroTextBoxCategoryName.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxCategoryName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxCategoryName.CustomButton.TabIndex = 1;
            this.metroTextBoxCategoryName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxCategoryName.CustomButton.UseSelectable = true;
            this.metroTextBoxCategoryName.CustomButton.Visible = false;
            this.metroTextBoxCategoryName.Lines = new string[0];
            this.metroTextBoxCategoryName.Location = new System.Drawing.Point(101, 239);
            this.metroTextBoxCategoryName.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxCategoryName.MaxLength = 32767;
            this.metroTextBoxCategoryName.Name = "metroTextBoxCategoryName";
            this.metroTextBoxCategoryName.PasswordChar = '\0';
            this.metroTextBoxCategoryName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxCategoryName.SelectedText = "";
            this.metroTextBoxCategoryName.SelectionLength = 0;
            this.metroTextBoxCategoryName.SelectionStart = 0;
            this.metroTextBoxCategoryName.ShortcutsEnabled = true;
            this.metroTextBoxCategoryName.Size = new System.Drawing.Size(532, 24);
            this.metroTextBoxCategoryName.TabIndex = 81;
            this.metroTextBoxCategoryName.UseSelectable = true;
            this.metroTextBoxCategoryName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxCategoryName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxCategoryId
            // 
            // 
            // 
            // 
            this.metroTextBoxCategoryId.CustomButton.Image = null;
            this.metroTextBoxCategoryId.CustomButton.Location = new System.Drawing.Point(510, 2);
            this.metroTextBoxCategoryId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxCategoryId.CustomButton.Name = "";
            this.metroTextBoxCategoryId.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.metroTextBoxCategoryId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxCategoryId.CustomButton.TabIndex = 1;
            this.metroTextBoxCategoryId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxCategoryId.CustomButton.UseSelectable = true;
            this.metroTextBoxCategoryId.CustomButton.Visible = false;
            this.metroTextBoxCategoryId.Lines = new string[0];
            this.metroTextBoxCategoryId.Location = new System.Drawing.Point(101, 128);
            this.metroTextBoxCategoryId.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxCategoryId.MaxLength = 32767;
            this.metroTextBoxCategoryId.Name = "metroTextBoxCategoryId";
            this.metroTextBoxCategoryId.PasswordChar = '\0';
            this.metroTextBoxCategoryId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxCategoryId.SelectedText = "";
            this.metroTextBoxCategoryId.SelectionLength = 0;
            this.metroTextBoxCategoryId.SelectionStart = 0;
            this.metroTextBoxCategoryId.ShortcutsEnabled = true;
            this.metroTextBoxCategoryId.Size = new System.Drawing.Size(532, 24);
            this.metroTextBoxCategoryId.TabIndex = 80;
            this.metroTextBoxCategoryId.Tag = "Search";
            this.metroTextBoxCategoryId.UseSelectable = true;
            this.metroTextBoxCategoryId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxCategoryId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // groupBox31
            // 
            this.groupBox31.BackColor = System.Drawing.Color.White;
            this.groupBox31.Controls.Add(this.metroButtonCategoryList);
            this.groupBox31.Controls.Add(this.metroButtonCategoryAdd);
            this.groupBox31.Controls.Add(this.metroButtonCategoryUpdate);
            this.groupBox31.Controls.Add(this.metroButtonCategoryDelete);
            this.groupBox31.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox31.Location = new System.Drawing.Point(754, 24);
            this.groupBox31.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox31.Size = new System.Drawing.Size(476, 344);
            this.groupBox31.TabIndex = 77;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Buttom Bar";
            // 
            // metroButtonCategoryList
            // 
            this.metroButtonCategoryList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonCategoryList.BackgroundImage")));
            this.metroButtonCategoryList.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonCategoryList.Location = new System.Drawing.Point(60, 284);
            this.metroButtonCategoryList.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonCategoryList.Name = "metroButtonCategoryList";
            this.metroButtonCategoryList.Size = new System.Drawing.Size(392, 50);
            this.metroButtonCategoryList.TabIndex = 79;
            this.metroButtonCategoryList.Text = "&List";
            this.metroButtonCategoryList.UseSelectable = true;
            this.metroButtonCategoryList.Click += new System.EventHandler(this.metroButtonCategoryList_Click);
            // 
            // metroButtonCategoryAdd
            // 
            this.metroButtonCategoryAdd.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.metroButtonCategoryAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonCategoryAdd.BackgroundImage")));
            this.metroButtonCategoryAdd.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonCategoryAdd.Location = new System.Drawing.Point(60, 64);
            this.metroButtonCategoryAdd.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonCategoryAdd.Name = "metroButtonCategoryAdd";
            this.metroButtonCategoryAdd.Size = new System.Drawing.Size(392, 48);
            this.metroButtonCategoryAdd.TabIndex = 75;
            this.metroButtonCategoryAdd.Text = "&Add";
            this.metroButtonCategoryAdd.UseSelectable = true;
            this.metroButtonCategoryAdd.Click += new System.EventHandler(this.metroButtonCategoryAdd_Click);
            // 
            // metroButtonCategoryUpdate
            // 
            this.metroButtonCategoryUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonCategoryUpdate.BackgroundImage")));
            this.metroButtonCategoryUpdate.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonCategoryUpdate.Location = new System.Drawing.Point(60, 140);
            this.metroButtonCategoryUpdate.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonCategoryUpdate.Name = "metroButtonCategoryUpdate";
            this.metroButtonCategoryUpdate.Size = new System.Drawing.Size(392, 48);
            this.metroButtonCategoryUpdate.TabIndex = 77;
            this.metroButtonCategoryUpdate.Text = "&Update";
            this.metroButtonCategoryUpdate.UseSelectable = true;
            this.metroButtonCategoryUpdate.Click += new System.EventHandler(this.metroButtonCategoryUpdate_Click);
            // 
            // metroButtonCategoryDelete
            // 
            this.metroButtonCategoryDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonCategoryDelete.BackgroundImage")));
            this.metroButtonCategoryDelete.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonCategoryDelete.Location = new System.Drawing.Point(60, 204);
            this.metroButtonCategoryDelete.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonCategoryDelete.Name = "metroButtonCategoryDelete";
            this.metroButtonCategoryDelete.Size = new System.Drawing.Size(392, 52);
            this.metroButtonCategoryDelete.TabIndex = 76;
            this.metroButtonCategoryDelete.Text = "&Delete";
            this.metroButtonCategoryDelete.UseSelectable = true;
            this.metroButtonCategoryDelete.Click += new System.EventHandler(this.metroButtonCategoryDelete_Click);
            // 
            // metroLabel48
            // 
            this.metroLabel48.AutoSize = true;
            this.metroLabel48.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel48.Location = new System.Drawing.Point(101, 96);
            this.metroLabel48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel48.Name = "metroLabel48";
            this.metroLabel48.Size = new System.Drawing.Size(71, 15);
            this.metroLabel48.TabIndex = 76;
            this.metroLabel48.Text = "Category ID:";
            // 
            // groupBox32
            // 
            this.groupBox32.BackColor = System.Drawing.Color.White;
            this.groupBox32.Controls.Add(this.metroComboBoxCategorySearch);
            this.groupBox32.Controls.Add(this.metroLabelCategoryInput);
            this.groupBox32.Controls.Add(this.metroTextBoxCategorySearchId);
            this.groupBox32.Controls.Add(this.metroButtonCategory);
            this.groupBox32.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox32.Location = new System.Drawing.Point(1347, 53);
            this.groupBox32.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox32.Size = new System.Drawing.Size(528, 380);
            this.groupBox32.TabIndex = 83;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Search By";
            // 
            // metroComboBoxCategorySearch
            // 
            this.metroComboBoxCategorySearch.FormattingEnabled = true;
            this.metroComboBoxCategorySearch.ItemHeight = 23;
            this.metroComboBoxCategorySearch.Items.AddRange(new object[] {
            "Category ID",
            "CategoryName"});
            this.metroComboBoxCategorySearch.Location = new System.Drawing.Point(26, 96);
            this.metroComboBoxCategorySearch.Margin = new System.Windows.Forms.Padding(4);
            this.metroComboBoxCategorySearch.Name = "metroComboBoxCategorySearch";
            this.metroComboBoxCategorySearch.Size = new System.Drawing.Size(478, 29);
            this.metroComboBoxCategorySearch.TabIndex = 11;
            this.metroComboBoxCategorySearch.UseSelectable = true;
            this.metroComboBoxCategorySearch.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxCategorySearch_SelectedIndexChanged);
            // 
            // metroLabelCategoryInput
            // 
            this.metroLabelCategoryInput.AutoSize = true;
            this.metroLabelCategoryInput.Location = new System.Drawing.Point(26, 55);
            this.metroLabelCategoryInput.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabelCategoryInput.Name = "metroLabelCategoryInput";
            this.metroLabelCategoryInput.Size = new System.Drawing.Size(103, 19);
            this.metroLabelCategoryInput.TabIndex = 85;
            this.metroLabelCategoryInput.Text = "Messages Show:";
            // 
            // metroTextBoxCategorySearchId
            // 
            // 
            // 
            // 
            this.metroTextBoxCategorySearchId.CustomButton.Image = null;
            this.metroTextBoxCategorySearchId.CustomButton.Location = new System.Drawing.Point(438, 2);
            this.metroTextBoxCategorySearchId.CustomButton.Margin = new System.Windows.Forms.Padding(4);
            this.metroTextBoxCategorySearchId.CustomButton.Name = "";
            this.metroTextBoxCategorySearchId.CustomButton.Size = new System.Drawing.Size(37, 37);
            this.metroTextBoxCategorySearchId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxCategorySearchId.CustomButton.TabIndex = 1;
            this.metroTextBoxCategorySearchId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxCategorySearchId.CustomButton.UseSelectable = true;
            this.metroTextBoxCategorySearchId.CustomButton.Visible = false;
            this.metroTextBoxCategorySearchId.Lines = new string[0];
            this.metroTextBoxCategorySearchId.Location = new System.Drawing.Point(26, 168);
            this.metroTextBoxCategorySearchId.Margin = new System.Windows.Forms.Padding(6);
            this.metroTextBoxCategorySearchId.MaxLength = 32767;
            this.metroTextBoxCategorySearchId.Name = "metroTextBoxCategorySearchId";
            this.metroTextBoxCategorySearchId.PasswordChar = '\0';
            this.metroTextBoxCategorySearchId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxCategorySearchId.SelectedText = "";
            this.metroTextBoxCategorySearchId.SelectionLength = 0;
            this.metroTextBoxCategorySearchId.SelectionStart = 0;
            this.metroTextBoxCategorySearchId.ShortcutsEnabled = true;
            this.metroTextBoxCategorySearchId.Size = new System.Drawing.Size(478, 42);
            this.metroTextBoxCategorySearchId.TabIndex = 83;
            this.metroTextBoxCategorySearchId.Tag = "Search";
            this.metroTextBoxCategorySearchId.UseSelectable = true;
            this.metroTextBoxCategorySearchId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxCategorySearchId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonCategory
            // 
            this.metroButtonCategory.BackColor = System.Drawing.Color.White;
            this.metroButtonCategory.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonCategory.BackgroundImage")));
            this.metroButtonCategory.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonCategory.Location = new System.Drawing.Point(26, 256);
            this.metroButtonCategory.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonCategory.Name = "metroButtonCategory";
            this.metroButtonCategory.Size = new System.Drawing.Size(482, 50);
            this.metroButtonCategory.TabIndex = 11;
            this.metroButtonCategory.Text = "&Search";
            this.metroButtonCategory.UseSelectable = true;
            this.metroButtonCategory.Click += new System.EventHandler(this.metroButtonCategory_Click);
            // 
            // metroContextMenu1
            // 
            this.metroContextMenu1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.metroContextMenu1.Name = "metroContextMenu1";
            this.metroContextMenu1.Size = new System.Drawing.Size(61, 4);
            // 
            // metroButtonAppExit
            // 
            this.metroButtonAppExit.BackColor = System.Drawing.Color.White;
            this.metroButtonAppExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("metroButtonAppExit.BackgroundImage")));
            this.metroButtonAppExit.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButtonAppExit.Location = new System.Drawing.Point(1714, 58);
            this.metroButtonAppExit.Margin = new System.Windows.Forms.Padding(6);
            this.metroButtonAppExit.Name = "metroButtonAppExit";
            this.metroButtonAppExit.Size = new System.Drawing.Size(262, 50);
            this.metroButtonAppExit.TabIndex = 12;
            this.metroButtonAppExit.Text = "&Exit";
            this.metroButtonAppExit.UseSelectable = true;
            this.metroButtonAppExit.Click += new System.EventHandler(this.metroButtonAppExit_Click);
            // 
            // HiTechSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(2041, 1142);
            this.ContextMenuStrip = this.metroContextMenu1;
            this.ControlBox = false;
            this.Controls.Add(this.metroButtonAppExit);
            this.Controls.Add(this.MainTab);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "HiTechSystem";
            this.Padding = new System.Windows.Forms.Padding(20, 120, 20, 20);
            this.RightToLeftLayout = true;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Style = MetroFramework.MetroColorStyle.Default;
            this.Text = "HiTechManagementSystem";
            this.Load += new System.EventHandler(this.HiTechSystem_Load);
            this.MainTab.ResumeLayout(false);
            this.metroTabPageUserManagement.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.metroTabPageMISManager.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.metroTabPageSalesManager.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClients)).EndInit();
            this.metroTabPageOrderClerks.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.metroTabPageAccountant.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.metroTabPageAuthor.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.metroTabPageItem.ResumeLayout(false);
            this.groupBox25.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.metroTabPageCategory.ResumeLayout(false);
            this.groupBox29.ResumeLayout(false);
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl MainTab;
        private MetroFramework.Controls.MetroTabPage metroTabPageUserManagement;
        private MetroFramework.Controls.MetroTabPage metroTabPageMISManager;
        private MetroFramework.Controls.MetroTabPage metroTabPageSalesManager;
        private MetroFramework.Controls.MetroTabPage metroTabPageOrderClerks;
        private MetroFramework.Controls.MetroTabPage metroTabPageItem;
        private MetroFramework.Controls.MetroTabPage metroTabPageAuthor;
        private MetroFramework.Controls.MetroTabPage metroTabPageCategory;
        private MetroFramework.Controls.MetroTabPage metroTabPageAccountant;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.GroupBox groupBox23;
        private MetroFramework.Controls.MetroButton btnUserSearch;
        private System.Windows.Forms.GroupBox groupBox24;
        private MetroFramework.Controls.MetroButton btnUserList;
        private MetroFramework.Controls.MetroButton btnUserAdd;
        private MetroFramework.Controls.MetroButton btnUserUpdate;
        private MetroFramework.Controls.MetroButton btnUserDelete;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox metroTextUserID;
        private MetroFramework.Controls.MetroTextBox metroTextBoxUserName;
        private MetroFramework.Controls.MetroTextBox metroTextBoxUserEmail;
        private MetroFramework.Controls.MetroTextBox metroTextBoxUserPassword;
        private MetroFramework.Controls.MetroListView lvUserInfo;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private MetroFramework.Controls.MetroTextBox metroTextBoxUserSearch;
        private MetroFramework.Controls.MetroListView metroListViewEmployee;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.GroupBox groupBox3;
        private MetroFramework.Controls.MetroButton metroButtonEmpList;
        private MetroFramework.Controls.MetroButton metroButtonEmpAdd;
        private MetroFramework.Controls.MetroButton metroButtonEmpUpdate;
        private MetroFramework.Controls.MetroButton metroButtonEmpDelete;
        private System.Windows.Forms.GroupBox groupBox2;
        private MetroFramework.Controls.MetroTextBox metroTextBoxSearchID;
        private MetroFramework.Controls.MetroButton metroButtonEmpSearch;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroTextBox metroTextBoxEmployeeNumber;
        private MetroFramework.Controls.MetroTextBox metroTextBoxFirstName;
        private MetroFramework.Controls.MetroTextBox metroTextBoxJobTitle;
        private MetroFramework.Controls.MetroTextBox metroTextBoxLastName;
        private MetroFramework.Controls.MetroLabel metroLabelFN;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox metroTextBoxEmpPhoneNumber;
        private MetroFramework.Controls.MetroTextBox metroTextBoxEmpEmail;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox7;
        private MetroFramework.Controls.MetroButton metroButtonClientList;
        private MetroFramework.Controls.MetroButton metroButtonClientAdd;
        private MetroFramework.Controls.MetroButton metroButtonClientUpdate;
        private MetroFramework.Controls.MetroButton metroButtonClientDelete;
        private System.Windows.Forms.GroupBox groupBox6;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClientSearchID;
        private System.Windows.Forms.GroupBox groupBox5;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClientCity;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClientID;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClientName;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClientEmail;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClientPhoneNumber;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private System.Windows.Forms.DataGridView dataGridViewClients;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroTextBox metroTextBoxUserEmpNumber;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClientFax;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClientCredit;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClientPostCode;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClientStreet;
        private System.Windows.Forms.GroupBox groupBox11;
        private MetroFramework.Controls.MetroButton metroButtonOrderList;
        private MetroFramework.Controls.MetroButton metroButtonOrderAdd;
        private MetroFramework.Controls.MetroButton metroButtonOderUpdate;
        private MetroFramework.Controls.MetroButton metroButtonOrderDelete;
        private System.Windows.Forms.GroupBox groupBox10;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOrderSearchId;
        private System.Windows.Forms.GroupBox groupBox9;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private MetroFramework.Controls.MetroLabel metroLabel22;
        private MetroFramework.Controls.MetroLabel metroLabel;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOderQuantity;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOrderTotal;
        private MetroFramework.Controls.MetroLabel metroLabel24;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOdrtPayment;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOderClientID;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOderItemId;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOderUnitPrice;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOderItemName;
        private MetroFramework.Controls.MetroLabel metroLabel25;
        private MetroFramework.Controls.MetroLabel metroLabel26;
        private MetroFramework.Controls.MetroLabel metroLabel27;
        private MetroFramework.Controls.MetroLabel metroLabel28;
        private MetroFramework.Controls.MetroContextMenu metroContextMenu1;
        private System.Windows.Forms.GroupBox groupBox12;
        private MetroFramework.Controls.MetroListView metroListViewOrder;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.GroupBox groupBox13;
        private MetroFramework.Controls.MetroListView metroListViewInvoice;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox15;
        private MetroFramework.Controls.MetroTextBox metroTextBoxInvoiceSearchId;
        private MetroFramework.Controls.MetroButton metroButtonInvoiceSearch;
        private System.Windows.Forms.GroupBox groupBox16;
        private MetroFramework.Controls.MetroButton metroButtonInvoiceList;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOrderId;
        private MetroFramework.Controls.MetroLabel metroLabel33;
        private System.Windows.Forms.GroupBox groupBox17;
        private MetroFramework.Controls.MetroButton metroButtonClientSearch;
        private MetroFramework.Controls.MetroButton metroButtonOrderSearch;
        private MetroFramework.Controls.MetroLabel metroLabelInput;
        private MetroFramework.Controls.MetroComboBox metroComboBoxUserSearch;
        private MetroFramework.Controls.MetroComboBox metroComboBoxMISSearch;
        private MetroFramework.Controls.MetroLabel metroLabelMISInput;
        private MetroFramework.Controls.MetroButton metroButtonAppExit;
        private MetroFramework.Controls.MetroComboBox metroComboBoxSaleSearch;
        private MetroFramework.Controls.MetroLabel metroLabelSaleInput;
        private MetroFramework.Controls.MetroComboBox metroComboBoxOrderSearch;
        private MetroFramework.Controls.MetroLabel metroLabelOrderInput;
        private MetroFramework.Controls.MetroComboBox metroComboBoxInvoiceSearch;
        private MetroFramework.Controls.MetroLabel metroLabelInvoiceInput;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOrderShippingDate;
        private MetroFramework.Controls.MetroTextBox metroTextBoxOrderDate;
        private System.Windows.Forms.GroupBox groupBox18;
        private MetroFramework.Controls.MetroListView metroListViewAuthor;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.GroupBox groupBox19;
        private MetroFramework.Controls.MetroTextBox metroTextBoxAuthorLN;
        private MetroFramework.Controls.MetroLabel metroLabel34;
        private MetroFramework.Controls.MetroTextBox metroTextBoxAuthorPublisherId;
        private MetroFramework.Controls.MetroTextBox metroTextBoxAuthorId;
        private System.Windows.Forms.GroupBox groupBox20;
        private MetroFramework.Controls.MetroButton metroButtonAuthorList;
        private MetroFramework.Controls.MetroButton metroButtonAuthorAdd;
        private MetroFramework.Controls.MetroButton metroButtonAuthorDelete;
        private MetroFramework.Controls.MetroTextBox metroTextBoxAuthorBookISBN;
        private MetroFramework.Controls.MetroTextBox metroTextBoxAuthorFN;
        private MetroFramework.Controls.MetroLabel metroLabel35;
        private MetroFramework.Controls.MetroLabel metroLabel36;
        private MetroFramework.Controls.MetroLabel metroLabel37;
        private MetroFramework.Controls.MetroLabel metroLabel38;
        private System.Windows.Forms.GroupBox groupBox21;
        private MetroFramework.Controls.MetroComboBox metroComboBoxAuthorSearch;
        private MetroFramework.Controls.MetroLabel metroLabelAuthorInput;
        private MetroFramework.Controls.MetroTextBox metroTextBoxAuthorSearchId;
        private MetroFramework.Controls.MetroButton metroButtonAuthorSearch;
        private MetroFramework.Controls.MetroButton metroButtonAuthorUpdate;
        private MetroFramework.Controls.MetroTextBox metroTextBoxAuthorEmail;
        private MetroFramework.Controls.MetroLabel metroLabel39;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.GroupBox groupBox25;
        private MetroFramework.Controls.MetroListView metroListViewBook;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.GroupBox groupBox26;
        private MetroFramework.Controls.MetroTextBox metroTextBoxBookPrice;
        private MetroFramework.Controls.MetroLabel metroLabel41;
        private MetroFramework.Controls.MetroTextBox metroTextBoxBookPublisherId;
        private MetroFramework.Controls.MetroTextBox metroTextBoxBookISBN;
        private System.Windows.Forms.GroupBox groupBox27;
        private MetroFramework.Controls.MetroButton metroButtonBookUpdate;
        private MetroFramework.Controls.MetroButton metroButtonBookList;
        private MetroFramework.Controls.MetroButton metroButtonBookAdd;
        private MetroFramework.Controls.MetroButton metroButtonBookDelete;
        private MetroFramework.Controls.MetroTextBox metroTextBoxBookCategoryId;
        private MetroFramework.Controls.MetroTextBox metroTextBoxBookBookTitle;
        private MetroFramework.Controls.MetroLabel metroLabel42;
        private MetroFramework.Controls.MetroLabel metroLabel43;
        private MetroFramework.Controls.MetroLabel metroLabel44;
        private MetroFramework.Controls.MetroLabel metroLabel45;
        private System.Windows.Forms.GroupBox groupBox28;
        private MetroFramework.Controls.MetroComboBox metroComboBoxBookSearch;
        private MetroFramework.Controls.MetroLabel metroLabelBookInput;
        private MetroFramework.Controls.MetroTextBox metroTextBoxBookSearchId;
        private MetroFramework.Controls.MetroButton metroButtonBookSearch;
        private MetroFramework.Controls.MetroTextBox metroTextBoxBookOrderQuantity;
        private MetroFramework.Controls.MetroLabel metroLabel40;
        private MetroFramework.Controls.MetroTextBox metroTextBoxBookQOH;
        private MetroFramework.Controls.MetroTextBox metroTextBoxBookPublishedDate;
        private MetroFramework.Controls.MetroLabel metroLabel47;
        private MetroFramework.Controls.MetroLabel metroLabel49;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.GroupBox groupBox29;
        private MetroFramework.Controls.MetroListView metroListViewCategory;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.GroupBox groupBox30;
        private MetroFramework.Controls.MetroLabel metroLabel46;
        private MetroFramework.Controls.MetroTextBox metroTextBoxCategoryName;
        private MetroFramework.Controls.MetroTextBox metroTextBoxCategoryId;
        private System.Windows.Forms.GroupBox groupBox31;
        private MetroFramework.Controls.MetroButton metroButtonCategoryList;
        private MetroFramework.Controls.MetroButton metroButtonCategoryAdd;
        private MetroFramework.Controls.MetroButton metroButtonCategoryUpdate;
        private MetroFramework.Controls.MetroButton metroButtonCategoryDelete;
        private MetroFramework.Controls.MetroLabel metroLabel48;
        private System.Windows.Forms.GroupBox groupBox32;
        private MetroFramework.Controls.MetroComboBox metroComboBoxCategorySearch;
        private MetroFramework.Controls.MetroLabel metroLabelCategoryInput;
        private MetroFramework.Controls.MetroTextBox metroTextBoxCategorySearchId;
        private MetroFramework.Controls.MetroButton metroButtonCategory;
    }
}