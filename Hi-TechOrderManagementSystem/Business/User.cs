﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiTechOrderManagementSystem.DataAccess;
using System.Data.SqlClient;

namespace HiTechOrderManagementSystem.Business
{
    public class User
    {
        private int userId;
        private int employeeNumber;
        private string userName;
        private string password;
        private string email;

        public int UserId { get => userId; set => userId = value; }
        public int EmployeeNumber { get => employeeNumber; set => employeeNumber = value; }
        public string UserName { get => userName; set => userName = value; }
        public string Password { get => password; set => password = value; }
        public string Email { get => email; set => email = value; }

        public bool VerifyUser(User aUser)
        {
            return UserDB.IsValidUser(aUser);
        }

        public void SaveRecord(User user)
        {
            UserDB.SaveUser(user);
        }

        public void UpdateRecord(User user)
        {
            UserDB.UpdateUser(user);
        }

        public void DeleteRecord(User user)
        {
            UserDB.DeleteUser(user);
        }

        public SqlDataReader SearchRecord(User user, int searchBy)
        {
            return UserDB.SearchUser(user,searchBy);
        }

        public SqlDataReader ListRecord()
        {
            return UserDB.ListUser();
        }



    }
}
