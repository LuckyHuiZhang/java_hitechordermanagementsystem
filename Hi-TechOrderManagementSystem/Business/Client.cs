﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiTechOrderManagementSystem.Business
{
    public class Client
    {
        private int clientId;
        private string clientName;
        private string clientPhoneNumber;
        private string clientEmail;
        private string clientFax;
        private string clientCity;
        private string clientStreet;
        private string clientPostCode;
        private int clientCredit;

        public int ClientId { get => clientId; set => clientId = value; }
        public string ClientName { get => clientName; set => clientName = value; }
        public string ClientPhoneNumber { get => clientPhoneNumber; set => clientPhoneNumber = value; }
        public string ClientEmail { get => clientEmail; set => clientEmail = value; }
        public string ClientFax { get => clientFax; set => clientFax = value; }
        public string ClientCity { get => clientCity; set => clientCity = value; }
        public string ClientStreet { get => clientStreet; set => clientStreet = value; }
        public string ClientPostCode { get => clientPostCode; set => clientPostCode = value; }
        public int ClientCredit { get => clientCredit; set => clientCredit = value; }
    }
}
