﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using HiTechOrderManagementSystem.Business;

namespace HiTechOrderManagementSystem.DataAccess
{
    public static class InvoiceDB
    {
        //public static void SaveInvoice(Invoice invoice)
        //{
        //    SqlConnection connDB = UtilityDB.ConnectDB();
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = connDB;

        //    string sqlInsert = "INSERT INTO Invoices(InvoiceNumber,OrderId,ItemId,ClientName,OrderDate)"
        //        + "VALUES (@InvoiceNumber,@OrderId,@ItemId,@ClientName,@OrderDate)";
        //    cmd.CommandText = sqlInsert;
        //    cmd.Parameters.AddWithValue("@InvoiceNumber", invoice.InvoiceNumber);
        //    cmd.Parameters.AddWithValue("@OrderId", invoice.OrderId);
        //    cmd.Parameters.AddWithValue("@ItemId", invoice.ItemId);
        //    cmd.Parameters.AddWithValue("@ClientName", invoice.ClientName);
        //    cmd.Parameters.AddWithValue("@OrderDate", invoice.OrderDate);
        //    cmd.ExecuteNonQuery();
        //    connDB.Close();

        //}

        //public static void UpdateInvoice(Invoice invoice)
        //{
          
        //    SqlConnection connDB = UtilityDB.ConnectDB();
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = connDB;

        //    cmd.CommandText = "UPDATE Invoices " +
        //                      " SET InvoiceNumber = " + invoice.InvoiceNumber +
        //                      ",OrderId= " + invoice.OrderId + 
        //                      ",ItemId= " + invoice.ItemId + 
        //                      ",ClientName ='" + invoice.ClientName + "'" +
        //                      ",OrderDate= '" + invoice.OrderDate + "'" +
        //                      " WHERE InvoiceNumber = " + invoice.InvoiceNumber;

        //    cmd.ExecuteNonQuery();
        //    connDB.Close();
        //}

        //public static void DeleteInvoice(Invoice invoice)
        //{
        //    SqlConnection connDB = UtilityDB.ConnectDB();
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = connDB;
        //    cmd.CommandText = "DELETE FROM Invoices WHERE InvoiceNumber = " + invoice.InvoiceNumber;
        //    cmd.ExecuteNonQuery();
        //    connDB.Close();
        //}

        public static SqlDataReader SearchInvoice(Invoice invoice, int searchBy)
        {
            string searchColumn = String.Empty;
            switch (searchBy)
            {
                case 0:
                    searchColumn = "InvoiceNumber = " + invoice.InvoiceNumber + ";";
                    break;
                case 1:
                    searchColumn = "ClientName = '" + invoice.ClientName + "';";
                    break;

                default:
                    break;
            }
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "SELECT * FROM Invoices " +
                             "WHERE  " + searchColumn;
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }

        public static SqlDataReader ListInvoice()
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "SELECT * FROM Invoices ORDER BY InvoiceNumber ";
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }
    }
}
