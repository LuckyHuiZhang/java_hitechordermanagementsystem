﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiTechOrderManagementSystem.DataAccess;
using System.Data.SqlClient;

namespace HiTechOrderManagementSystem.Business
{
    public class Author
    {
        private int authorId;
        private int publisherId;
        private int bookISBN;
        private string authorFirstName;
        private string authorLastName;
        private string authorEmail;


        public int AuthorId { get => authorId; set => authorId = value; }
        public int PublisherId { get => publisherId; set => publisherId = value; }
        public int BookISBN { get => bookISBN; set => bookISBN = value; }
        public string AuthorFirstName { get => authorFirstName; set => authorFirstName = value; }
        public string AuthorLastName { get => authorLastName; set => authorLastName = value; }
        public string AuthorEmail { get => authorEmail; set => authorEmail = value; }
    

        public void SaveRecord(Author author)
        {
            AuthorDB.SaveAuthor(author);
        }

        public void UpdateRecord(Author author)
        {
            AuthorDB.UpdateAuthor(author);
        }

        public void DeleteRecord(Author author)
        {
            AuthorDB.DeleteAuthor(author);
        }

        public SqlDataReader SearchRecord(Author author, int searchBy)
        {
            return AuthorDB.SearchAuthor(author, searchBy);
        }

        public SqlDataReader ListRecord()
        {
            return AuthorDB.ListAuthor();
        }
    }
}
