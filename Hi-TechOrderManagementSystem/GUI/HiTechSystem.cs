﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using HiTechOrderManagementSystem.Business;
using HiTechOrderManagementSystem.DataAccess;
using MetroFramework;
using System.Data.Entity.Migrations;


namespace HiTechOrderManagementSystem
{
    public partial class HiTechSystem : MetroFramework.Forms.MetroForm
    {
        public HiTechSystem()
        {
            InitializeComponent();
        }
        //******************************************************************************************************************//
        //===================================================== User ======================================================//
        //*****************************************************************************************************************//
        private void MyDisplay(SqlDataReader dr)
        {
            this.lvUserInfo.Items.Clear();
            while (dr.Read())
            {
                string[] row =
                {
                    dr[0].ToString(),
                    dr[1].ToString(),
                    dr[2].ToString(),
                    dr[3].ToString(),
                    dr[4].ToString()
            };
                ListViewItem lvi = new ListViewItem(row);
                this.lvUserInfo.Items.Add(lvi);
            }
            //dr.Close();

            this.lvUserInfo.Focus();
            if (this.lvUserInfo.Items.Count > 0) this.lvUserInfo.Items[0].Selected = true;

        }

        private void MyDisplayListview(List<Business.User> list)
        {
            if (list == null || list.Count() <= 0)
            {
                return;
            }
            lvUserInfo.Items.Clear();
            foreach (var user in list)
            {
                ListViewItem item = new ListViewItem();
                item.Text = user.UserId.ToString();
                item.SubItems.Add(user.EmployeeNumber.ToString());
                item.SubItems.Add(user.UserName);
                item.SubItems.Add(user.Password);
                item.SubItems.Add(user.Email);
                lvUserInfo.Items.Add(item);

            }
        }

        private void btnUserAdd_Click_1(object sender, EventArgs e)
        {
            User user = new User();
            user.UserId = Convert.ToInt32(metroTextUserID.Text.Trim());
            user.EmployeeNumber= Convert.ToInt32(metroTextBoxUserEmpNumber.Text.Trim());
            user.UserName = metroTextBoxUserName.Text.Trim();
            user.Password = metroTextBoxUserPassword.Text.Trim();
            user.Email = metroTextBoxUserEmail.Text.Trim();
            user.SaveRecord(user);
            MessageBox.Show("User Information Has Been Save Successfully!");
          
        }

        private void btnUserUpdate_Click_1(object sender, EventArgs e)
        {
            User user = new User();
            user.UserId = Convert.ToInt32(metroTextUserID.Text.Trim());
            user.EmployeeNumber = Convert.ToInt32(metroTextBoxUserEmpNumber.Text.Trim());
            user.UserName = metroTextBoxUserName.Text.Trim();
            user.Password = metroTextBoxUserPassword.Text.Trim();
            user.Email = metroTextBoxUserEmail.Text.Trim();
            user.UpdateRecord(user);
            MessageBox.Show("User Information Has Been Update Successfully!");
            
        }

        private void btnUserDelete_Click_1(object sender, EventArgs e)
        {
            if (this.lvUserInfo.SelectedItems.Count == 0)
            {
                MessageBox.Show("You have to select an User.", "Delete User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Business.User user = new Business.User();
            user.UserId = Convert.ToInt32(this.lvUserInfo.SelectedItems[0].SubItems[0].Text.Trim());
            user.DeleteRecord(user);
            MessageBox.Show("User Information Has Been Delete Successfully!");
            
        }

        private void btnUserList_Click_1(object sender, EventArgs e)
        {
            User user = new User();
            SqlDataReader dr = user.ListRecord();
            this.MyDisplay(dr);
        }

        private void btnUserSearch_Click_1(object sender, EventArgs e)
        {
            int selectOption = metroComboBoxUserSearch.SelectedIndex;
            switch (selectOption)
            {
                case 0:
                    User user = new User();
                    user.UserId = Convert.ToInt32(metroTextBoxUserSearch.Text.Trim());
                    SqlDataReader dr = user.SearchRecord(user,0);
                    this.MyDisplay(dr);
                    dr = user.SearchRecord(user,0);
                    break;
                case 1:
                    user = new User();
                    user.UserName = metroTextBoxUserSearch.Text.Trim();
                    SqlDataReader dr1 = user.SearchRecord(user, 1);
                    this.MyDisplay(dr1);
                    dr = user.SearchRecord(user, 1);
                    break;
                default:
                    break;
            }
           
        }

       

       

        //******************************************************************************************************************//
        //================================================ Employee ======================================================//
        //*****************************************************************************************************************//

        private void metroButtonEmpAdd_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(metroTextBoxEmployeeNumber.Text.Trim());
            Employee emp = new Employee();
            emp.EmployeeNumber = id;
            emp.FirstName = metroTextBoxFirstName.Text.Trim();
            emp.LastName = metroTextBoxLastName.Text.Trim();
            emp.JobTitle = metroTextBoxJobTitle.Text.Trim();
            emp.PhoneNumber = metroTextBoxJobTitle.Text.Trim();
            emp.Email = metroTextBoxJobTitle.Text.Trim();
            emp.SaveEmployee(emp);
            MessageBox.Show("Employee Information Has Been Save Successfully!");
            //if (!validator.IsValidId(textBoxEmployeeNumber, 4))
            //{
            //    return;
            //}
        }

        private void metroButtonEmpUpdate_Click(object sender, EventArgs e)
        {
            Employee emp = new Employee();
            emp.EmployeeNumber = Convert.ToInt32(metroTextBoxEmployeeNumber.Text.Trim());
            emp.FirstName = metroTextBoxFirstName.Text.Trim();
            emp.LastName = metroTextBoxLastName.Text.Trim();
            emp.JobTitle = metroTextBoxJobTitle.Text.Trim();
            emp.PhoneNumber = metroTextBoxEmpPhoneNumber.Text.Trim();
            emp.Email = metroTextBoxEmpEmail.Text.Trim();
            emp.UpdateEmployee(emp);
            MessageBox.Show("Employee Information Has Been Update Successfully!");
        }

        private void metroButtonEmpDelete_Click(object sender, EventArgs e)
        {
           
            if (this.metroListViewEmployee.SelectedItems.Count == 0)
            {
                MessageBox.Show("You have to select an Employee.", "Delete Employee", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Employee emp = new Employee();
            emp.EmployeeNumber = Convert.ToInt32(this.metroListViewEmployee.SelectedItems[0].SubItems[0].Text.Trim());
           emp.DeleteEmployee(emp);
            MessageBox.Show("Employee Information Has Been Delete Successfully!");
            this.metroButtonEmpList.PerformClick();
        }

        private void metroButtonEmpList_Click(object sender, EventArgs e)
        {
            Employee emp = new Employee();
            SqlDataReader dr = emp.ListEmployee();
            this.myDisplay(dr);
        }

        private void metroButtonEmpSearch_Click(object sender, EventArgs e)
        {
            int selectOption = metroComboBoxMISSearch.SelectedIndex;
            switch (selectOption)
            {
                case 0:
                    Employee emp = new Employee();
                    emp.EmployeeNumber = Convert.ToInt32(metroTextBoxSearchID.Text.Trim());
                    SqlDataReader dr = emp.SearchEmployee(emp,0);
                    this.myDisplay(dr);
                    dr = emp.SearchEmployee(emp, 0);
                    break;
                case 1:
                    emp = new Employee();
                    emp.FirstName = metroTextBoxSearchID.Text.Trim();
                    SqlDataReader dr1 = emp.SearchEmployee(emp, 1);
                    this.myDisplay(dr1);
                    dr = emp.SearchEmployee(emp, 1);
                    break;
                default:
                    break;
            }

        }

        private void myDisplay(SqlDataReader dr)
        {
            this.metroListViewEmployee.Items.Clear();
            while (dr.Read())
            {
                string[] row =
                {
                    dr[0].ToString(),
                    dr[1].ToString(),
                    dr[2].ToString(),
                    dr[3].ToString(),
                    dr[4].ToString(),
                    dr[5].ToString()
                };
                ListViewItem lvi = new ListViewItem(row);
                this.metroListViewEmployee.Items.Add(lvi);
            }
            dr.Close();
        }
        private void DisplayListView(List<Employee> list)
        {
            if (list == null || list.Count() <= 0)
            {

                return;
            }

            metroListViewEmployee.Items.Clear();
            foreach (var ee in list)
            {
                ListViewItem item = new ListViewItem();
                item.Text = ee.EmployeeNumber.ToString();
                item.SubItems.Add(ee.FirstName);
                item.SubItems.Add(ee.LastName);
                item.SubItems.Add(ee.JobTitle);
                item.SubItems.Add(ee.PhoneNumber);
                item.SubItems.Add(ee.Email);
                metroListViewEmployee.Items.Add(item);
            }
        }


        //******************************************************************************************************************//
        //================================================ Client ======================================================//
        //*****************************************************************************************************************//

        DataSet dsHiTechDB;
        DataTable dtClients;
        SqlDataAdapter da;
        private void metroButtonClientAdd_Click(object sender, EventArgs e)
        {
            Client aClient = new Client();
            aClient.ClientId = Convert.ToInt32(metroTextBoxClientID.Text.Trim());
            aClient.ClientName = metroTextBoxClientName.Text.Trim();
            aClient.ClientPhoneNumber = metroTextBoxClientPhoneNumber.Text.Trim();
            aClient.ClientEmail = metroTextBoxClientEmail.Text.Trim();
            aClient.ClientFax = metroTextBoxClientFax.Text.Trim();
            aClient.ClientCity = metroTextBoxClientCity.Text.Trim();
            aClient.ClientStreet = metroTextBoxClientStreet.Text.Trim();
            aClient.ClientPostCode= metroTextBoxClientPostCode.Text.Trim();
            aClient.ClientCredit = Convert.ToInt32(metroTextBoxClientCredit.Text.Trim());
          
            DataRow drClient = dtClients.NewRow();
            drClient["ClientId"] = Convert.ToInt32(aClient.ClientId);
            drClient["ClientName"] = aClient.ClientName;
            drClient["ClientPhoneNumber"] = aClient.ClientPhoneNumber;
            drClient["ClientEmail"] = aClient.ClientEmail;
            drClient["ClientFax"] = aClient.ClientFax;
            drClient["ClientCity"] = aClient.ClientCity;
            drClient["ClientStreet"] = aClient.ClientStreet;
            drClient["ClientPostCode"] = aClient.ClientPostCode;
            drClient["ClientCredit"] = aClient.ClientCredit;
            dtClients.Rows.Add(drClient);
            MessageBox.Show(drClient.RowState.ToString());

        }

        private void metroButtonClientList_Click(object sender, EventArgs e)
        {
            dsHiTechDB = new DataSet("HiTechDB");
            dtClients = new DataTable("Clients");
            dtClients.Columns.Add("ClientId", typeof(Int32));
            dtClients.Columns.Add("ClientName", typeof(String));
            dtClients.Columns.Add("ClientPhoneNumber", typeof(String));
            dtClients.Columns.Add("ClientEmail", typeof(String));
            dtClients.Columns.Add("ClientFax", typeof(String));
            dtClients.Columns.Add("ClientCity", typeof(String));
            dtClients.Columns.Add("ClientStreet", typeof(String));
            dtClients.Columns.Add("ClientPostCode", typeof(String));
            dtClients.Columns.Add("ClientCredit", typeof(Int32));
            dtClients.PrimaryKey = new DataColumn[] { dtClients.Columns["ClientId"] };
            dsHiTechDB.Tables.Add(dtClients);
            da = new SqlDataAdapter("SELECT * FROM Clients", UtilityDB.ConnectDB());
            da.Fill(dsHiTechDB.Tables["Clients"]);

            BindingSource ppp = new BindingSource();
            ppp.DataSource = dtClients;
            dataGridViewClients.DataSource = ppp;            
        }

        private void metroButtonClientUpdate_Click(object sender, EventArgs e)
        {
            SqlCommandBuilder sqlBuilder = new SqlCommandBuilder(da);
            da.Update(dsHiTechDB.Tables["Clients"]);
            MessageBox.Show("Data has been updated seccessfully!", "Update DB");
        }

        private void metroButtonClientDelete_Click(object sender, EventArgs e)
        {
            string searchId = metroTextBoxClientSearchID.Text.Trim();
            DataRow drClient = dtClients.Rows.Find(searchId);
            drClient.Delete();
            MessageBox.Show(drClient.RowState.ToString());
        }

        private void metroButtonClientSearch_Click(object sender, EventArgs e)
        {
            int selectedOption = metroComboBoxSaleSearch.SelectedIndex;
            switch (selectedOption)
            {
                case 0:
                    int searchId = Convert.ToInt32(metroTextBoxClientSearchID.Text.Trim());

                    DataRow drClient = dtClients.Rows.Find(searchId);

                    var myrows = dtClients.NewRow();
                    myrows.ItemArray = (object[])drClient.ItemArray.Clone();
                   
                    dtClients.Rows.Clear();

                    dtClients.Rows.Add(myrows);

                    //dataGridViewClients.DataSource = dtClients;
                    return;
                    if (drClient != null)
                    {
                        metroTextBoxClientID.Text = drClient["ClientId"].ToString();
                        metroTextBoxClientName.Text = drClient["ClientName"].ToString();
                        metroTextBoxClientPhoneNumber.Text = drClient["ClientPhoneNumber"].ToString();
                        metroTextBoxClientEmail.Text = drClient["ClientEmail"].ToString();
                        metroTextBoxClientFax.Text = drClient["ClientFax"].ToString();
                        metroTextBoxClientCity.Text = drClient["ClientCity"].ToString();
                        metroTextBoxClientStreet.Text = drClient["ClientStreet"].ToString();
                        metroTextBoxClientPostCode.Text = drClient["ClientPostCode"].ToString();
                        metroTextBoxClientCredit.Text = drClient["ClientCredit"].ToString();
                    }
                    else
                    {
                        MessageBox.Show("Client is not find!", "Try again!");
                    }
                    break;
                case 1:
                    string searchFirstName = metroTextBoxClientSearchID.Text.Trim();
                    DataRow drClient1 = dtClients.Rows.Find(searchFirstName);
                    dataGridViewClients.DataSource = dtClients;
                    if (drClient1 != null)
                    {
                        metroTextBoxClientID.Text = drClient1["ClientId"].ToString();
                        metroTextBoxClientName.Text = drClient1["ClientName"].ToString();
                        metroTextBoxClientPhoneNumber.Text = drClient1["ClientPhoneNumber"].ToString();
                        metroTextBoxClientEmail.Text = drClient1["ClientEmail"].ToString();
                        metroTextBoxClientFax.Text = drClient1["ClientFax"].ToString();
                        metroTextBoxClientCity.Text = drClient1["ClientCity"].ToString();
                        metroTextBoxClientStreet.Text = drClient1["ClientStreet"].ToString();
                        metroTextBoxClientPostCode.Text = drClient1["ClientPostCode"].ToString();
                        metroTextBoxClientCredit.Text = drClient1["ClientCredit"].ToString();
                    }
                    else
                    {
                        MessageBox.Show("Client is not find!", "Try again!");
                    }

                    break;

                default:
                    break;
            }
           

        }


        //******************************************************************************************************************//
        //================================================Order Clerks======================================================//
        //*****************************************************************************************************************//

        HiTechDatabase.Entity.HiTechDBEntities2 dbEntities = new HiTechDatabase.Entity.HiTechDBEntities2();

        private void metroButtonOrderList_Click(object sender, EventArgs e)
        {
            var result = from order in dbEntities.Orders select order;
           
            foreach (var element in result)
            {
                ListViewItem item = new ListViewItem(Convert.ToString(element.OrderId));
                item.SubItems.Add(Convert.ToString(element.ClientId));
                item.SubItems.Add(Convert.ToString(element.ItemId));
                item.SubItems.Add(element.ItemName);
                item.SubItems.Add(Convert.ToString(element.UnitPrice));
                item.SubItems.Add(Convert.ToString(element.Quantity));
                item.SubItems.Add(element.Payment);
                item.SubItems.Add(element.OrderDate);
                item.SubItems.Add(element.ShippingDate);
                item.SubItems.Add(Convert.ToString(element.OrderTotal));
                metroListViewOrder.Items.Add(item);
            }
        
            
        }

        private void metroButtonOrderAdd_Click(object sender, EventArgs e)
        {
            using (HiTechDatabase.Entity.HiTechDBEntities2 dbEntities = new HiTechDatabase.Entity.HiTechDBEntities2())
            {
                HiTechDatabase.Entity.Order order = new HiTechDatabase.Entity.Order();
                order.OrderId = Convert.ToInt32(metroTextBoxOrderId.Text.Trim());
                order.ClientId = Convert.ToInt32(metroTextBoxOderClientID.Text.Trim());
                order.ItemId = Convert.ToInt32(metroTextBoxOderItemId.Text.Trim());
                order.ItemName = metroTextBoxOderItemName.Text.Trim();
                order.UnitPrice = Convert.ToInt32(metroTextBoxOderUnitPrice.Text.Trim());
                order.Quantity = Convert.ToInt32(metroTextBoxOderQuantity.Text.Trim());
                order.Payment = metroTextBoxOdrtPayment.Text.Trim();
                order.OrderDate = metroTextBoxOrderDate.Text.Trim();
                order.ShippingDate = metroTextBoxOrderShippingDate.Text.Trim();
                order.OrderTotal = Convert.ToInt32(metroTextBoxOrderTotal.Text.Trim());
                order.OrderedBy = "";

                dbEntities.Orders.Add(order);
                dbEntities.SaveChanges();

                MessageBox.Show("The new order has been saved successfully.", "Saved!");
            }    
        }

        private void metroButtonOderUpdate_Click(object sender, EventArgs e)
        {
            //Version 1
            int searchId = Convert.ToInt32(metroTextBoxOrderId.Text.Trim());
            HiTechDatabase.Entity.Order updateOrder = dbEntities.Orders.Find(searchId);
            updateOrder.OrderId = Convert.ToInt32(metroTextBoxOrderId.Text.Trim());
            updateOrder.ClientId = Convert.ToInt32(metroTextBoxOderClientID.Text.Trim());
            updateOrder.ItemId = Convert.ToInt32(metroTextBoxOderItemId.Text.Trim());
            updateOrder.ItemName = metroTextBoxOderItemName.Text.Trim();
            updateOrder.UnitPrice = Convert.ToInt32(metroTextBoxOderUnitPrice.Text.Trim());
            updateOrder.Quantity = Convert.ToInt32(metroTextBoxOderQuantity.Text.Trim());
            updateOrder.Payment = metroTextBoxOdrtPayment.Text.Trim();
            updateOrder.OrderDate = metroTextBoxOrderDate.Text.Trim();
            updateOrder.ShippingDate = metroTextBoxOrderShippingDate.Text.Trim();
            updateOrder.OrderTotal = Convert.ToInt32(metroTextBoxOrderTotal.Text.Trim());
            MessageBox.Show("Order updated successfully.", "Confirmation");

            //Version 2
            //HiTechDatabase.Entity.Order order = new HiTechDatabase.Entity.Order()
            //{
            //    OrderId = Convert.ToInt32(metroTextBoxOrderId.Text.Trim()),
            //    ClientId = Convert.ToInt32(metroTextBoxOderClientID.Text.Trim()),
            //    ItemId = Convert.ToInt32(metroTextBoxOderItemId.Text.Trim()),
            //    ItemName = metroTextBoxOderItemName.Text.Trim(),
            //    UnitPrice = Convert.ToInt32(metroTextBoxOderUnitPrice.Text.Trim()),
            //    Quantity = Convert.ToInt32(metroTextBoxOderQuantity.Text.Trim()),
            //    Payment = metroTextBoxOdrtPayment.Text.Trim(),
            //    OrderDate = metroTextBoxOrderDate.Text.Trim(),
            //    ShippingDate = metroTextBoxOrderShippingDate.Text.Trim(),
            //    OrderTotal = Convert.ToInt32(metroTextBoxOrderTotal.Text.Trim())
            //};

            //using (var item = new HiTechDatabase.Entity.HiTechDBEntities2())
            //{
            //    dbEntities.Orders.AddOrUpdate(order);
            //    dbEntities.SaveChanges();

            //}

        }

        private void metroButtonOrderDelete_Click(object sender, EventArgs e)
        {
            var order = new Order { OrderId = Convert.ToInt32(metroTextBoxOrderId.Text.Trim()) };
            using (var item = new HiTechDatabase.Entity.HiTechDBEntities2())
            {
                item.Entry(order).State = System.Data.Entity.EntityState.Deleted;
                item.SaveChanges();
            }
            MessageBox.Show("The new order has been deleted successfully.", "Deleted!");
        }

        private void HiTechSystem_Load(object sender, EventArgs e)
        {

        }

        private void metroButtonOrderSearch_Click(object sender, EventArgs e)
        {
            int selectedOption = metroComboBoxOrderSearch.SelectedIndex;
           
                    int searchId = Convert.ToInt32(metroTextBoxOrderSearchId.Text.Trim());
                    HiTechDatabase.Entity.Order order = dbEntities.Orders.Find(searchId);

                    if (order != null)
                    {
                        metroTextBoxOrderId.Text = order.OrderId.ToString();
                        metroTextBoxOderClientID.Text = order.ClientId.ToString();
                        metroTextBoxOderItemId.Text = order.ItemId.ToString();
                        metroTextBoxOderItemName.Text = order.ItemName;
                        metroTextBoxOderUnitPrice.Text = order.UnitPrice.ToString();
                        metroTextBoxOderQuantity.Text = order.Quantity.ToString();
                        metroTextBoxOdrtPayment.Text = order.Payment;
                        metroTextBoxOrderDate.Text = order.OrderDate.ToString();
                        metroTextBoxOrderShippingDate.Text = order.ShippingDate.ToString();
                        metroTextBoxOrderTotal.Text = order.OrderTotal.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Order not found!", "Invalid Order ID");
                    }
                 
        }



        //******************************************************************************************************************//
        //================================================ Invoice ======================================================//
        //*****************************************************************************************************************//

        private void myDisplayInvoice(SqlDataReader dr)
        {
            this.metroListViewInvoice.Items.Clear();
            while (dr.Read())
            {
                string[] row =
                {
                    dr[0].ToString(),
                    dr[1].ToString(),
                    dr[2].ToString(),
                    dr[3].ToString(),
                    dr[4].ToString()

                };
                ListViewItem lvi = new ListViewItem(row);
                this.metroListViewInvoice.Items.Add(lvi);
            }
            dr.Close();
        }
        private void DisplayListViewInvoice(List<Invoice> list)
        {
            if (list == null || list.Count() <= 0)
            {

                return;
            }

            metroListViewInvoice.Items.Clear();
            foreach (var invoice in list)
            {
                ListViewItem item = new ListViewItem();
                item.Text = invoice.InvoiceNumber.ToString();
                item.SubItems.Add(invoice.OrderId.ToString());
                item.SubItems.Add(invoice.ItemId.ToString());
                item.SubItems.Add(invoice.ClientName);
                item.SubItems.Add(invoice.OrderDate.ToString());
                metroListViewInvoice.Items.Add(item);
            }
        }
        private void metroButtonInvoiceList_Click(object sender, EventArgs e)
        {
            Invoice invoice = new Invoice();
            SqlDataReader dr = invoice.ListRecord();
            this.myDisplayInvoice(dr);
        }

        //private void metroButtonInvoiceAdd_Click(object sender, EventArgs e)
        //{
 
        //    Invoice invoice = new Invoice();
        //    invoice.InvoiceNumber = Convert.ToInt32(metroTextBoxInvoiceNumber.Text.Trim());
        //    invoice.OrderId = Convert.ToInt32(metroTextBoxInvoiceOrderId.Text.Trim());
        //    invoice.ItemId = Convert.ToInt32(metroTextBoxInvoiceItemId.Text.Trim());
        //    invoice.ClientName = metroTextBoxInvoiceClientName.Text.Trim();
        //    invoice.OrderDate = metroTextBoxInvoiceDate.Text.Trim();
        //    invoice.SaveRecord(invoice);
        //    MessageBox.Show("Invoice Information Has Been Save Successfully!");
        //}

        private void metroComboBoxUserSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (metroComboBoxUserSearch.SelectedIndex)
            {
                case 0:
                    metroLabelInput.Text = "Please enter Employee ID.";
                    metroTextBoxUserSearch.Clear();
                    metroTextBoxUserSearch.Focus();
                    break;
                case 1:
                    metroLabelInput.Text = "Please enter Employee first name.";
                    metroTextBoxUserSearch.Clear();
                    metroTextBoxUserSearch.Focus();
                    break;

                default:
                    break;
            }
        }

        private void metroButtonAppExit_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you want to exit the application?", "Exit?", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                MessageBox.Show("Logout successfully.", "Confirmation");
                LoginForm loginForm = new LoginForm();
                loginForm.Show();
               // Application.Exit();
            }
        }

        private void metroComboBoxMISSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (metroComboBoxMISSearch.SelectedIndex)
            {
                case 0:
                    metroLabelMISInput.Text = "Please enter Employee ID.";
                    metroTextBoxSearchID.Clear();
                    metroTextBoxSearchID.Focus();
                    break;
                case 1:
                    metroLabelMISInput.Text = "Please enter Employee First Name.";
                    metroTextBoxSearchID.Clear();
                    metroTextBoxSearchID.Focus();
                    break;

                default:
                    break;
            }
        }

        private void metroComboBoxSaleSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            switch (metroComboBoxSaleSearch.SelectedIndex)
            {
                case 0:
                    metroLabelSaleInput.Text = "Please enter Client ID.";
                    metroTextBoxClientSearchID.Clear();
                    metroTextBoxClientSearchID.Focus();
                    break;
                case 1:
                    metroLabelSaleInput.Text = "Please enter Employee Client Name.";
                    metroTextBoxClientSearchID.Clear();
                    metroTextBoxClientSearchID.Focus();
                    break;

                default:
                    break;
            }
        }

        //private void metroButtonInvoiceUpdate_Click(object sender, EventArgs e)
        //{
        //    Invoice invoice = new Invoice();
        //    invoice.InvoiceNumber = Convert.ToInt32(metroTextBoxInvoiceNumber.Text.Trim());
        //    invoice.OrderId = Convert.ToInt32(metroTextBoxInvoiceOrderId.Text.Trim());
        //    invoice.ItemId = Convert.ToInt32(metroTextBoxInvoiceItemId.Text.Trim());
        //    invoice.ClientName = metroTextBoxInvoiceClientName.Text.Trim();
        //    invoice.OrderDate = metroTextBoxInvoiceDate.Text.Trim();
        //    invoice.UpdateRecord(invoice);
        //    MessageBox.Show("Invoice Information Has Been Save Successfully!");
        //}

        private void metroButtonInvoiceSearch_Click(object sender, EventArgs e)
        {
            int selectOption = metroComboBoxInvoiceSearch.SelectedIndex;
            switch (selectOption)
            {
                case 0:
                    Invoice invoice = new Invoice();
                    invoice.InvoiceNumber = Convert.ToInt32(metroTextBoxInvoiceSearchId.Text.Trim());
                    SqlDataReader dr = invoice.SearchRecord(invoice, 0);
                    this.myDisplayInvoice(dr);
                    dr = invoice.SearchRecord(invoice, 0);
                    break;

                case 1:
                    invoice = new Invoice();
                    invoice.ClientName = metroTextBoxInvoiceSearchId.Text.Trim();
                    SqlDataReader dr1 = invoice.SearchRecord(invoice, 1);
                    this.myDisplayInvoice(dr1);
                    dr = invoice.SearchRecord(invoice, 1);
                    break;
                default:
                    break;
            }
        }

        //private void metroButtonInvoiceDelete_Click(object sender, EventArgs e)
        //{
        //    Invoice invoice = new Invoice();
        //    invoice.InvoiceNumber = Convert.ToInt32(this.metroListViewInvoice.SelectedItems[0].SubItems[0].Text.Trim());
        //    invoice.DeleteRecord(invoice);
        //    MessageBox.Show("Invoice Information Has Been Delete Successfully!");
        //}

        private void metroComboBoxInvoiceSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (metroComboBoxInvoiceSearch.SelectedIndex)
            {
                case 0:
                    metroLabelInvoiceInput.Text = "Please enter Invoice Number.";
                    metroTextBoxInvoiceSearchId.Clear();
                    metroTextBoxInvoiceSearchId.Focus();
                    break;
                case 1:
                    metroLabelInvoiceInput.Text = "Please enter Employee Item Name.";
                    metroTextBoxInvoiceSearchId.Clear();
                    metroTextBoxInvoiceSearchId.Focus();
                    break;
                default:
                    break;
            }
        }


        //******************************************************************************************************************//
        //================================================ Author ======================================================//
        //*****************************************************************************************************************//

        private void MyDisplayAuthor(SqlDataReader dr)
        {
            this.metroListViewAuthor.Items.Clear();
            while (dr.Read())
            {
                string[] row =
                {
                    dr[0].ToString(),
                    dr[1].ToString(),
                    dr[2].ToString(),
                    dr[3].ToString(),
                    dr[4].ToString(),
                    dr[5].ToString()

            };
                ListViewItem lvi = new ListViewItem(row);
                this.metroListViewAuthor.Items.Add(lvi);
            }
            //dr.Close();

            this.metroListViewAuthor.Focus();
            if (this.metroListViewAuthor.Items.Count > 0) this.metroListViewAuthor.Items[0].Selected = true;

        }

        private void MyDisplayListviewAuthor(List<Business.Author> list)
        {
            if (list == null || list.Count() <= 0)
            {
                return;
            }
            metroListViewAuthor.Items.Clear();
            foreach (var author in list)
            {
                ListViewItem item = new ListViewItem();
                item.Text = author.AuthorId.ToString();
                item.SubItems.Add(author.PublisherId.ToString());
                item.SubItems.Add(author.BookISBN.ToString());
                item.SubItems.Add(author.AuthorFirstName);
                item.SubItems.Add(author.AuthorLastName);
                item.SubItems.Add(author.AuthorEmail);
                metroListViewAuthor.Items.Add(item);

            }
        }

        private void metroButtonAuthorList_Click(object sender, EventArgs e)
        {
            Author author = new Author();
            SqlDataReader dr = author.ListRecord();
            this.MyDisplayAuthor(dr);
        }

        private void metroButtonAuthorAdd_Click(object sender, EventArgs e)
        {
            Author author = new Author();
            author.AuthorId = Convert.ToInt32(metroTextBoxAuthorId.Text.Trim());
            author.PublisherId = Convert.ToInt32(metroTextBoxAuthorPublisherId.Text.Trim());
            author.BookISBN = Convert.ToInt32(metroTextBoxAuthorBookISBN.Text.Trim());
            author.AuthorFirstName = metroTextBoxAuthorFN.Text.Trim();
            author.AuthorLastName = metroTextBoxAuthorLN.Text.Trim();
            author.AuthorEmail = metroTextBoxAuthorEmail.Text.Trim();
            author.SaveRecord(author);
            MessageBox.Show("Authorr Information Has Been Save Successfully!");
        }

        private void metroButtonAuthorUpdate_Click(object sender, EventArgs e)
        {
            Author author = new Author();
            author.AuthorId = Convert.ToInt32(metroTextBoxAuthorId.Text.Trim());
            author.PublisherId = Convert.ToInt32(metroTextBoxAuthorPublisherId.Text.Trim());
            author.BookISBN = Convert.ToInt32(metroTextBoxAuthorBookISBN.Text.Trim());
            author.AuthorFirstName = metroTextBoxAuthorFN.Text.Trim();
            author.AuthorLastName = metroTextBoxAuthorLN.Text.Trim();
            author.AuthorEmail = metroTextBoxAuthorEmail.Text.Trim();
            author.UpdateRecord(author);
            MessageBox.Show("Author Information Has Been Update Successfully!");
        }

        private void metroButtonAuthorDelete_Click(object sender, EventArgs e)
        {
            if (this.metroListViewAuthor.SelectedItems.Count == 0)
            {
                MessageBox.Show("You have to select an Author.", "Delete Author", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Business.Author author = new Author();
            author.AuthorId = Convert.ToInt32(this.metroListViewAuthor.SelectedItems[0].SubItems[0].Text.Trim());
            author.DeleteRecord(author);
            MessageBox.Show("Author Information Has Been Delete Successfully!");
        }

        private void metroComboBoxAuthorSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (metroComboBoxAuthorSearch.SelectedIndex)
            {
                case 0:
                    metroLabelAuthorInput.Text = "Please enter Author ID.";
                    metroTextBoxAuthorSearchId.Clear();
                    metroTextBoxAuthorSearchId.Focus();
                    break;
                case 1:
                    metroLabelAuthorInput.Text = "Please enter Author First Name.";
                    metroTextBoxAuthorSearchId.Clear();
                    metroTextBoxAuthorSearchId.Focus();
                    break;

                default:
                    break;
            }
        }

        private void metroButtonAuthorSearch_Click(object sender, EventArgs e)
        {
            int selectOption = metroComboBoxAuthorSearch.SelectedIndex;
            switch (selectOption)
            {
                case 0:
                    Author author = new Author();
                    author.AuthorId = Convert.ToInt32(metroTextBoxAuthorSearchId.Text.Trim());
                    SqlDataReader dr = author.SearchRecord(author, 0);
                    this.MyDisplayAuthor(dr);
                    dr = author.SearchRecord(author, 0);
                    break;
                case 1:
                    author = new Author();
                    author.AuthorFirstName = metroTextBoxAuthorSearchId.Text.Trim();
                    SqlDataReader dr1 = author.SearchRecord(author, 1);
                    this.MyDisplayAuthor(dr1);
                    dr = author.SearchRecord(author, 1);
                    break;
                default:
                    break;
            }
        }

        //******************************************************************************************************************//
        //================================================ Book ======================================================//
        //*****************************************************************************************************************//

        private void MyDisplayBook(SqlDataReader dr)
        {
            this.metroListViewBook.Items.Clear();
            while (dr.Read())
            {
                string[] row =
                {
                    dr[0].ToString(),
                    dr[1].ToString(),
                    dr[2].ToString(),
                    dr[3].ToString(),
                    dr[4].ToString(),
                    dr[5].ToString(),
                    dr[6].ToString(),
                    dr[7].ToString()
            };
                ListViewItem lvi = new ListViewItem(row);
                this.metroListViewBook.Items.Add(lvi);
            }
            //dr.Close();

            this.metroListViewBook.Focus();
            if (this.metroListViewBook.Items.Count > 0) this.metroListViewBook.Items[0].Selected = true;

        }

        private void MyDisplayListviewBook(List<Business.Book> list)
        {
            if (list == null || list.Count() <= 0)
            {
                return;
            }
            metroListViewBook.Items.Clear();
            foreach (var book in list)
            {
                ListViewItem item = new ListViewItem();
                item.Text = book.BookISBN.ToString();
                item.SubItems.Add(book.PublisherId.ToString());
                item.SubItems.Add(book.CategoryId.ToString());
                item.SubItems.Add(book.BookTitle);
                item.SubItems.Add(book.BookPrice.ToString());
                item.SubItems.Add(book.BookQOH.ToString());
                item.SubItems.Add(book.BookOrderQuantity.ToString());
                item.SubItems.Add(book.BookPublishedDate);
                metroListViewBook.Items.Add(item);

            }
        }

        private void metroButtonBookList_Click(object sender, EventArgs e)
        {
            Book book = new Book();
            SqlDataReader dr = book.ListRecord();
            this.MyDisplayBook(dr);
        }

        private void metroButtonBookAdd_Click(object sender, EventArgs e)
        {
            Book book = new Book();
            book.BookISBN = Convert.ToInt32(metroTextBoxBookISBN.Text.Trim());
            book.PublisherId= Convert.ToInt32( metroTextBoxBookPublisherId.Text.Trim());
            book.CategoryId= Convert.ToInt32(metroTextBoxBookCategoryId.Text.Trim());
            book.BookTitle= metroTextBoxBookBookTitle.Text.Trim();
            book.BookPrice= Convert.ToInt32(metroTextBoxBookPrice.Text.Trim());
            book.BookQOH= Convert.ToInt32(metroTextBoxBookQOH.Text.Trim());
            book.BookOrderQuantity= Convert.ToInt32(metroTextBoxBookOrderQuantity.Text.Trim());
            book.BookPublishedDate= metroTextBoxBookPublishedDate.Text.Trim();
            book.SaveRecord(book);
            MessageBox.Show("Book Information Has Been Save Successfully!");
           
        }

        private void metroButtonBookUpdate_Click(object sender, EventArgs e)
        {
            Book book = new Book();
            book.BookISBN = Convert.ToInt32(metroTextBoxBookISBN.Text.Trim());
            book.PublisherId = Convert.ToInt32(metroTextBoxBookPublisherId.Text.Trim());
            book.CategoryId = Convert.ToInt32(metroTextBoxBookCategoryId.Text.Trim());
            book.BookTitle = metroTextBoxBookBookTitle.Text.Trim();
            book.BookPrice = Convert.ToInt32(metroTextBoxBookPrice.Text.Trim());
            book.BookQOH = Convert.ToInt32(metroTextBoxBookQOH.Text.Trim());
            book.BookOrderQuantity = Convert.ToInt32(metroTextBoxBookOrderQuantity.Text.Trim());
            book.BookPublishedDate = metroTextBoxBookPublishedDate.Text.Trim();
            book.UpdateRecord(book);
            MessageBox.Show("Book Information Has Been Update Successfully!");


        }

        private void metroButtonBookDelete_Click(object sender, EventArgs e)
        {
            if (this.metroListViewBook.SelectedItems.Count == 0)
            {
                MessageBox.Show("You have to select a Book.", "Delete Book", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Business.Book book = new Book();
            book.BookISBN = Convert.ToInt32(this.metroListViewBook.SelectedItems[0].SubItems[0].Text.Trim());
            book.DeleteRecord(book);
            MessageBox.Show("Book Information Has Been Delete Successfully!");
         
        }

        private void metroComboBoxBookSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (metroComboBoxBookSearch.SelectedIndex)
            {
                case 0:
                    metroLabelBookInput.Text = "Please enter Book ISBN.";
                    metroTextBoxBookSearchId.Clear();
                    metroTextBoxBookSearchId.Focus();
                    break;
                case 1:
                    metroLabelBookInput.Text = "Please enter Book Title.";
                    metroTextBoxBookSearchId.Clear();
                    metroTextBoxBookSearchId.Focus();
                    break;

                default:
                    break;
            }
        }

        private void metroButtonBookSearch_Click(object sender, EventArgs e)
        {
            int selectOption = metroComboBoxBookSearch.SelectedIndex;
            switch (selectOption)
            {
                case 0:
                    Book book = new Book();
                    book.BookISBN = Convert.ToInt32(metroTextBoxBookSearchId.Text.Trim());
                    SqlDataReader dr = book.SearchRecord(book, 0);
                    this.MyDisplayBook(dr);
                    dr = book.SearchRecord(book, 0);
                    break;
                case 1:
                    book = new Book();
                    book.BookTitle = metroTextBoxBookSearchId.Text.Trim();
                    SqlDataReader dr1 = book.SearchRecord(book, 1);
                    this.MyDisplayBook(dr1);
                    dr = book.SearchRecord(book, 1);
                    break;
                default:
                    break;
            }
        }

        //******************************************************************************************************************//
        //================================================ Book ======================================================//
        //*****************************************************************************************************************//
        private void MyDisplayCategory(SqlDataReader dr)
        {
            this.metroListViewCategory.Items.Clear();
            while (dr.Read())
            {
                string[] row =
                {
                    dr[0].ToString(),
                    dr[1].ToString()
            };
                ListViewItem lvi = new ListViewItem(row);
                this.metroListViewCategory.Items.Add(lvi);
            }
            //dr.Close();

            this.metroListViewCategory.Focus();
            if (this.metroListViewCategory.Items.Count > 0) this.metroListViewCategory.Items[0].Selected = true;

        }

        private void MyDisplayListviewCategory(List<Business.Category> list)
        {
            if (list == null || list.Count() <= 0)
            {
                return;
            }
            metroListViewCategory.Items.Clear();
            foreach (var category in list)
            {
                ListViewItem item = new ListViewItem();
                item.Text = category.CategoryId.ToString();
                item.SubItems.Add(category.CategoryName);
                metroListViewCategory.Items.Add(item);

            }
        }

        private void metroButtonCategoryList_Click(object sender, EventArgs e)
        {
            Category category = new Category();
            SqlDataReader dr = category.ListRecord();
            this.MyDisplayCategory(dr);
        }

        private void metroButtonCategoryAdd_Click(object sender, EventArgs e)
        {
            Category category = new Category();
            category.CategoryId= Convert.ToInt32(metroTextBoxCategoryId.Text.Trim());
            category.CategoryName= metroTextBoxCategoryName.Text.Trim();
            category.SaveRecord(category);
            MessageBox.Show("Category Information Has Been Save Successfully!");
        }

        private void metroButtonCategoryUpdate_Click(object sender, EventArgs e)
        {
            Category category = new Category();
            category.CategoryId = Convert.ToInt32(metroTextBoxCategoryId.Text.Trim());
            category.CategoryName = metroTextBoxCategoryName.Text.Trim();
            category.UpdateRecord(category);
            MessageBox.Show("Category Information Has Been Update Successfully!");
        }

        private void metroButtonCategoryDelete_Click(object sender, EventArgs e)
        {
            if (this.metroListViewCategory.SelectedItems.Count == 0)
            {
                MessageBox.Show("You have to select a Category.", "Delete Category", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Business.Category category = new Category();
            category.CategoryId = Convert.ToInt32(this.metroListViewCategory.SelectedItems[0].SubItems[0].Text.Trim());
            category.DeleteRecord(category);
            MessageBox.Show("Category Information Has Been Delete Successfully!");

        }

        private void metroComboBoxCategorySearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (metroComboBoxCategorySearch.SelectedIndex)
            {
                case 0:
                    metroLabelCategoryInput.Text = "Please enter Category ID.";
                    metroTextBoxCategorySearchId.Clear();
                    metroTextBoxCategorySearchId.Focus();
                    break;
                case 1:
                    metroLabelCategoryInput.Text = "Please enter Category Name.";
                    metroTextBoxCategorySearchId.Clear();
                    metroTextBoxCategorySearchId.Focus();
                    break;

                default:
                    break;
            }
        }

        private void metroButtonCategory_Click(object sender, EventArgs e)
        {
            int selectOption = metroComboBoxCategorySearch.SelectedIndex;
            switch (selectOption)
            {
                case 0:
                    Category category = new Category();
                    category.CategoryId = Convert.ToInt32(metroTextBoxCategorySearchId.Text.Trim());
                    SqlDataReader dr = category.SearchRecord(category, 0);
                    this.MyDisplayCategory(dr);
                    dr = category.SearchRecord(category, 0);
                    break;
                case 1:
                    category = new Category();
                    category.CategoryName =metroTextBoxCategorySearchId.Text.Trim();
                    SqlDataReader dr1 = category.SearchRecord(category, 1);
                    this.MyDisplayCategory(dr1);
                    dr = category.SearchRecord(category, 1);
                    break;
                default:
                    break;
            }
        }

        private void metroComboBoxOrderSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            metroLabelOrderInput.Text = "Please enter Order ID.";
            metroTextBoxOrderSearchId.Clear();
            metroTextBoxOrderSearchId.Focus();
        }
    }
}
