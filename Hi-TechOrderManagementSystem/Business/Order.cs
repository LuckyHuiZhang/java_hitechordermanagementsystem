﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiTechOrderManagementSystem.Business
{
    public class Order
    {
        private int orderId;
        private int clientId;
        private int itemId;
        private string itemName;
        private int unitPrice;
        private int quantity;
        private string payment;
        private string orderDate;
        private string shippingDate;
        private int orderTotal;

        public int OrderId { get => orderId; set => orderId = value; }
        public int ClientId { get => clientId; set => clientId = value; }
        public int ItemId { get => itemId; set => itemId = value; }
        public string ItemName { get => itemName; set => itemName = value; }
        public int UnitPrice { get => unitPrice; set => unitPrice = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public string Payment { get => payment; set => payment = value; }
        public string OrderDate { get => orderDate; set => orderDate = value; }
        public string ShippingDate { get => shippingDate; set => shippingDate = value; }
        public int OrderTotal { get => orderTotal; set => orderTotal = value; }
    }
}
