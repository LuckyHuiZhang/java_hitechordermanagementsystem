USE [master]
GO
/****** Object:  Database [HiTechDB]    Script Date: 2019/4/14 19:48:58 ******/
CREATE DATABASE [HiTechDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'HiTechDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\HiTechDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'HiTechDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\HiTechDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [HiTechDB] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HiTechDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [HiTechDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [HiTechDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [HiTechDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [HiTechDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [HiTechDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [HiTechDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [HiTechDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [HiTechDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [HiTechDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [HiTechDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [HiTechDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [HiTechDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [HiTechDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [HiTechDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [HiTechDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [HiTechDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [HiTechDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [HiTechDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [HiTechDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [HiTechDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [HiTechDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [HiTechDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [HiTechDB] SET RECOVERY FULL 
GO
ALTER DATABASE [HiTechDB] SET  MULTI_USER 
GO
ALTER DATABASE [HiTechDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [HiTechDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [HiTechDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [HiTechDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [HiTechDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [HiTechDB] SET QUERY_STORE = OFF
GO
USE [HiTechDB]
GO
/****** Object:  Table [dbo].[Authors]    Script Date: 2019/4/14 19:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authors](
	[AuthorId] [int] NOT NULL,
	[PublisherId] [int] NOT NULL,
	[BookISBN] [nvarchar](50) NOT NULL,
	[AuthorFirstName] [nvarchar](50) NOT NULL,
	[AuthorLastName] [nvarchar](50) NOT NULL,
	[AuthorEmail] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Authors] PRIMARY KEY CLUSTERED 
(
	[AuthorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Books]    Script Date: 2019/4/14 19:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Books](
	[BookISBN] [nvarchar](50) NOT NULL,
	[PublisherId] [int] NULL,
	[CategoryId] [int] NOT NULL,
	[BookTitle] [nvarchar](50) NOT NULL,
	[BookPrice] [int] NOT NULL,
	[BookQOH] [int] NOT NULL,
	[BookOrderQuantity] [int] NOT NULL,
	[BookPublishedDate] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Books] PRIMARY KEY CLUSTERED 
(
	[BookISBN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categorys]    Script Date: 2019/4/14 19:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categorys](
	[CategoryId] [int] NOT NULL,
	[CategoryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Categorys] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 2019/4/14 19:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[ClientId] [int] NOT NULL,
	[ClientName] [nvarchar](50) NOT NULL,
	[ClientPhoneNumber] [nvarchar](50) NOT NULL,
	[ClientEmail] [nvarchar](50) NOT NULL,
	[ClientFax] [nvarchar](50) NOT NULL,
	[ClientCity] [nvarchar](100) NOT NULL,
	[ClientStreet] [nvarchar](100) NOT NULL,
	[ClientPostCode] [nvarchar](50) NOT NULL,
	[ClientCredit] [int] NOT NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 2019/4/14 19:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[EmployeeNumber] [int] NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[JobTitle] [nvarchar](50) NOT NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[EmployeeNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 2019/4/14 19:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[InvoiceNumber] [int] NOT NULL,
	[OrderId] [int] NOT NULL,
	[ItemId] [int] NOT NULL,
	[ClientName] [nvarchar](50) NOT NULL,
	[OrderDate] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED 
(
	[InvoiceNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ItemTypes]    Script Date: 2019/4/14 19:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemTypes](
	[ItemId] [int] NOT NULL,
	[ItemName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ItemTypes] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 2019/4/14 19:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderId] [int] NOT NULL,
	[ClientId] [int] NOT NULL,
	[ItemId] [int] NOT NULL,
	[ItemName] [nvarchar](50) NOT NULL,
	[UnitPrice] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Payment] [nvarchar](50) NOT NULL,
	[OrderedBy] [nvarchar](50) NOT NULL,
	[OrderDate] [nvarchar](50) NOT NULL,
	[ShippingDate] [nvarchar](50) NOT NULL,
	[OrderTotal] [int] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Publishers]    Script Date: 2019/4/14 19:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Publishers](
	[PublisherId] [int] NOT NULL,
	[PublisherName] [nvarchar](50) NOT NULL,
	[PublisherEmail] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Publishers] PRIMARY KEY CLUSTERED 
(
	[PublisherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 2019/4/14 19:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] NOT NULL,
	[EmployeeNumber] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Authors] ([AuthorId], [PublisherId], [BookISBN], [AuthorFirstName], [AuthorLastName], [AuthorEmail]) VALUES (1001, 8001, N'1524763136', N'Michelle ', N'Obama', N'michelle@gmail.com')
INSERT [dbo].[Authors] ([AuthorId], [PublisherId], [BookISBN], [AuthorFirstName], [AuthorLastName], [AuthorEmail]) VALUES (1002, 8002, N'0345535529', N'George', N'R.R Martin', N'george@gmail.com')
INSERT [dbo].[Authors] ([AuthorId], [PublisherId], [BookISBN], [AuthorFirstName], [AuthorLastName], [AuthorEmail]) VALUES (1003, 8003, N'0756668611', N'D', N'K', N'dk@gmail.com')
INSERT [dbo].[Authors] ([AuthorId], [PublisherId], [BookISBN], [AuthorFirstName], [AuthorLastName], [AuthorEmail]) VALUES (1004, 8002, N'0345535529', N't', N't', N't')
INSERT [dbo].[Books] ([BookISBN], [PublisherId], [CategoryId], [BookTitle], [BookPrice], [BookQOH], [BookOrderQuantity], [BookPublishedDate]) VALUES (N'0345535529', NULL, 2001, N'Game of Thrones', 38, 25000, 800, N'2013/10/29')
INSERT [dbo].[Books] ([BookISBN], [PublisherId], [CategoryId], [BookTitle], [BookPrice], [BookQOH], [BookOrderQuantity], [BookPublishedDate]) VALUES (N'0756668611', NULL, 2002, N'The Philosophy', 28, 5000, 60, N'2011/01/17')
INSERT [dbo].[Books] ([BookISBN], [PublisherId], [CategoryId], [BookTitle], [BookPrice], [BookQOH], [BookOrderQuantity], [BookPublishedDate]) VALUES (N'1524763136', NULL, 2001, N'Becoming', 29, 10000, 50, N'2018/11/13')
INSERT [dbo].[Categorys] ([CategoryId], [CategoryName]) VALUES (2001, N'Fantasy')
INSERT [dbo].[Categorys] ([CategoryId], [CategoryName]) VALUES (2002, N'Seience Fiction')
INSERT [dbo].[Categorys] ([CategoryId], [CategoryName]) VALUES (2003, N'Westerns')
INSERT [dbo].[Categorys] ([CategoryId], [CategoryName]) VALUES (2004, N'Romance')
INSERT [dbo].[Categorys] ([CategoryId], [CategoryName]) VALUES (2005, N'Thriller')
INSERT [dbo].[Categorys] ([CategoryId], [CategoryName]) VALUES (2006, N'Mystery')
INSERT [dbo].[Categorys] ([CategoryId], [CategoryName]) VALUES (2007, N'Detective')
INSERT [dbo].[Categorys] ([CategoryId], [CategoryName]) VALUES (2008, N'Dystopia')
INSERT [dbo].[Clients] ([ClientId], [ClientName], [ClientPhoneNumber], [ClientEmail], [ClientFax], [ClientCity], [ClientStreet], [ClientPostCode], [ClientCredit]) VALUES (3001, N'LaSalle', N'4389999999', N'lasalle@gmail.com', N'5145678765', N'MontrEal', N'Saint Catherine', N'H3H2T3', 600)
INSERT [dbo].[Clients] ([ClientId], [ClientName], [ClientPhoneNumber], [ClientEmail], [ClientFax], [ClientCity], [ClientStreet], [ClientPostCode], [ClientCredit]) VALUES (3002, N'Concordia University', N'4388888888', N'concordia@gmail.com', N'5145678453', N'Montreal', N'Boulevard de Maisonneuve', N'H3G1M8', 800)
INSERT [dbo].[Clients] ([ClientId], [ClientName], [ClientPhoneNumber], [ClientEmail], [ClientFax], [ClientCity], [ClientStreet], [ClientPostCode], [ClientCredit]) VALUES (3003, N'McGill', N'4387777777', N'mcgill@gmail.com', N'5147687545', N'Montreal', N'Sherbrooke', N'H3A0G4', 1000)
INSERT [dbo].[Clients] ([ClientId], [ClientName], [ClientPhoneNumber], [ClientEmail], [ClientFax], [ClientCity], [ClientStreet], [ClientPostCode], [ClientCredit]) VALUES (3004, N'Quebec', N'4387777777', N'mcgill@gmail.com', N'5147687545', N'Montreal', N'Sherbrooke', N'H3A0G4', 1000)
INSERT [dbo].[Employees] ([EmployeeNumber], [FirstName], [LastName], [JobTitle], [PhoneNumber], [Email]) VALUES (1111, N'lucky ', N'zhang', N'manager', N'4389997777', N'lucky@gmail.com')
INSERT [dbo].[Employees] ([EmployeeNumber], [FirstName], [LastName], [JobTitle], [PhoneNumber], [Email]) VALUES (4001, N'Henry', N'Brown', N'MIS Manager', N'4386576920', N'henry@gmail.com')
INSERT [dbo].[Employees] ([EmployeeNumber], [FirstName], [LastName], [JobTitle], [PhoneNumber], [Email]) VALUES (4002, N'Thomas', N'Moore', N'Sales Manager', N'4389034534', N'thomas@gmail.com')
INSERT [dbo].[Employees] ([EmployeeNumber], [FirstName], [LastName], [JobTitle], [PhoneNumber], [Email]) VALUES (4003, N'Mary', N'Brown', N'Order Clerks', N'4381267803', N'mary@gmail.com')
INSERT [dbo].[Employees] ([EmployeeNumber], [FirstName], [LastName], [JobTitle], [PhoneNumber], [Email]) VALUES (4004, N'Jennifer', N'Bouchard', N'Order Clerks', N'4382309345', N'jennifer@gmail.com')
INSERT [dbo].[Employees] ([EmployeeNumber], [FirstName], [LastName], [JobTitle], [PhoneNumber], [Email]) VALUES (4005, N'Kim', N'Hoa Nguyen', N'Accountant', N'4382309567', N'kim@gmail.com')
INSERT [dbo].[Employees] ([EmployeeNumber], [FirstName], [LastName], [JobTitle], [PhoneNumber], [Email]) VALUES (4006, N'Peter', N'Wang', N'Inventory Controller', N'4380979340', N'peter@gmail.com')
INSERT [dbo].[Invoices] ([InvoiceNumber], [OrderId], [ItemId], [ClientName], [OrderDate]) VALUES (5001, 7001, 6001, N'Lasalle', N'Jan  1 2018 12:00AM')
INSERT [dbo].[Invoices] ([InvoiceNumber], [OrderId], [ItemId], [ClientName], [OrderDate]) VALUES (5002, 7002, 6002, N'MaGill', N'Jan  9 2018 12:00AM')
INSERT [dbo].[ItemTypes] ([ItemId], [ItemName]) VALUES (6001, N'Book')
INSERT [dbo].[ItemTypes] ([ItemId], [ItemName]) VALUES (6002, N'Magazine')
INSERT [dbo].[ItemTypes] ([ItemId], [ItemName]) VALUES (6003, N'Album')
INSERT [dbo].[Orders] ([OrderId], [ClientId], [ItemId], [ItemName], [UnitPrice], [Quantity], [Payment], [OrderedBy], [OrderDate], [ShippingDate], [OrderTotal]) VALUES (7001, 3001, 6001, N'Book', 35, 60, N'Credit', N'Email', N'2018/02/09', N'2018/09/08', 2100)
INSERT [dbo].[Orders] ([OrderId], [ClientId], [ItemId], [ItemName], [UnitPrice], [Quantity], [Payment], [OrderedBy], [OrderDate], [ShippingDate], [OrderTotal]) VALUES (7002, 3001, 6002, N'Book', 38, 80, N'Cridet', N'Fax', N'2019/01/01', N'2019/04/04', 3040)
INSERT [dbo].[Publishers] ([PublisherId], [PublisherName], [PublisherEmail]) VALUES (8001, N'Crown', N'Crown@gmail.com')
INSERT [dbo].[Publishers] ([PublisherId], [PublisherName], [PublisherEmail]) VALUES (8002, N'Bantam', N'Bantam@gmail.com')
INSERT [dbo].[Publishers] ([PublisherId], [PublisherName], [PublisherEmail]) VALUES (8003, N'Wrox', N'DK@gmail.com')
INSERT [dbo].[Users] ([UserId], [EmployeeNumber], [UserName], [Password], [Email]) VALUES (9001, 1111, N'lucky', N'lucky123', N'lucky@gmail.com')
INSERT [dbo].[Users] ([UserId], [EmployeeNumber], [UserName], [Password], [Email]) VALUES (9002, 4005, N't', N't', N't')
INSERT [dbo].[Users] ([UserId], [EmployeeNumber], [UserName], [Password], [Email]) VALUES (9006, 4003, N'k', N'c', N'c')
ALTER TABLE [dbo].[Authors]  WITH CHECK ADD  CONSTRAINT [FK_Authors_Books] FOREIGN KEY([BookISBN])
REFERENCES [dbo].[Books] ([BookISBN])
GO
ALTER TABLE [dbo].[Authors] CHECK CONSTRAINT [FK_Authors_Books]
GO
ALTER TABLE [dbo].[Authors]  WITH CHECK ADD  CONSTRAINT [FK_Authors_Publishers] FOREIGN KEY([PublisherId])
REFERENCES [dbo].[Publishers] ([PublisherId])
GO
ALTER TABLE [dbo].[Authors] CHECK CONSTRAINT [FK_Authors_Publishers]
GO
ALTER TABLE [dbo].[Books]  WITH CHECK ADD  CONSTRAINT [FK_Books_Categorys] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categorys] ([CategoryId])
GO
ALTER TABLE [dbo].[Books] CHECK CONSTRAINT [FK_Books_Categorys]
GO
ALTER TABLE [dbo].[Books]  WITH CHECK ADD  CONSTRAINT [FK_Books_Publishers] FOREIGN KEY([PublisherId])
REFERENCES [dbo].[Publishers] ([PublisherId])
GO
ALTER TABLE [dbo].[Books] CHECK CONSTRAINT [FK_Books_Publishers]
GO
ALTER TABLE [dbo].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_Invoices_ItemTypes] FOREIGN KEY([ItemId])
REFERENCES [dbo].[ItemTypes] ([ItemId])
GO
ALTER TABLE [dbo].[Invoices] CHECK CONSTRAINT [FK_Invoices_ItemTypes]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Clients1] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([ClientId])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Clients1]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_ItemTypes1] FOREIGN KEY([ItemId])
REFERENCES [dbo].[ItemTypes] ([ItemId])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_ItemTypes1]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Employees] FOREIGN KEY([EmployeeNumber])
REFERENCES [dbo].[Employees] ([EmployeeNumber])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Employees]
GO
USE [master]
GO
ALTER DATABASE [HiTechDB] SET  READ_WRITE 
GO
