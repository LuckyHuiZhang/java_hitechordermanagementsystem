﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiTechOrderManagementSystem.Business;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace HiTechOrderManagementSystem.DataAccess
{
    public static class EmployeeDB
    {
        public static void SaveRecord(Employee emp)
        {
            
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "INSERT INTO Employees " +
                              " VALUES(" + emp.EmployeeNumber + ", '"
                                         + emp.FirstName + "','" +
                                         emp.LastName + "','" +
                                         emp.JobTitle + "','" +
                                         emp.PhoneNumber + "','" +
                                        emp.Email + "')";

            cmd.ExecuteNonQuery();
            connDB.Close();
        }

        public static void UpdateRecord(Employee emp)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "UPDATE Employees " +
                               " SET EmployeeNumber = " + emp.EmployeeNumber +
                               ",FirstName= '" + emp.FirstName + "'" +
                               ",LastName ='" + emp.LastName + "'" +
                               ",JobTitle = '" + emp.JobTitle + "'" +
                                ",PhoneNumber= '" + emp.PhoneNumber + "'" +
                                 ",Email = '" + emp.Email + "'" +
                               " WHERE EmployeeNumber = " + emp.EmployeeNumber;


            cmd.ExecuteNonQuery();
            connDB.Close();
        }



        public static void DeleteRecord(Employee emp)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "DELETE FROM Employees WHERE EmployeeNumber = " + emp.EmployeeNumber;
            cmd.ExecuteNonQuery();
            connDB.Close();
        }

        public static SqlDataReader SearchRecord(Employee emp,int searchBy)
        {
            string searchColumn = String.Empty;
            switch (searchBy)
            {
                case 0:
                    searchColumn = "EmployeeNumber = " + emp.EmployeeNumber + ";";
                    break;
                case 1:
                    searchColumn = "FirstName = '" + emp.FirstName + "';";
                    break;

                default:
                    break;
            }

            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "SELECT * FROM Employees " +
                               "WHERE " + searchColumn;
            SqlDataReader dr = cmd.ExecuteReader();       
            return dr;
        }
        public static SqlDataReader RecordList()
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = ("SELECT * FROM Employees ORDER BY EmployeeNumber;");
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
            
        }


    }
}
