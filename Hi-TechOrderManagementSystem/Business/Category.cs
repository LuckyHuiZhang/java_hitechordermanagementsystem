﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiTechOrderManagementSystem.DataAccess;
using System.Data.SqlClient;

namespace HiTechOrderManagementSystem.Business
{
    public class Category
    {
        private int categoryId;
        private string categoryName;

        public int CategoryId { get => categoryId; set => categoryId = value; }
        public string CategoryName { get => categoryName; set => categoryName = value; }

        public void SaveRecord(Category category)
        {
            CategoryDB.SaveCategory(category);
        }

        public void UpdateRecord(Category category)
        {
            CategoryDB.UpdateCategory(category);
        }

        public void DeleteRecord(Category category)
        {
            CategoryDB.DeleteCategory(category);
        }

        public SqlDataReader SearchRecord(Category category, int searchBy)
        {
            return CategoryDB.SearchCategory(category, searchBy);
        }

        public SqlDataReader ListRecord()
        {
            return CategoryDB.ListCategory();
        }
    }
}
