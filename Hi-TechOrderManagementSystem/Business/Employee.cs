﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiTechOrderManagementSystem.DataAccess;
using System.Data.SqlClient;

namespace HiTechOrderManagementSystem.Business
{
    public class Employee
    {
        private int employeeNumber;
        private string firstName;
        private string lastName;
        private string jobTitle;
        private string phoneNumber;
        private string email;


        public int EmployeeNumber { get => employeeNumber; set => employeeNumber = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string JobTitle { get => jobTitle; set => jobTitle = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string Email { get => email; set => email = value; }

        public void SaveEmployee(Employee emp)
        {
            EmployeeDB.SaveRecord(emp);
        }

        public void UpdateEmployee(Employee emp)
        {
            EmployeeDB.UpdateRecord(emp);
        }

        public void DeleteEmployee(Employee emp)
        {
            EmployeeDB.DeleteRecord(emp);
        }

        public SqlDataReader SearchEmployee(Employee emp, int searchBy)
        {
            return EmployeeDB.SearchRecord(emp, searchBy);
        }

        public SqlDataReader ListEmployee()
        {
            return EmployeeDB.RecordList();
        }
    }
}
