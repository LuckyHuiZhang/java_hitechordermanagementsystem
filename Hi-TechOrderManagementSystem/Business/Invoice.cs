﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using HiTechOrderManagementSystem.DataAccess;

namespace HiTechOrderManagementSystem.Business
{
    public class Invoice
    {
        private int invoiceNumber;
        private int orderId;
        private int itemId;
        private string clientName;
        private string orderDate;

        public int InvoiceNumber { get => invoiceNumber; set => invoiceNumber = value; }
        public int OrderId { get => orderId; set => orderId = value; }
        public int ItemId { get => itemId; set => itemId = value; }
        public string ClientName { get => clientName; set => clientName = value; }
        public string OrderDate { get => orderDate; set => orderDate = value; }

        //public void SaveRecord(Invoice invoice)
        //{
        //    InvoiceDB.SaveInvoice(invoice);
        //}

        //public void UpdateRecord(Invoice invoice)
        //{
        //    InvoiceDB.UpdateInvoice(invoice);
        //}

        //public void DeleteRecord(Invoice invoice)
        //{
        //    InvoiceDB.DeleteInvoice(invoice);
        //}

        public SqlDataReader SearchRecord(Invoice invoice, int searchBy)
        {
            return InvoiceDB.SearchInvoice(invoice,searchBy);
        }

        public SqlDataReader ListRecord()
        {
            return InvoiceDB.ListInvoice();
        }
    }
}
