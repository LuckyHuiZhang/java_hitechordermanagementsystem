﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HiTechOrderManagementSystem.Business;
using System.Data.SqlClient;

namespace HiTechOrderManagementSystem.DataAccess
{
    public static class AuthorDB
    {
        public static void SaveAuthor(Author author)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;

            string sqlInsert = "INSERT INTO Authors(AuthorId,PublisherId,BookISBN,AuthorFirstName,AuthorLastName,AuthorEmail)"
                + "VALUES (@AuthorId,@PublisherId,@BookISBN,@AuthorFirstName,@AuthorLastName,@AuthorEmail)";
            cmd.CommandText = sqlInsert;
            cmd.Parameters.AddWithValue("@AuthorId", author.AuthorId);
            cmd.Parameters.AddWithValue("@PublisherId", author.PublisherId);
            cmd.Parameters.AddWithValue("@BookISBN", author.BookISBN);
            cmd.Parameters.AddWithValue("@AuthorFirstName", author.AuthorFirstName);
            cmd.Parameters.AddWithValue("@AuthorLastName", author.AuthorLastName);
            cmd.Parameters.AddWithValue("@AuthorEmail", author.AuthorEmail);
            cmd.ExecuteNonQuery();
            connDB.Close();

        }

        public static void UpdateAuthor(Author author)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;

            cmd.CommandText = "UPDATE Authors " +
                              " SET AuthorId = " + author.AuthorId +
                              ",PublisherId= " + author.PublisherId +
                              ",BookISBN= " + author.BookISBN + 
                              ",AuthorFirstName ='" + author.AuthorFirstName + "'" +
                              ",AuthorLastName = '" + author.AuthorLastName + "'" +
                              ",AuthorEmail = '" + author.AuthorEmail + "'" +
                              " WHERE AuthorId = " + author.AuthorId;

            cmd.ExecuteNonQuery();
            connDB.Close();
        }

        public static void DeleteAuthor(Author author)
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "DELETE FROM Authors WHERE AuthorId = " + author.AuthorId;
            cmd.ExecuteNonQuery();
            connDB.Close();
        }

        public static SqlDataReader SearchAuthor(Author author, int searchBy)
        {
            string searchColumn = String.Empty;
            switch (searchBy)
            {
                case 0:
                    searchColumn = "AuthorId = " + author.AuthorId + ";";
                    break;
                case 1:
                    searchColumn = "AuthorFirstName = '" + author.AuthorFirstName + "';";
                    break;

                default:
                    break;
            }
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "SELECT * FROM Authors " +
                             "WHERE " + searchColumn;
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }

        public static SqlDataReader ListAuthor()
        {
            SqlConnection connDB = UtilityDB.ConnectDB();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connDB;
            cmd.CommandText = "SELECT * FROM Authors ORDER BY AuthorId ";
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }
    }
}
